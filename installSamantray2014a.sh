#!/bin/sh

### Create a folder for the dune and dumux modules
### Go into the folder and execute this script

if [ -d dune-common ]; then
  echo "error: A directory named dune-common already exists."
  echo "Aborting."
  exit 1
fi

### Clone the necessary modules
git clone https://gitlab.dune-project.org/core/dune-common.git
git clone https://gitlab.dune-project.org/core/dune-geometry.git
git clone https://gitlab.dune-project.org/core/dune-grid.git
git clone https://gitlab.dune-project.org/core/dune-istl.git
git clone https://gitlab.dune-project.org/core/dune-localfunctions.git
git clone https://gitlab.dune-project.org/pdelab/dune-pdelab.git
git clone https://gitlab.dune-project.org/pdelab/dune-typetree.git
git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git dumux
git clone https://git.iws.uni-stuttgart.de/dumux-pub/Samantray2014a.git dumux-Samantray2014a

### Go to specific branches
cd dune-common && git checkout releases/2.3 && cd ..
cd dune-geometry && git checkout releases/2.3 && cd ..
cd dune-grid && git checkout releases/2.3 && cd ..
cd dune-istl && git checkout releases/2.3 && cd ..
cd dune-localfunctions && git checkout releases/2.3 && cd ..
cd dune-typetree && git checkout releases/2.3 && cd ..
cd dune-pdelab && git checkout releases/2.0 && cd ..
cd dumux && git checkout releases/2.7 && cd ..
cd dumux-Samantray2014a && git checkout master && cd ..

### Go to specific commits
cd dune-common && git checkout 8cfcea54a78fee457993729535b7c63b3497be5e && cd ..
cd dune-geometry && git checkout 8e5075fc4c58e116d4a0f003a4d1f0482a3ab618 && cd ..
cd dune-grid && git checkout 2749fb531fc2ab027ca728f69634478b9ec9b814 && cd ..
cd dune-istl && git checkout b5979486b15ad529251b815fb63a9b4d2c765b49 && cd ..
cd dune-localfunctions && git checkout eedfe2a7639be4a7e6dc457fc5181454088aebe0 && cd ..
cd dune-pdelab && git checkout f7a7ff4a23e6f43967b006318480ffbf894ff63f && cd ..
cd dune-typetree && git checkout ecffa10c59fa61a0071e7c788899464b0268719f && cd ..
cd dumux && git checkout 80bd9b3c97d2fcd0191e014e81e01dfff37aaf48 && cd ..
cd dumux-Samantray2014a && git checkout 2f81f25d3c1eea2e5fdf4bbb60e104471f4790d2 && cd ..

### EXTERNAL MODULES: UG is required
./dumux-Samantray2014a/installexternal.sh ug

### run dunecontrol
pwd > temp.txt
sed -i 's/\//\\\//g' temp.txt
EXTPATH=`cat temp.txt`
/usr/bin/rm temp.txt
sed "s/EXTDIR=.*/EXTDIR=$EXTPATH\/external/" dumux-Samantray2014a/automake.opts > dumux-Samantray2014a/automake_used.opts
./dune-common/bin/dunecontrol --opts=dumux-Samantray2014a/automake_used.opts all
