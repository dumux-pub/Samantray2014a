Summary
=======
This is the DuMuX module containing the code for producing the results
published in:

P. Samantray<br>
Implementation of advanced algebraic turbulence models on a staggered grid<br>
Msc Thesis, 2014

You can use the .bib file provided [here](Samantray2014a.bib).


Installation
============

The easiest way to install this module is to create a new folder and to execute the file
[installSamantray2014a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Samantray2014a/raw/master/installSamantray2014a.sh)
in this folder.

```bash
mkdir -p Samantray2014a && cd Samantray2014a
wget -q https://git.iws.uni-stuttgart.de/dumux-pub/Samantray2014a/raw/master/installSamantray2014a.sh
sh ./installSamantray2014a.sh
```


Applications
============

The applications used for this publication can be found in appl/staggeredgrid/zeroeq/.
This folder also contains the jobfiles which specify the individual setups.
There is test which can be run using the ctest facility to check whether the reference
results are obtained.

* __test_lauferpipe__:
  A test case including a simple pipe flow setup according to the experiments by Laufer 1954.
* __test_verticalpipe__:
  A pipe with flow vertical direction.

In order to run an executable, type e.g.:
```bash
cd dumux-Samantray2014a/appl/staggeredgrid/zeroeq/
make test_lauferpipe
./test_lauferpipe
```


Used Versions and Software
==========================

For an overview on the used versions of the DUNE and DuMuX modules, please have a look at
[installSamantray2014a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Samantray2014a/raw/master/installSamantray2014a.sh).

In addition the following external software package are necessary for
compiling the executables:

| software           | version | type          |
| ------------------ | ------- | ------------- |
| automake           | 1.13.4  | build tool    |
| SuperLU            | 4.3     | linear solver |
| ug                 | 3.11.0  | grid manager  |

The module has been checked for the following compilers:

| compiler      | version |
| ------------- | ------- |
| gcc/g++       | 4.8     |
