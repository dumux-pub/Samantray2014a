###
# simulationOutput.sh
#
# This file automatically creates *.png files from csv files containing
# element or intersection related data

# HELP
if [ "$1" = "--help" -o "$1" = "-h" ] || [ -z $1 ]; then
  echo
  echo "USAGE"
  echo "$0 INPUTFILES"
  echo
  echo "INFO"
  echo "Please specify some files as input, you could also use * and ? operators."
  echo "The file \"simulationOutput.gnuplot\" has to exist in this folder."
  echo "The output can be found in the subdirectory \"pics\"."
  echo
  echo "OPTIONS"
  echo "-h     = show this help"
  echo
  exit
fi

if [ ! -d pics ]; then
  mkdir pics
else
#   echo "Directory pics already exists. Should it be removed?"
#   rm -I pics/*
  rm -f pics/*
  echo
fi

function cpGnuplotFile {
  echo 'reset' > temp.gnuplot
  echo 'set pm3d' >> temp.gnuplot
  echo 'set view map' >> temp.gnuplot
  echo 'set size ratio 1' >> temp.gnuplot
  echo 'set key below' >> temp.gnuplot
  echo 'set title "'$3'"' >> temp.gnuplot
  echo 'set palette rgb 33,13,10' >> temp.gnuplot
  echo '# set palette model HSV defined ( 0 0 1 1, 1 1 1 1 )' >> temp.gnuplot
  echo '# set palette model RGB defined (-2 "dark-green", -1 "green", 0 "red", 1 "yellow", 2 "dark-yellow" )' >> temp.gnuplot
  echo 'set terminal png size 800,800' >> temp.gnuplot
  echo 'set output "pics/"."'$2'"' >> temp.gnuplot
  echo 'splot "'$1'" using 1:2:COLUMN every 5::6 with points palette pointsize 1 pointtype 5 t ""' >> temp.gnuplot
  echo '# splot "'$1'" using 1:2:COLUMN with dots t ''' >> temp.gnuplot
}

FILES=("$@")
for ((FILENUM=0; FILENUM<${#FILES[@]}; FILENUM++)); do
INFILENAME=${FILES[${FILENUM}]}
cp $INFILENAME temp.out
sed "s/,/ /g" -i temp.out
printf "processing file: %s ... 1 2" $INFILENAME
NUMBER_OF_COLUMNS=`sed -n '2p' temp.out | grep ' ' -o | wc -l`
# echo "Total number of columns: $NUMBER_OF_COLUMNS"
  for ((COLUMN=3; COLUMN<$((NUMBER_OF_COLUMNS)); COLUMN++)); do
    printf " %d" "$COLUMN"
    TIME=`sed -n '1p' temp.out | awk '{print $3}'`
    COLUMNNAME=`sed -n '2p' temp.out | awk '{print $'$((COLUMN+1))'}'`
    OUTFILENAME=`printf "%s-%s.png" $COLUMNNAME ${FILES[${FILENUM}]}`
    cpGnuplotFile temp.out $OUTFILENAME "$COLUMNNAME @ $TIME s"
    sed "s/COLUMN/$COLUMN/g" -i temp.gnuplot
    gnuplot temp.gnuplot 2> temp-error.txt
  done
  echo " ... done"
#   if [ -s temp-error.txt ]; then # "-s" = file is not empty, "-f" = file exists
#     echo "                 `cat temp-error.txt`"
#     rm -rf temp-error.txt
#   fi
done

rm -rf temp.gnuplot temp.out temp-error.txt
