/**
 * \file
 * \ingroup StaggeredGrid
 * \ingroup NavierStokesStaggeredGrid
 *
 * \brief Storage term / transient part of staggered grid discretization for Navier-Stokes equation.
 *
 * The Navier-Stokes-Equations:<br>
 *
 * Mass balance:
 * \f[
 *    \frac{\partial}{\partial t} \varrho
 *    + \dots
 *    = 0
 * \f]
 *
 * Momentum balance:
 * \f[
 *    \frac{\partial}{\partial t}  \left( \varrho v \right)
 *    + \dots
 *    = 0
 * \f]
 */

#ifndef DUMUX_NAVIER_STOKES_TRANSIENT_STAGGERED_GRID_HH
#define DUMUX_NAVIER_STOKES_TRANSIENT_STAGGERED_GRID_HH

#include<cstddef>

#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/localoperator/flags.hh>
#include<dune/pdelab/localoperator/idefault.hh>
#include<dune/pdelab/localoperator/pattern.hh>

#include<appl/staggeredgrid/navierstokes/basenavierstokestransientstaggeredgrid.hh>

namespace Dune
{
  namespace PDELab
  {
    /**
     * \brief Local operator for staggered grid discretization solving
     * the transient part of the Navier-Stokes equation.
     */
    template<typename GridView>
    class NavierStokesTransientStaggeredGrid
    : public NumericalJacobianVolume<NavierStokesTransientStaggeredGrid<GridView> >,
      public NumericalJacobianApplyVolume<NavierStokesTransientStaggeredGrid<GridView> >,
      public FullVolumePattern,
      public LocalOperatorDefaultFlags,
      public InstationaryLocalOperatorDefaultMethods<double>,
      public BaseNavierStokesTransientStaggeredGrid<GridView>
    {
    public:
      typedef BaseNavierStokesTransientStaggeredGrid<GridView> ParentType;

      //! \brief pattern assembly flags
      enum { doPatternVolume = true };

      //! \brief residual assembly flags
      enum { doAlphaVolume = true };

      //! \brief Index of unknowns
      enum { velocityIdx = 0,
             pressureIdx = 1 };

      //! \brief Constructor
      NavierStokesTransientStaggeredGrid(GridView gridView_)
        : ParentType(gridView_)
      { }

      //! set time for subsequent evaluation
      void setTime (double t)
      {
        time = t;
      }

      /**
       * \brief Contribution to volume integral.
       *
       * This function just calls the ParentType.
       *
       * \tparam EG element geometry
       * \tparam LFSU local function space for ansatz functions
       * \tparam X coefficient vector
       * \tparam LFSV local function space for test functions
       * \tparam R residual vector
       * \param eg element geometry
       * \param lfsu local functions space for ansatz functions
       * \param x coefficient vector
       * \param lfsv local function space for test functions
       * \param r residual vector
       */
      template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
      {
        ParentType::alpha_volume(eg, lfsu, x, lfsv, r);
      }

    private:
      //! Instationary variables
      double time;
    };
  } // end namespace PDELab
} // end namespace Dune

#endif // DUMUX_NAVIER_STOKES_TRANSIENT_STAGGERED_GRID_HH
