if [ "$1" == "-h" -o "$1" == "--help" -o "$1" == "--h" ] || [ $# != 4 ]; then
  echo;
  echo "Usage: $0 FILE POSITION X_COLUMN Y_COLUMN"
  echo; exit
fi

head -n 2 $1 | tail -n 1 > plotOverLineData.csv
# POSITION=`awk -v col=$2 -F"," '{print $col}' plotOverLineData.csv`
XCOLUMN=`awk -v col=$3 -F"," '{print $col}' plotOverLineData.csv`
YCOLUMN=`awk -v col=$4 -F"," '{print $col}' plotOverLineData.csv`
TIME=`awk '{print $3}' $1`
echo "Creating plot..."
echo " time:      $TIME"
echo " position:  $2"
echo " xValue:    $XCOLUMN"
echo " yValue:    $YCOLUMN"
grep $2 $1 >> plotOverLineData.csv
cp plotOverLineData.csv temp.csv
# sort --key=$4 --field-separator=, plotOverLineData.csv > temp2.csv
# sort --key=$3 --field-separator=, temp2.csv > temp.csv

echo "set datafile separator ','" > temp.gp
echo "set title '$1 at $2 and time $TIME'" >> temp.gp
echo "set xlabel '$XCOLUMN'" >> temp.gp
echo "set ylabel '$YCOLUMN'" >> temp.gp
echo "plot 'temp.csv' u "$3":"$4" w l" >> temp.gp
echo 'set terminal png size 800,600' >> temp.gp
echo 'set output "plot.png"' >> temp.gp
echo 'replot' >> temp.gp

gnuplot --persist temp.gp
/usr/bin/rm temp.gp temp.csv

exit
