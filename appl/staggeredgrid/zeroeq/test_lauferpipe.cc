/**
 * \file
 * \ingroup StaggeredModel
 * \ingroup LauferPipe
 *
 *  \brief Solve Laufer's pipe flow using the staggered grid
 *         local operator for algebraic eddy viscosity models.
 *
 * The domain is of length 10m and height 0.2469m and has no-slip boundaries
 * on top and bottom, which means, that the velocities are fixed to zero there.
 * At the left a constant velocity profile is given and on the right outflow
 * conditions are applied.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include<iostream>
#include<vector>
#include<dune/common/parallel/mpihelper.hh>
#include<dune/common/exceptions.hh>
#include<dune/common/fvector.hh>
#include<dune/grid/io/file/dgfparser/dgfug.hh>
#include<dune/grid/io/file/dgfparser/gridptr.hh>
#include<dune/grid/io/file/vtk/subsamplingvtkwriter.hh>
#include<dune/grid/sgrid.hh>
#include<dune/istl/bvector.hh>
#include<dune/istl/io.hh>
#include<dune/istl/operators.hh>
#include<dune/istl/preconditioners.hh>
#include<dune/istl/solvers.hh>
#include<dune/istl/superlu.hh>

#include<dune/pdelab/backend/istlmatrixbackend.hh>
#include<dune/pdelab/backend/istlsolverbackend.hh>
#include<dune/pdelab/backend/istlvectorbackend.hh>
#include<dune/pdelab/common/function.hh>
#include<dune/pdelab/common/vtkexport.hh>
#include<dune/pdelab/constraints/noconstraints.hh>
#include<dune/pdelab/constraints/common/constraintsparameters.hh>
#include<dune/pdelab/finiteelementmap/p0fem.hh>
#include<dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include<dune/pdelab/gridfunctionspace/gridfunctionspaceutilities.hh>
#include<dune/pdelab/gridfunctionspace/interpolate.hh>
#include<dune/pdelab/gridfunctionspace/subspace.hh>
#include<dune/pdelab/gridfunctionspace/vtk.hh>
#include<dune/pdelab/gridoperator/gridoperator.hh>
#include<dune/pdelab/gridoperator/onestep.hh>
#include<dune/pdelab/instationary/onestep.hh>

#define PRINT_GNUPLOT_OUTPUT 1
#define PRINT_ACCUMULATION_TERM 0
#define PRINT_ACCUMULATION_TERM_EDDY 0
#define PRINT_EDDY_VISCOSITY 0
unsigned int numStartCells = 32;
unsigned int numRefinementLevels = 0;
double tend = 5;
double dt = .1;
unsigned int eddyViscosityModel = 0;
int velocity = 2.5;

#include<appl/staggeredgrid/zeroeq/problem_lauferpipe.hh>

#include<appl/staggeredgrid/common/callbacknewton.hh>
#include<appl/staggeredgrid/common/fixvelocityconstraints.hh>
#include<appl/staggeredgrid/localfunctions/staggeredq0fem.hh>
#include<appl/staggeredgrid/navierstokes/navierstokestransientstaggeredgrid.hh>
#include<appl/staggeredgrid/zeroeq/zeroeqstaggeredgrid.hh>


#include <ctime>
using namespace std;
clock_t start = clock();

/**
 * \ingroup StaggeredModel
 *
 * \brief Problem setup and solution, including an own Newton solver.
 *
 * \tparam BCType Boundary condition type
 * \tparam SourceMomentumBalance Source type for momentum balance (velocity)
 * \tparam SourceMassBalance Source type for mass balance (pressure)
 * \tparam DirichletVelocity Dirchlet boundary condition for velocity
 * \tparam DirichletPressure Dirchlet boundary condition for pressure
 * \tparam NeumannVelocity Dirchlet boundary condition for velocity
 * \tparam NeumannPressure Dirchlet boundary condition for pressure
 * \tparam InitialVelocity Initial condition for velocity
 * \tparam InitialPressure Initial condition for pressure
 * \tparam GV GridView type
 * \tparam VFEM Finite element map for velocity
 * \tparam PFEM Finite element map for pressure
*/
template<typename BCType,
         typename SourceMomentumBalance, typename SourceMassBalance,
         typename DirichletVelocity, typename DirichletPressure,
         typename NeumannVelocity, typename NeumannPressure,
         typename InitialVelocity, typename InitialPressure,
         typename GV, typename VFEM, typename PFEM>
void driver(BCType& bc, SourceMomentumBalance& sourceMomentumBalance, SourceMassBalance& sourceMassBalance,
            DirichletVelocity& dirichletVelocity, DirichletPressure& dirichletPressure,
            NeumannVelocity& neumannVelocity, NeumannPressure& neumannPressure,
            InitialVelocity& initialVelocity, InitialPressure& initialPressure,
            const GV& gv, const VFEM& vfem, const PFEM& pfem, std::string filename, int gridCells, unsigned int eddyViscosityModel, int velocity)
{
  // types and constants
  typedef typename GV::Grid::ctype DF;
  typedef double RF;
  const int indexVelocity = 0;
  const int indexPressure = 1;

  // construct grid function spaces
  typedef Dune::PDELab::ISTLVectorBackend<> VectorBackend;
  typedef Dune::PDELab::FixVelocityConstraints VelocityConstraints;
  VelocityConstraints velocityConstraints;
  typedef Dune::PDELab::GridFunctionSpace<GV, VFEM, VelocityConstraints, VectorBackend> StaggeredQ0GFS;
  StaggeredQ0GFS staggeredQ0Gfs(gv, vfem, velocityConstraints);
  staggeredQ0Gfs.name("velocity");
  typedef Dune::PDELab::GridFunctionSpace<GV, PFEM, Dune::PDELab::NoConstraints, VectorBackend> P0GFS;
  P0GFS p0gfs(gv, pfem);
  p0gfs.name("pressure");
  // composed function space
  typedef Dune::PDELab::CompositeGridFunctionSpace<VectorBackend,
    Dune::PDELab::LexicographicOrderingTag, StaggeredQ0GFS, P0GFS> MGFS;
  MGFS mgfs(staggeredQ0Gfs, p0gfs);

  // constraints
  typedef typename MGFS::template ConstraintsContainer<RF>::Type ConstraintsContainer;
  // container for transformation
  ConstraintsContainer constraintsContainer;
  constraintsContainer.clear();
  // fill container
  Dune::PDELab::constraints(bc, mgfs, constraintsContainer);

  // construct composite grid functions for initial condition and Dirichlet boundaries
  typedef Dune::PDELab::CompositeGridFunction<InitialVelocity, InitialPressure> InitialComposed;
  InitialComposed initialComposed(initialVelocity, initialPressure);
  typedef Dune::PDELab::CompositeGridFunction<DirichletVelocity, DirichletPressure> DirichletComposed;
  DirichletComposed dirichletComposed(dirichletVelocity, dirichletPressure);

  // make grid function operator
  typedef Dune::PDELab::ZeroEqStaggeredGrid<BCType, SourceMomentumBalance, SourceMassBalance,
    DirichletVelocity, DirichletPressure, NeumannVelocity, NeumannPressure, GV> LOP;
  LOP lop(bc, sourceMomentumBalance, sourceMassBalance,
          dirichletVelocity, dirichletPressure, neumannVelocity, neumannPressure, gv, eddyViscosityModel);
  typedef Dune::PDELab::NavierStokesTransientStaggeredGrid<GV> TransientLocalOperator;
  TransientLocalOperator transientLocalOperator(gv);

  typedef Dune::PDELab::ISTLMatrixBackend MatrixBackend;
  typedef Dune::PDELab::GridOperator<MGFS,MGFS,LOP,MatrixBackend,
    double,double,double,ConstraintsContainer,ConstraintsContainer> GO0;
  GO0 go0(mgfs,constraintsContainer,mgfs,constraintsContainer,lop);
  typedef Dune::PDELab::GridOperator<MGFS,MGFS,TransientLocalOperator,MatrixBackend,
    double,double,double,ConstraintsContainer,ConstraintsContainer> GO1;
  GO1 go1(mgfs,constraintsContainer,mgfs,constraintsContainer,transientLocalOperator);
  typedef Dune::PDELab::OneStepGridOperator<GO0,GO1> IGO;
  IGO igo(go0,go1);
  igo.divideMassTermByDeltaT(); // TODO: oder anders? multiplySpatialTermByDeltaT();
  typedef typename IGO::Traits::Domain X;

  // Linear solver
  typedef Dune::PDELab::ISTLBackend_SEQ_SuperLU LinearSolver;
  LinearSolver ls(false);

  // Solve (possibly) nonlinear problem
  Dune::PDELab::CallbackNewton<IGO,LOP,LinearSolver,X> newton(igo,lop,ls);
  newton.setVerbosityLevel(2);
  newton.setMaxIterations(10);
  newton.setReduction(1e-6);
  newton.setAbsoluteLimit(1e-5);
  newton.setLineSearchStrategy(newton.noLineSearch);

  // time stepper / implicit Euler scheme
  Dune::PDELab::ImplicitEulerParameter<double> method;               // defines coefficients
  Dune::PDELab::OneStepMethod<double,IGO,Dune::PDELab::CallbackNewton<IGO,LOP,LinearSolver,X>,X,X> osm(method,igo,newton);
  osm.setVerbosityLevel(2);                                     // time stepping scheme

  // make coefficent Vectors
  X xOld(mgfs, 0.0);
  // do interpolation
  Dune::PDELab::interpolate(initialComposed, mgfs, xOld);
//   Dune::PDELab::set_nonconstrained_dofs(constraintsContainer, 0.0, xOld);  // clear interior
  //Dune::PDELab::set_shifted_dofs(constraintsContainer, 0.0, xOld);

  lop.updateStoredValues(mgfs, xOld);

  // plot initial solution
  typedef Dune::PDELab::GridFunctionSubSpace
    <MGFS,Dune::TypeTree::TreePath<indexVelocity> > VSUB;
  VSUB vsub(mgfs);                   // velocity subspace

  typedef Dune::PDELab::GridFunctionSubSpace
    <MGFS,Dune::TypeTree::TreePath<indexPressure> > PSUB;
  PSUB psub(mgfs);                   // pressure subspace

  // make discrete function object
  typedef Dune::PDELab::DiscreteGridFunction<VSUB, X> StaggeredQ0DGF;
  StaggeredQ0DGF staggeredQ0DGF(vsub, xOld);
  typedef Dune::PDELab::DiscreteGridFunction<PSUB, X> P0DGF;
  P0DGF p0dgf(psub, xOld);
 
  // plot result as VTK
  Dune::SubsamplingVTKWriter<GV> vtkwriter(gv, 1);
  vtkwriter.addCellData(new Dune::PDELab::VTKGridFunctionAdapter<P0DGF>(p0dgf, "pressure"));
  vtkwriter.addCellData(new Dune::PDELab::VTKGridFunctionAdapter<StaggeredQ0DGF>(staggeredQ0DGF, "velocity"));
  char fname[255];
  sprintf(fname, "test_lauferpipe-%04d-eddyViscModel%02d-0000", gridCells, eddyViscosityModel);
  vtkwriter.write(fname, Dune::VTK::ascii);

  // output of custom element and intersection data
  lop.gnuplotElementOutput(0, 0, gridCells);
  lop.gnuplotIntersectionOutput(0, 0, gridCells);

  // time loop
  X xNew(mgfs, 0.0);                                              // solution to be computed
  double time = 0.0;
  unsigned int timestep = 1;
  std::vector<double> timeVector(0.0);
  timeVector.push_back(time);
  while (time < tend - 1e-8)
  {
    lop.updateStoredValues(mgfs, xOld);
    osm.apply(time, dt, xOld, dirichletComposed, xNew);       // do one time step
    
    // plot result as VTK
    StaggeredQ0DGF staggeredQ0DGF(vsub, xNew);
    P0DGF p0dgf(psub, xNew);
    Dune::SubsamplingVTKWriter<GV> vtkwriter(gv, 1);
    vtkwriter.addCellData(new Dune::PDELab::VTKGridFunctionAdapter<P0DGF>(p0dgf, "pressure"));
    vtkwriter.addCellData(new Dune::PDELab::VTKGridFunctionAdapter<StaggeredQ0DGF>(staggeredQ0DGF, "velocity"));
    sprintf(fname, "test_lauferpipe-%04d-eddyViscModel%02d-%04d", gridCells, eddyViscosityModel, timestep);
    vtkwriter.write(fname, Dune::VTK::ascii);

    // output of custom element and intersection data
    lop.gnuplotElementOutput(time+dt, timestep, gridCells);
    lop.gnuplotIntersectionOutput(time+dt, timestep, gridCells);

    xOld = xNew;                                              // advance time step
//    if (time > 3.0 - 1e-6)
//    {
//    	dt = 0.5;
//    }
    time += dt;
    timeVector.push_back(time);
    ++timestep;
  }

  sprintf(fname, "test_lauferpipe-%04d-eddyViscModel%02d.pvd", gridCells, eddyViscosityModel);
  std::string pvdname = fname;
  std::ofstream pvd(pvdname.c_str());
  //std::cout << "WRITE PVD FILE " << pvdname << std::endl;
  assert(pvd.is_open());
  pvd << std::fixed;
  pvd << "<?xml version=\"1.0\"?>\n"
      << "<VTKFile type=\"Collection\" version=\"0.1\">\n"
      << "<Collection>\n";
  for (unsigned int i=0; i<timestep; i++)
      {
          sprintf(fname, "test_lauferpipe-%04d-eddyViscModel%02d-%04d.vtu", gridCells, eddyViscosityModel, i);
          pvd << "  <DataSet"
              << " index=\"" << i << "\""
              << " timestep=\"" << timeVector[i] << "\""
              << " file=\"" << fname << "\"/>\n";
      }
  pvd << "</Collection>\n"
      << "</VTKFile>\n";
  pvd.close();
}

/**
 * \brief Construct a composite boundary condition function and calls driver function
 *
 * \tparam GV GridView type
 * \tparam VFEM Finite element method for velocity
 * \tparam PFEM Finite element method for pressure
 */
template<typename GV, typename PFEM, typename VFEM>
void dispatcher(const GV& gv, const VFEM& vfem, const PFEM& pfem, std::string gridname, int gridCells, unsigned int eddyViscosityModel, int velocity)
{
  typedef double RF;

  // Construct a composite boundary condition type function
  BCVelocity bcVelocity;
  BCPressure bcPressure;
  typedef Dune::PDELab::CompositeConstraintsParameters<BCVelocity, BCPressure> BCType;
  BCType bc(bcVelocity, bcPressure);
  DirichletVelocity<GV, RF> dirichletVelocity(gv);
  DirichletPressure<GV, RF> dirichletPressure(gv);
  NeumannVelocity<GV, RF> neumannVelocity(gv);
  NeumannPressure<GV, RF> neumannPressure(gv);
  SourceMomentumBalance<GV, RF> sourceMomentumBalance(gv);
  SourceMassBalance<GV, RF> sourceMassBalance(gv);
  InitialVelocity<GV, RF> initialVelocity(gv);
  InitialPressure<GV, RF> initialPressure(gv);
  driver(bc, sourceMomentumBalance, sourceMassBalance, dirichletVelocity, dirichletPressure,
         neumannVelocity, neumannPressure,
         initialVelocity, initialPressure, gv, vfem, pfem, gridname, gridCells, eddyViscosityModel, velocity);
}

/**
 * \brief Main routine of the program
 */
int main(int argc, char** argv)
{
  std::cout << std::endl << "Reynoldsnumber of this setup is: " << (VELOCITY * PIPE_HEIGHT / KINEMATIC_VISCOSITY) << std::endl;
  try{
	//Maybe initialize MPI
	Dune::MPIHelper::instance(argc, argv);

	char gridFile[200];
	sprintf(gridFile, "grids/temp.dgf");

    if (argc == 5)
    {
      for (unsigned int i = 0; i < argc; ++i)
      {
        std::cout << " " << argv[i];
      }
      std::cout << std::endl;
      numStartCells = atoi(argv[1]);
      dt = atof(argv[2]);
      tend = atof(argv[3]);
      eddyViscosityModel = atoi(argv[4]);
//      velocity = atoi(argv[5]);
    }
    else
    {
      std::cout << "usage " << argv[0] << " numStartCells dt tend:" << std::endl
        << "Wrong number of arguments, defaulting to:" << std::endl
        << " numStartCells " << numStartCells << std::endl
        << " dt " << dt << std::endl
        << " tend " << tend << std::endl
      << " eddyViscosityModel " << eddyViscosityModel << std::endl;
//      << " velocity " << velocity << std::endl;
    }

    for (unsigned int refine = 0; refine <= numRefinementLevels; ++refine)
    {
        char fname[200];
  	    const int dim = 2;

        typedef Dune::UGGrid<dim> Grid;
        Dune::GridPtr<Grid> grid_ptr(gridFile);
        Grid& grid = *grid_ptr;
        grid.globalRefine(refine);
        int gridCells = 4 * std::pow(2, refine);

//        Dune::FieldVector<double, dim> low(0.0);
//        Dune::FieldVector<double, dim> high(0.0);
//        high[0] = PIPE_LENGTH;
//        high[1] = PIPE_HEIGHT;
//        Dune::FieldVector<int, dim> n(numStartCells);
//        n[0] = numStartCells;
//        n[1] = numStartCells;
//        typedef Dune::SGrid<dim, dim, double> Grid;
//        Grid grid(n, low, high);
//        grid.globalRefine(refine);
//        int gridCells = numStartCells * std::pow(2, refine);

        // instantiate finite element maps
        typedef Grid::ctype DF;
        typedef Grid::LeafGridView GV;
        typedef double RF;
        typedef Dune::PDELab::P0LocalFiniteElementMap<DF, RF, dim> P0FEM;
        P0FEM p0fem(Dune::GeometryType(Dune::GeometryType::cube, dim));
        typedef Dune::PDELab::StaggeredQ0LocalFiniteElementMap<DF, RF, dim> StaggeredQ0FEM;
        StaggeredQ0FEM staggeredq0fem;

        sprintf(fname, "test_lauferpipe-%04d-eddyViscModel%02d", gridCells, eddyViscosityModel);
        dispatcher(grid.leafGridView(), staggeredq0fem, p0fem, fname, gridCells, eddyViscosityModel, velocity);
    }
    clock_t ends = clock();
       std::cout << "Running Time : " << (double) (ends - start) / CLOCKS_PER_SEC << endl;
    // test passed
    return 0;

  }
  catch (Dune::Exception &e){
    std::cerr << "Dune reported error: " << e << std::endl;
    clock_t ends = clock();
       std::cout << "Running Time : " << (double) (ends - start) / CLOCKS_PER_SEC << endl;
    return 1;
  }
  catch (...){
    std::cerr << "Unknown exception thrown!" << std::endl;
    clock_t ends = clock();
       std::cout << "Running Time : " << (double) (ends - start) / CLOCKS_PER_SEC << endl;
    return 1;
  }
}


   



