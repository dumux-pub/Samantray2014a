/**
 * \file
 * \ingroup VerticalPipe
 *
 * \brief Boundary condition types and boundary condition values for
 *        a vertical pipe flow experiment.
 */

#ifndef DUMUX_ZEROEQ_PROBLEM_VERTICAL_PIPE_HH
#define DUMUX_ZEROEQ_PROBLEM_VERTICAL_PIPE_HH

#define ENABLE_NAVIER_STOKES 1

// EDDY VISCOSITY MODEL
// 0 no eddy viscosity model
// 1 prandtl mixing length
// 9 linear eddy visosity distribution
#define WALL_NORMAL_AXIS 0
#define BBOXMIN_IS_WALL 1
#define BBOXMAX_IS_WALL 1

// FLUID PROPERTIES
#define KINEMATIC_VISCOSITY 15e-6   // GAS
#define DENSITY 1.21                // GAS

// PIPE PROPERTIES AND BOUNDARY CONDITIONS
#define VELOCITY 1.0
#define PRESSURE 1.0e5
#define PIPE_LENGTH 1.5
#define PIPE_HEIGHT 4.5

#define EPSILON 1e-6

#include<cmath>
#include<dune/pdelab/common/function.hh>
#include<dune/pdelab/constraints/common/constraintsparameters.hh>

#include<appl/staggeredgrid/common/velocitywallfunctions.hh>

/**
 * \brief Boundary condition function for the velocity components.
 */
class BCVelocity
: public Dune::PDELab::DirichletConstraintsParameters
{
public:
  //! \brief Return whether Intersection is Wall Boundary for velocity
  //! \tparam I Intersection type
  template<typename I>
  bool isWall(const I& intersection,
              const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
  {
//     return (!isInflow(intersection, coord) && !isOutflow(intersection, coord));
    Dune::FieldVector<typename I::ctype, I::dimension>
      global = intersection.geometry().global(coord);
    return (global[0] < EPSILON
            || global[0] > PIPE_LENGTH -EPSILON
            || (global[0] >= PIPE_LENGTH / 2.0 && global[1] > PIPE_HEIGHT - EPSILON)
            || (global[0] <= PIPE_LENGTH / 2.0 && global[1] < EPSILON));
  }

  //! \brief Return whether Intersection is Inflow Boundary for velocity
  //! \tparam I Intersection type
  template<typename I>
  bool isInflow(const I& intersection,
                const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
  {
    Dune::FieldVector<typename I::ctype, I::dimension>
      global = intersection.geometry().global(coord);
    return (global[1] < EPSILON && global[0] > PIPE_LENGTH / 2.0);
  }

  //! \brief Return whether Intersection is Outflow Boundary for velocity
  //! \tparam I Intersection type
  template<typename I>
  bool isOutflow(const I& intersection,
                 const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
  {
    Dune::FieldVector<typename I::ctype, I::dimension>
      global = intersection.geometry().global(coord);
    return (global[1] > PIPE_HEIGHT - EPSILON && global[0] < PIPE_LENGTH / 2.0);
  }

  //! \brief Return whether Intersection is Symmetry Boundary for velocity
  //! \tparam I Intersection type
  template<typename I>
  bool isSymmetry(const I& intersection,
                  const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
  {
    return false;
  }
};

/**
 * \brief Boundary condition function for the pressure component.
 */
class BCPressure
: public Dune::PDELab::DirichletConstraintsParameters
{
public:
  //! \brief Return whether Intersection is Dirichlet Boundary for pressure
  //! \tparam I Intersection type
  template<typename I>
  bool isDirichlet(const I& intersection,
                   const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
  {
    const Dune::FieldVector<typename I::ctype, I::dimension>
      global = intersection.geometry().global(coord);
    return (global[1] > PIPE_HEIGHT - EPSILON && global[0] < PIPE_LENGTH / 2.0);
  }

  //! \brief Return whether Intersection is Outflow Boundary for pressure
  //! \tparam I Intersection type
  template<typename I>
  bool isOutflow(const I& intersection,
                 const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
  {
    return !isDirichlet(intersection, coord); // everywhere else
  }
};


/**
 * \brief Function for velocity Dirichlet boundary conditions and initialization.
 *
 * \tparam GV GridView type
 * \tparam RF Range type
 */
template<typename GV, typename RF>
class DirichletVelocity
: public Dune::PDELab::AnalyticGridFunctionBase<
    Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, GV::dimension>, DirichletVelocity<GV, RF> >,
  public Dune::PDELab::InstationaryFunctionDefaults,
  public Dune::PDELab::VelocityWallFunctions<GV, RF>
{
public:
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, GV::dimension> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, DirichletVelocity<GV, RF> > BaseT;
  typedef Dune::PDELab::VelocityWallFunctions<GV, RF> VelocityWallFunctions;

  //! \brief Constructor
  DirichletVelocity (const GV& gv)
  : BaseT(gv), VelocityWallFunctions(gv) {}

  inline void evaluate (const typename Traits::ElementType& e,
                        const typename Traits::DomainType& x,
                        typename Traits::RangeType& y) const
  {
    const typename Traits::DomainType& global = e.geometry().global(x);

    y = 0.0;
    if (global[1] < EPSILON && global[0] > PIPE_LENGTH / 2.0)
    {
      y[1] = VELOCITY;
    }

    if (this->useWallFunction(e) && x[0] < PIPE_LENGTH / 2.0)
    {
      y[0] = this->beaversJosephSlipVelocity(e, 1.0e-7, 1.0);
  //       if ((global[0] > 6.25 - EPSILON && global[0] < 6.25 + EPSILON)
  //           || (global[0] > 8.75 - EPSILON && global[0] < 8.75 + EPSILON))
  //       {
  //         std::cout << "bj @ " << global << " : " << this->beaversJosephSlipVelocity(e, 1.0e-7, 1.0) << std::endl;
  //       }
    }
  }

  //! \brief Roughness Value \f$ m \f$
  double roughness(const typename Traits::ElementType& e,
                   const typename Traits::DomainType& x) const
  {
    if (this->useWallFunction(e) && x[0] > PIPE_LENGTH / 2.0)
    {
      return 0.1;
    }
    else
    {
      return 0.0;
    }
  }
};

/**
 * \brief Function for pressure Dirichlet boundary conditions and initialization.
 *
 * \tparam GV GridView type
 * \tparam RF Range type
 */
template<typename GV, typename RF>
class DirichletPressure
: public Dune::PDELab::AnalyticGridFunctionBase<
    Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, 1>, DirichletPressure<GV, RF> >,
  public Dune::PDELab::InstationaryFunctionDefaults
{
public:
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, 1> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, DirichletPressure<GV, RF> > BaseT;

  //! \brief Constructor
  DirichletPressure (const GV& gv)
  : BaseT(gv) {}

  //! \brief Evaluate the function globally
  inline void evaluateGlobal(const typename Traits::DomainType& x,
                             typename Traits::RangeType& y) const
  {
    y = PRESSURE;
  }
};


/**
 * \brief Function for velocity Neumann boundary conditions and initialization.
 *
 * \tparam GV GridView type
 * \tparam RF Range type
 */
template<typename GV, typename RF>
class NeumannVelocity
: public Dune::PDELab::AnalyticGridFunctionBase<
    Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, GV::dimension>, NeumannVelocity<GV, RF> >
{
public:
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, GV::dimension> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, NeumannVelocity<GV, RF> > BaseT;

  //! \brief Constructor
  NeumannVelocity (const GV& gv)
  : BaseT(gv) {}

  //! \brief Evaluate the function globally
  inline void evaluateGlobal(const typename Traits::DomainType& x,
                             typename Traits::RangeType& y) const
  {
    y = 0;
  }
};

/**
 * \brief Function for pressure Neumann boundary conditions and initialization.
 *
 * \tparam GV GridView type
 * \tparam RF Range type
 */
template<typename GV, typename RF>
class NeumannPressure
: public Dune::PDELab::AnalyticGridFunctionBase<
    Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, 1>, NeumannPressure<GV, RF> >
{
public:
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, 1> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, NeumannPressure<GV, RF> > BaseT;

  //! \brief Constructor
  NeumannPressure (const GV& gv)
  : BaseT(gv) {}

  //! \brief Evaluate the function globally
  inline void evaluateGlobal(const typename Traits::DomainType& x,
                             typename Traits::RangeType& y) const
  {
    y = 0;
  }
};


/**
 * \brief Source term function for the momentum balance.
 *
 * \tparam GV GridView type
 * \tparam RF Range type
 */
template<typename GV, typename RF>
class SourceMomentumBalance
  : public Dune::PDELab::AnalyticGridFunctionBase<
      Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, GV::dimension>, SourceMomentumBalance<GV, RF> >
{
public:
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, GV::dimension> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, SourceMomentumBalance<GV, RF> > BaseT;

  //! \brief Constructor
  SourceMomentumBalance (const GV& gv)
  : BaseT(gv) {}

  //! \brief Evaluate the function globally
  inline void evaluateGlobal(const typename Traits::DomainType& x,
                             typename Traits::RangeType& y) const
  {
    y = 0.0;
  }
};

/**
 * \brief Source term function for the mass balance.
 *
 * \tparam GV GridView type
 * \tparam RF Range type
 */
template<typename GV, typename RF>
class SourceMassBalance
  : public Dune::PDELab::AnalyticGridFunctionBase<
      Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, 1>, SourceMassBalance<GV, RF> >
{
public:
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, 1> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, SourceMassBalance<GV, RF> > BaseT;

  //! \brief Constructor
  SourceMassBalance (const GV& gv)
  : BaseT(gv) {}

  //! \brief Evaluate the function globally
  inline void evaluateGlobal(const typename Traits::DomainType& x,
                             typename Traits::RangeType& y) const
  {
    y = 0.0;
  }
};

#endif // DUMUX_ZEROEQ_PROBLEM_VERTICAL_PIPE_HH
