/**
 * \file
 * \ingroup StaggeredGrid
 * \ingroup ZeroEqStaggeredGrid
 *
 * \copydoc EddyViscosityStaggeredGrid
 *
 * \brief Local operator for staggered grid discretization for steady-state
 * Navier-Stokes equation, modeled with algebraic turbulence models.
 *
 * The eddy viscosity depends on the chosen model, but looks in general like this:
 * \f[
 *    \nu_{\alpha,\textrm{t}}
 *    = \nu_{\alpha,\textrm{t}} \left( y, \left| \frac{\partial u}{\partial y} \right| \right)
 *    > 0
 * \f]
 *
 * The following models are available and can be chosen via setting <tt>eddyViscosityModel</tt>,
 * for detailed description have look at the individual functions in the local operator.<br>
 * (0) No eddy viscosity model<br>
 * (1) Prandtl's mixing length<br>
 * (9) linear eddy viscosity distribution<br>
 *
 * \note Be aware that the storage terms (the time dependent terms)
 * are not included here but are implemented in the transient part
 * of the local operator.
 */

#ifndef DUMUX_ZEROEQ_STAGGERED_GRID_HH
#define DUMUX_ZEROEQ_STAGGERED_GRID_HH

#include<algorithm>
#include<cstddef>
#include<vector>

#include<dune/common/exceptions.hh>
#include<dune/common/fvector.hh>
#include<dune/common/fmatrix.hh>

#include<dune/geometry/referenceelements.hh>
#include<dune/geometry/type.hh>
#include<dune/grid/common/mcmgmapper.hh>

#include<dune/pdelab/gridfunctionspace/localfunctionspace.hh>
#include<dune/pdelab/gridfunctionspace/lfsindexcache.hh>
#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/localoperator/idefault.hh>
#include<dune/pdelab/localoperator/flags.hh>
#include<dune/pdelab/localoperator/pattern.hh>

#include<dune/geometry/quadraturerules.hh>

#include<appl/staggeredgrid/zeroeq/eddyviscositystaggeredgrid.hh>

namespace Dune
{
  namespace PDELab
  {
    /**
     * \brief Local operator for staggered grid discretization solving
     * the steady-state Navier-Stokes equation.
     *
     * \tparam BC Boundary condition type
     * \tparam SourceMomentumBalance Source type for velocity
     * \tparam SourceMassBalance Source type for pressure
     * \tparam DirichletVelocity Dirchlet boundary condition for velocity
     * \tparam DirichletPressure Dirchlet boundary condition for pressure
     * \tparam NeumannVelocity Dirchlet boundary condition for velocity
     * \tparam NeumannPressure Dirchlet boundary condition for pressure
     * \tparam GridView Grid view type
     */
    template<typename BC, typename SourceMomentumBalance, typename SourceMassBalance,
             typename DirichletVelocity, typename DirichletPressure,
             typename NeumannVelocity, typename NeumannPressure, typename GridView>
    class ZeroEqStaggeredGrid
    :
      public NumericalJacobianApplyVolume<ZeroEqStaggeredGrid<
        BC, SourceMomentumBalance, SourceMassBalance, DirichletVelocity, DirichletPressure,
        NeumannVelocity, NeumannPressure, GridView> >,
      public NumericalJacobianVolume<ZeroEqStaggeredGrid<
        BC, SourceMomentumBalance, SourceMassBalance, DirichletVelocity, DirichletPressure,
        NeumannVelocity, NeumannPressure, GridView> >,
      public NumericalJacobianSkeleton<ZeroEqStaggeredGrid<
        BC, SourceMomentumBalance, SourceMassBalance, DirichletVelocity, DirichletPressure,
        NeumannVelocity, NeumannPressure, GridView> >,
      public NumericalJacobianBoundary<ZeroEqStaggeredGrid<
        BC, SourceMomentumBalance, SourceMassBalance, DirichletVelocity, DirichletPressure,
        NeumannVelocity, NeumannPressure, GridView> >,
      public FullVolumePattern,
      public FullSkeletonPattern,
      public LocalOperatorDefaultFlags,
      public InstationaryLocalOperatorDefaultMethods<double>,
      public EddyViscosityStaggeredGrid<BC,
        SourceMomentumBalance, SourceMassBalance,
        DirichletVelocity, DirichletPressure,
        NeumannVelocity, NeumannPressure,
        GridView>
    {
    public:
      typedef EddyViscosityStaggeredGrid<BC,
        SourceMomentumBalance, SourceMassBalance,
        DirichletVelocity, DirichletPressure,
        NeumannVelocity, NeumannPressure,
        GridView> ParentType;
      typedef Dune::MultipleCodimMultipleGeomTypeMapper<GridView, Dune::MCMGIntersectionLayout>
        MapperIntersection;
      typedef Dune::MultipleCodimMultipleGeomTypeMapper<GridView, Dune::MCMGElementLayout>
        MapperElement;
      // type to store the tangentially derived coordinates and velocities for each face
      typedef std::vector<Dune::FieldVector<double, GridView::dimension> > StoredGlobalCoordinate;
      typedef std::vector<typename GridView::IndexSet::IndexType> StoredGridViewID;
      typedef std::vector<double> StoredScalar;
      std::vector<Dune::FieldVector<double, GridView::dimension> > storedPressureGradient;

      //! \brief Pattern assembly flags
      enum { doPatternVolume = true,
             doPatternSkeleton = true };

      //! \brief Residual assembly flags
      enum { doAlphaVolume = true,
             doAlphaSkeleton = true,
             doAlphaBoundary = true };

      //! \brief Index of unknowns
      enum { velocityIdx = 0,
             pressureIdx = 1 };

      //! \brief Constructor
      ZeroEqStaggeredGrid(const BC& bc_,
                          const SourceMomentumBalance& sourceMomentumBalance_, const SourceMassBalance& sourceMassBalance_,
                          const DirichletVelocity& dirichletVelocity_, const DirichletPressure& dirichletPressure_,
                          const NeumannVelocity& neumannVelocity_, const NeumannPressure& neumannPressure_,
                          GridView gridView_, unsigned int eddyViscosityModel_)
        : ParentType(bc_,
              sourceMomentumBalance_, sourceMassBalance_,
              dirichletVelocity_, dirichletPressure_,
              neumannVelocity_, neumannPressure_,
              gridView_),
          bc(bc_),
          sourceMomentumBalance(sourceMomentumBalance_), sourceMassBalance(sourceMassBalance_),
          dirichletVelocity(dirichletVelocity_), dirichletPressure(dirichletPressure_),
          neumannVelocity(neumannVelocity_), neumannPressure(neumannPressure_),
          gridView(gridView_), eddyViscosityModel(eddyViscosityModel_),
          mapperElement(gridView_), mapperIntersection(gridView_)

      {
        initialize(gridView);
      }

      /**
       * \brief Contribution to volume integral.
       *
       * Volume integral depending on test and ansatz functions.
       *
       * \tparam EG element geometry
       * \tparam LFSU local function space for ansatz functions
       * \tparam X coefficient vector
       * \tparam LFSV local function space for test functions
       * \tparam R residual vector
       * \param eg element geometry
       * \param lfsu local functions space for ansatz functions
       * \param x coefficient vector
       * \param lfsv local function space for test functions
       * \param r residual vector
       */
      template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_volume(const EG& eg, const LFSU& lfsu, const X& x,
                        const LFSV& lfsv, R& r) const
      {
        ParentType::alpha_volume(eg, lfsu, x, lfsv, r);
      }


      /**
       * \brief Skeleton integral depending on test and ansatz functions.
       *
       * Contribution of flux over interface. Each face is only visited once!
       *
       * \tparam IG interface geometry over which the fluxes are evaluated
       * \tparam LFSU local function space for ansatz functions
       * \tparam X coefficient vector
       * \tparam LFSV local function space for test functions
       * \tparam R residual vector
       * \param ig interface geometry
       * \param lfsu_s local functions space for ansatz functions of self/inside
       * \param x_s coefficient vector of self/inside
       * \param lfsv_s local function space for test functions of self/inside
       * \param lfsu_n local functions space for ansatz functions of neighbor/outside
       * \param x_n coefficient vector of neighbor/outside
       * \param lfsv_n local function space for test functions of neighbor/outside
       * \param r_s residual vector of self/inside
       * \param r_n residual vector of neighbor/outside
       */
      template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_skeleton(const IG& ig,
                          const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s,
                          const LFSU& lfsu_n, const X& x_n, const LFSV& lfsv_n,
                          R& r_s, R& r_n) const
      {
        ParentType::alpha_skeleton(ig, lfsu_s, x_s, lfsv_s, lfsu_n, x_n,  lfsv_n, r_s, r_n);
      }

      /**
       * \brief Boundary integral depending on test and ansatz functions
       *
       * We put the Dirchlet evaluation also in the alpha term to save
       * some geometry evaluations.
       *
       * \tparam IG interface geometry over which the fluxes are evaluated
       * \tparam LFSU local function space for ansatz functions
       * \tparam X coefficient vector
       * \tparam LFSV local function space for test functions
       * \tparam R residual vector
       * \param ig interface geometry
       * \param lfsu_s local functions space for ansatz functions
       * \param x_s coefficient vector
       * \param lfsv_s local function space for test functions
       * \param r_s residual vector
       */
      template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_boundary(const IG& ig, const LFSU& lfsu_s, const X& x_s,
                          const LFSV& lfsv_s, R& r_s) const
      {
        ParentType::alpha_boundary(ig, lfsu_s, x_s, lfsv_s, r_s);
      }

      /**
       * \brief Initialize vector-stored values used for alpha_* routines.
       *
       * Travers grid and store values like relation to next wall.<br>
       * In a first step, this method writes the ID and coordinates of all elements
       * having a boundary face into a vector. In the second traversing grid step,
       * to each individual elements the relation to it's shortest corresponding
       * wall element is found and stored.
       *
       * \tparam GridView GridView type
       */
      void initialize(const GridView& gridView)
      {
        ParentType::initialize(gridView);
//         std::cout << "initialize zeroeq" << std::endl;

        std::cout << "Used eddy viscosity model = ";
        switch (eddyViscosityModel)
        {
          case 0:
            std::cout << "no eddy viscosity model" << std::endl;
            break;
          case 1:
            std::cout << "prandtl" << std::endl;
            break;
          case 2:
            std::cout << "1st modification" << std::endl;
            break;
          case 3:
            std::cout << "2nd modification" << std::endl;
            break;
          case 4:
            std::cout << "3rd modification" << std::endl;
            break;
          case 5:
            std::cout<< "Cebecci Smith Model"<<std::endl;
            break;
          case 6:
            std::cout<< "Baldwin Lomax Model"<<std::endl;
            break;
          case 7:
            std::cout<< "CebecciSmithother"<<std::endl;
           break; 
          case 9:
            std::cout << "linear" << std::endl;
            break;
          default:
            DUNE_THROW(Dune::NotImplemented, "This eddy viscosity method is not implemented.");
        }

        storedCorrespondingWallElementID.resize(mapperElement.size());
        storedCorrespondingWallGlobal.resize(mapperElement.size());
        storedDistanceToWall.resize(mapperElement.size());
        storedVelMax.resize(mapperElement.size());
        storedym.resize(mapperElement.size());
        storedviscouter.resize(mapperElement.size());
        storedviscinner.resize(mapperElement.size());
        storedcomp.resize(mapperElement.size());
        storedvelthick.resize(mapperElement.size());
        storedomegamax.resize(mapperElement.size());
        storedymax.resize(mapperElement.size());
        storedFmax.resize(mapperElement.size());
        storedvelymax.resize(mapperElement.size());
        storedsign.resize(mapperElement.size());
        storedMixing.resize(mapperElement.size());
        storedshearvel.resize(mapperElement.size());
        storedyplus.resize(mapperElement.size());
        storededdy.resize(mapperElement.size());
        storedstress.resize(mapperElement.size());
        storedBound.resize(mapperElement.size());
        storedfunc1.resize(mapperElement.size());
        storedFmaxi.resize(mapperElement.size());
        storedfwake.resize(mapperElement.size());
        storedvell.resize(mapperElement.size());
                
                
//        storedsign1.resize(mapperElement.size());
 //storedmixfrFmax.resize(mapperElement.size());

        


        // constants and types
        const int dim = GridView::dimension;
        typedef typename GridView::Traits::template Codim<0>::Iterator ElementIterator;
        typedef typename GridView::IntersectionIterator IntersectionIterator;
        typedef double RF;
        typedef double DF;

        // stored velocities are only needed for tangential case of viscous term
        // which is only the case for dim > 1
        if (dim == 1)
        {
          return;
        }

        // vector containing element id's of boundary elements
        std::vector<typename GridView::IndexSet::IndexType> wallElements(0);
        std::vector<Dune::FieldVector<DF, dim>> wallGlobals(0);
        wallElements.clear();
        wallGlobals.clear();
                                                                         
                                                                                                                                                                      
                                                                                              
        storedPressureGradient.resize(mapperElement.size());                                                                                                          
                                                                                                                                                                      
        for (unsigned int i = 0; i < mapperElement.size(); ++i)                                                                                                       
        {                                                                                                                                                       
          for (unsigned int j = 0; j < dim; ++j)                                                                                                                      
          {                                                                                                                                                           
            storedPressureGradient[i][j] = 0.0;                                                                                                                       
          }
        }
        
        // loop over grid view to get elements with a wall intersection
        for (ElementIterator eit = gridView.template begin<0>();
            eit != gridView.template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.map(*eit);
          storedCorrespondingWallGlobal[elementInsideID] = 0.0;

          for (IntersectionIterator is = gridView.ibegin(*eit);
              is != gridView.iend(*eit); ++is)
          {
            //! \todo this function for calculating wall distances has to be improved, by using the
            //!       the BCTypes
            if (is->neighbor() // has neighboring element
                || std::abs(is->centerUnitOuterNormal()[1-WALL_NORMAL_AXIS]) > 1e-6 // outer normal is not wall axis normal
                || !((BBOXMIN_IS_WALL && (is->centerUnitOuterNormal()[WALL_NORMAL_AXIS] > 1e-6))
                     || (BBOXMAX_IS_WALL && (is->centerUnitOuterNormal()[WALL_NORMAL_AXIS] < -1e-6)))
                   // neither bboxmin is wall and direction of normal fits, nor it is the case for bboxmax
               )
            {
              continue;
            }
            wallElements.push_back(elementInsideID);
            wallGlobals.push_back(is->geometry().global(0.5));
            break;
          }
        }

        // loop over grid view
        for (ElementIterator eit = gridView.template begin<0>();
            eit != gridView.template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.map(*eit);

          DF distanceToWall = 1e10;
          Dune::FieldVector<DF, dim> temp;
          for (unsigned int i = 0; i < wallGlobals.size(); ++i)
          {
            temp = eit->geometry().center();
            temp -= wallGlobals[i];
            if (temp.two_norm() < distanceToWall)
            {
              distanceToWall = temp.two_norm();
              storedCorrespondingWallElementID[elementInsideID] = wallElements[i];
              storedCorrespondingWallGlobal[elementInsideID] = wallGlobals[i];
            }
          }
          storedDistanceToWall[elementInsideID] =
            std::abs(eit->geometry().center()[WALL_NORMAL_AXIS] - storedCorrespondingWallGlobal[elementInsideID][WALL_NORMAL_AXIS]);
        }
      }

      /**
       * \brief Update vector-stored values used for alpha_* routines.
       *
       * In a first step, this method writes the ID and coordinates of all elements
       * having a boundary face into a vector. In the second traversing grid step,
       * to each individual elements the relation to it's shortest corresponding
       * wall element is found and stored.
       * The velocities are copied across each intersection to be available on the other
       * side adjacent face. This is only necessary for the tangential case of the viscous
       * term, which means <tt>dim > 1</tt>.
       * For the eddy viscosity models also the velocity gradient tensor containing
       * the gradient of each velocity component in all spatial dimension is stored
       * per element.
       *
       * \tparam GFS GridFunctionSpace type
       * \tparam X Coefficient vector
       * \param gfs Grid function space
       * \param lastSolution Coefficient vector from last Newton step or time step
       */
      template<typename GFS, typename X>
      void updateStoredValues(const GFS& gfs, X& lastSolution)
      {
    	  
    	  
    	  
          const int dim = GridView::dimension;
          typedef typename GridView::Traits::template Codim<0>::Iterator ElementIterator;
          typedef typename GridView::IntersectionIterator IntersectionIterator;
    	  
          // make local function space
          typedef Dune::PDELab::LocalFunctionSpace<GFS> LFS;
          typedef typename LFS::template Child<pressureIdx>::Type LFS_P;
          typedef Dune::PDELab::LFSIndexCache<LFS> LFSCache;
          typedef typename X::template ConstLocalView<LFSCache> XView;

          typedef typename LFS_P::Traits::FiniteElementType::
            Traits::LocalBasisType::Traits FETraits;
          typedef typename FETraits::DomainFieldType DF;
          typedef typename FETraits::RangeFieldType RF;
          typedef typename FETraits::RangeType RangeType;

          LFS lfs(gfs);
          LFSCache lfsCache(lfs);
          XView xView(lastSolution);
          std::vector<RF> xLocal(lfs.maxSize());
          std::vector<RangeType> basisLocal(lfs.maxSize());

          for (ElementIterator eit = gfs.gridView().template begin<0>();
               eit != gfs.gridView().template end<0>(); ++eit)
          {
			  const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.map(*eit);
			  for (unsigned int i = 0; i < dim; ++i)
			  {
				storedPressureGradient[elementInsideID][i] = 0.0;
			  }
			  for (IntersectionIterator is = gridView.ibegin(*eit);
				  is != gridView.iend(*eit); ++is)
			  {
				if (is->neighbor())
				{
				  // evaluate orientation of intersection
				  unsigned int normDim = 0;
				  for (unsigned int curDim = 0; curDim < dim; ++curDim)
				  {
					if (std::abs(is->centerUnitOuterNormal()[curDim]) > 1e-10)
					{
					  normDim = curDim;
					}
				  }
	
				  // local position of cell and face centers
				  const Dune::FieldVector<DF, dim>& insideCellCenterLocal =
					Dune::ReferenceElements<DF, dim>::general(is->inside()->type()).position(0, 0);
				  const Dune::FieldVector<DF, dim>& outsideCellCenterLocal =
					Dune::ReferenceElements<DF, dim>::general(is->outside()->type()).position(0, 0);
	
				  // global position of cell and face centers
				  Dune::FieldVector<DF, dim> insideCellCenterGlobal =
					is->inside()->geometry().global(insideCellCenterLocal);
				  Dune::FieldVector<DF, dim> outsideCellCenterGlobal =
					is->outside()->geometry().global(outsideCellCenterLocal);
	
				  // bind local function space to inside element
				  lfs.bind(*is->inside());
				  LFS_P lfs_p_s = lfs.template child<pressureIdx>();
				  lfsCache.update();
				  xView.bind(lfsCache);
				  xView.read(xLocal);
				  xView.unbind();
	
				  // evaluate solution bound to grid function space for face mid-points
				  RangeType pressure_s(0.0);
				  lfs_p_s.finiteElement().localBasis().evaluateFunction(insideCellCenterGlobal, basisLocal);
				  for (unsigned int i = 0; i < lfs_p_s.size(); ++i)
				  {
					pressure_s.axpy(xLocal[lfs_p_s.localIndex(i)], basisLocal[i]);
				  }
	
				  // bind local function space to outside element
				  lfs.bind(*is->outside());
				  LFS_P lfs_p_n = lfs.template child<pressureIdx>();
				  lfsCache.update();
				  xView.bind(lfsCache);
				  xView.read(xLocal);
				  xView.unbind();
	
				  // evaluate solution bound to grid function space for face mid-points
				  RangeType pressure_n(0.0);
				  lfs_p_n.finiteElement().localBasis().evaluateFunction(outsideCellCenterGlobal, basisLocal);
				  for (unsigned int i = 0; i < lfs_p_n.size(); ++i)
				  {
					pressure_n.axpy(xLocal[lfs_p_n.localIndex(i)], basisLocal[i]);
				  }
	
				  storedPressureGradient[elementInsideID][normDim] += (pressure_n - pressure_s)
																	  / (insideCellCenterGlobal[normDim] - outsideCellCenterGlobal[normDim]);
				}
			  }


          //! \bug correct values for elements without a boundary (two values have been summed up)
          //! worked only with this strange procedure
          for (IntersectionIterator is = gridView.ibegin(*eit);
              is != gridView.iend(*eit); ++is)
          {
            if (is->neighbor())
            {
              // evaluate orientation of intersection
              unsigned int normDim = 0;
              for (unsigned int curDim = 0; curDim < dim; ++curDim)
              {
                if (std::abs(is->centerUnitOuterNormal()[curDim]) > 1e-10)
                {
                  normDim = curDim;
                }
              }
              storedPressureGradient[elementInsideID][normDim] /= 2.0;
            }
          }
          // strange procedure
          for (unsigned int i = 0; i < dim; ++i)
          {
            storedPressureGradient[elementInsideID][i] *= 2.0;
          }
    	  
          }
    	  
    	  
    	  
    	  
    	  
    	  
    	  
    	  
    	  
    	  
    	  
    	  
    	  
    	  
    	  
    	  
        ParentType::updateStoredValues(gfs, lastSolution);
//         std::cout << "updateStoredValues zeroeq" << std::endl;

        typedef typename GridView::Traits::
        template Codim<0>::Iterator ElementIterator;
    
 // typedef std::vector<Dune::FieldVector<double, GridView::dimension> >velocities;
      
          // Loop to find velocity
//        Dune::FieldVector<double, GridView::dimension> storedVelmax;
        //Dune::FieldVector<double, GridView::dimension> storedElementCentersGlobal;
        for (int i = 0; i < storedVelMax.size(); ++i)
        {
            storedVelMax[i][0] = 0.0;
            storedVelMax[i][1] = 0.0;
        }

        for (ElementIterator eit = gfs.gridView().template begin<0>();
             eit != gfs.gridView().template end<0>(); ++eit)
        {
        
               
             Dune::FieldVector<double, GridView::dimension> cellCenterLocal(0.5);
             Dune::FieldVector<double, GridView::dimension> cellCenterGlobal = eit->geometry().global(cellCenterLocal);

      
             double eps = 0.1;
             double velocityGradient = this->storedVelocityGradientTensor[mapperElement.map(*eit)][1-WALL_NORMAL_AXIS][WALL_NORMAL_AXIS];
             double wallDistance = storedDistanceToWall[mapperElement.map(*eit)];
             double kinematicEddyViscosity = this->storedKinematicEddyViscosity[mapperElement.map(*eit)];
             const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.map(*eit);
             //double wallDistance = storedDistanceToWall[elementInsideID];
             const typename GridView::IndexSet::IndexType wallelementInsideID = storedCorrespondingWallElementID[elementInsideID];
             //double velocityGradient = this->storedVelocityGradientTensor[elementInsideID][1-WALL_NORMAL_AXIS][WALL_NORMAL_AXIS];
        //     Dune::FieldVector<double, GridView::dimension> velocities = this->storedVelocitiesAtElementCenter[elementInsideID][0];
             double velocities = this->storedVelocitiesAtElementCenter[elementInsideID][0];
             if (velocities > storedVelMax[wallelementInsideID][0])
             {
                 storedVelMax[wallelementInsideID][0] = velocities;
          
             }
  
             //for(i=0;i<32;i++)
             //if (storedVelocitiesAtElementCenter[i]>a)
           //   a=store +            storedVelocitiesAtElementCenter[elementIdInside][curDim] = (vRight[curDim] + vLeft[curDim]) / 2.0;dVelocitiesAtElementCenter[i];
           //   ll=storedElementCentersGlobal[elementinsideID];
             //double mixingLength =  karmanConstant*wallDistance;
          
             //this->storedKinematicEddyViscosity[elementInsideID]
           //        = mixingLength * mixingLength * std::abs(velocityGradient);
        }
   
   
        for (int i = 0; i < storedym.size(); ++i)
                       {
                           storedym[i] = 0.0;
                  
                       }
        for (int i = 0; i < storedcomp.size(); ++i)
                              {
                                  storedcomp[i] = 1e6;
                         
                              }
        for (int i = 0; i < storedvelthick.size(); ++i)
                              {
                                  storedvelthick[i] = 0;
                               
                              }
        for (int i = 0; i < storedomegamax.size(); ++i)
                                          {
                 storedomegamax[i] = 0;
                                     
                                          }
             for (int i = 0; i < storedymax.size(); ++i)
                                                  {
                         storedymax[i] = 0;
                                             
                                                  }
             for (int i = 0; i < storedFmax.size(); ++i)
                                                  {
                         storedFmax[i] = 0;
                                             
                                                  }
             for (int i = 0; i < storedvelymax.size(); ++i)
                                                  {
                         storedvelymax[i] =0;
                                             
                                                  }
             for (int i = 0; i < storedsign.size(); ++i)
                                                              {
                                     storedsign[i] =0;
                                                         
                                                              }
             for (int i = 0; i < storedMixing.size(); ++i)
                                                                          {
                                                 storedMixing[i] =0;
                                                                     
                                                                          }
             for (int i = 0; i < storedshearvel.size(); ++i)
                                                              {
                                     storedshearvel[i] =0;
                                                         
                                                              }
             for (int i = 0; i < storedyplus.size(); ++i)
                                                               {
                 storedyplus[i] =0;
                                                          
                                                               }
             for (int i = 0; i < storededdy.size(); ++i)
                                                                            {
            	 storededdy[i] =0;
                                                                       
                                                                            }
             for (int i = 0; i < storedstress.size(); ++i)
                                                                                        {
            	 storedstress[i] =0;
                                                                                   
                                                                                        }
             for (int i = 0; i < storedBound.size(); ++i)
                                                                                                 {
            	 storedBound[i] =0;
                                                                                            
                                                                                                 }
             for (int i = 0; i < storedfunc1.size(); ++i)
                                                                                                             {
                        	 storedfunc1[i] =0;
                                                                                                        
                                                                                                             }
             for (int i = 0; i < storedFmaxi.size(); ++i)
                                                                                                                        {
                                   	storedFmaxi[i] =0;
                                                                                                                   
                                                                                                                        }

             for (int i = 0; i < storedfwake.size(); ++i)
                                                                                                                        {
                                   	storedfwake[i] =0;
                                                                                                                   
                                                                                                                        }
             for (int i = 0; i < storedvell.size(); ++i)
                                                                                                                                    {
                                               	storedfwake[i] =0;
                                                                                                                               
                                                                                                                                    }
                     //storedmixfrFmax=0;
         //storedmixfrFmax=0;
 //storedomegamax=0;
        // loop over grid view
//          
//             for (ElementIterator eit = gfs.gridView().template begin<0>();
//                         eit != gfs.gridView().template end<0>(); ++eit)
//                     {
//                          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.map(*eit);
//                       switch (eddyViscosityModel)
//                                {
//                                case 0:
//                                              this->storedKinematicEddyViscosity[elementInsideID] = 0.0;
//                                              break;
//                                            case 1:
//                                              break;
//                                            case 2:
//                                              break;
//                                            case 3:
//                                              break;
//                                            case 4:
//                                              break;
//                                            case 5:                                             
//                                              break;
//                                            case 6: 
//                                                break;
//                                            case 7:
//                                          determinevelthick(eit, gfs, lastSolution);
//                                              break;
//                                            case 9:
//                                              break;
//                                            default:
//                                              DUNE_THROW(Dune::NotImplemented, "This eddy viscosity method is not implemented.");
//
//                                }
//                     }
        for (ElementIterator eit = gfs.gridView().template begin<0>();
            eit != gfs.gridView().template end<0>(); ++eit)
        {
             const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.map(*eit);
          switch (eddyViscosityModel)
                   {
                   case 0:
                                 this->storedKinematicEddyViscosity[elementInsideID] = 0.0;
                                 break;
                               case 1:
                                 break;
                               case 2:
                                 break;
                               case 3:
                                 break;
                               case 4:
                                 break;
                               case 5:
                                 determinevelthick(eit, gfs, lastSolution);
                                 break;
                               case 6:
                                 determineymax(eit, gfs, lastSolution);
                                 break;
//                               case 7:
//                              // determinevelthick(eit, gfs, lastSolution);
//                                 storesignvalue(eit, gfs, lastSolution);
//                                 break;
                               case 9:
                                 break;
                               default:
                                 DUNE_THROW(Dune::NotImplemented, "This eddy viscosity method is not implemented.");

                   }
        }
        for (ElementIterator eit = gfs.gridView().template begin<0>();
                   eit != gfs.gridView().template end<0>(); ++eit)
               {
                    const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.map(*eit);
                 switch (eddyViscosityModel)
                          {
                          case 0:
                                        this->storedKinematicEddyViscosity[elementInsideID] = 0.0;
                                        break;
                                      case 1:
                                        break;
                                      case 2:
                                        break;
                                      case 3:
                                        break;
                                      case 4:
                                        break;
                                      case 5:
                                        determineym(eit, gfs, lastSolution);
                                        break;
                                      case 6:
                                        determineymBald(eit, gfs, lastSolution);
                                        break;
//                                      case 7:
//                                        determineymCebother(eit, gfs, lastSolution);
//                                        break;
                                      case 9:
                                        break;
                                      default:
                                        DUNE_THROW(Dune::NotImplemented, "This eddy viscosity method is not implemented.");

                          }
               }
            
        for (ElementIterator eit = gfs.gridView().template begin<0>();
                    eit != gfs.gridView().template end<0>(); ++eit)
                {
             const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.map(*eit);
          switch (eddyViscosityModel)
          {
          case 0:
                        this->storedKinematicEddyViscosity[elementInsideID] = 0.0;
                        break;
                      case 1:
                        prandtlMixingLength(eit, gfs, lastSolution);
                        break;
                      case 2:
                        prandtlMixingLengthmod(eit, gfs, lastSolution);
                        break;
                      case 3:
                        prandtlMixingLengthmod2(eit, gfs, lastSolution);
                        break;
                      case 4:
                        prandtlMixingLengthmod3(eit, gfs, lastSolution);
                        break;
                      case 5:
                        CebecciSmithmodel(eit, gfs, lastSolution);
                        break;
                      case 6:
                       BaldwinLomaxmodel(eit, gfs, lastSolution);
                        break;
//                      case 7:
//                       CebecciSmithmodel(eit, gfs, lastSolution);
//                        break;
                      case 9:
                        linear(eit, gfs, lastSolution);
                        break;
                      default:
                        DUNE_THROW(Dune::NotImplemented, "This eddy viscosity method is not implemented.");

          }
#if PRINT_EDDY_VISCOSITY
Dune::FieldVector<double, GridView::dimension> cellCenterLocal(0.5);
Dune::FieldVector<double, GridView::dimension> cellCenterGlobal = eit->geometry().global(cellCenterLocal);

double eps = 0.1;
double velocityGradient = this->storedVelocityGradientTensor[mapperElement.map(*eit)][1-WALL_NORMAL_AXIS][WALL_NORMAL_AXIS];
double wallDistance = storedDistanceToWall[mapperElement.map(*eit)];
double kinematicEddyViscosity = this->storedKinematicEddyViscosity[mapperElement.map(*eit)];
if ((cellCenterGlobal[0] > 5.0 - eps) && (cellCenterGlobal[0] < 5.0 + eps))
{
  std::cout << "EDDY VISCOSITY @" <<
  " cellCenterGlobal: " << cellCenterGlobal <<
  " velocityGradient: " << velocityGradient <<
  " wallDistance: " << wallDistance <<
  " kinematicEddyViscosity: " << kinematicEddyViscosity <<
  std::endl;
}
#endif // PRINT_EDDY_VISCOSITY
        }
      }

      /**linear
       * \brief Calculates the eddy viscosity based on Prandtl's mixing length approach
       *
       * \f[ \nu_\textrm{t} = l_\textrm{mix}^2 \left| \frac{\partial u}{\partial y} \right| \f]
       * with \f$ l_\textrm{mix} = \kappa y \f$ and \f$ \kappa = 0.4 \f$.
       *
       */
 //yy=0;
      template<typename ElementIterator, typename GFS, typename X>
      void prandtlMixingLength(const ElementIterator eit, const GFS& gfs, X& lastSolution)
      {
        const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.map(*eit);

        double wallDistance = storedDistanceToWall[elementInsideID];
        double velocityGradient = this->storedVelocityGradientTensor[elementInsideID][1-WALL_NORMAL_AXIS][WALL_NORMAL_AXIS];
        double mixingLength =  karmanConstant*wallDistance;
        storedMixing[elementInsideID]=mixingLength;
        this->storedKinematicEddyViscosity[elementInsideID]
          = mixingLength * mixingLength * std::abs(velocityGradient);
     
      }
     
                                                                  /*****     START OF VANDRIEST MODEL IMPLEMENTATION (1ST MODIFICATION)    *********/                                      

         
     
   
   
      double wallviscosity= KINEMATIC_VISCOSITY;
      double viscosity= KINEMATIC_VISCOSITY;
//      double yy;
      ///Modification by Van Driest for prandtl mixing length model
      ///Implementation of the 1st modification of the Prandtl mixing length model
   
      template<typename ElementIterator, typename GFS, typename X>
           void prandtlMixingLengthmod(const ElementIterator eit, const GFS& gfs, X& lastSolution)
           {
      //std::cout<<"Using prandlt test"<<std::endl;
             const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.map(*eit);
             double wallDistance = storedDistanceToWall[elementInsideID];
             double velocityGradient = this->storedVelocityGradientTensor[elementInsideID][1-WALL_NORMAL_AXIS][WALL_NORMAL_AXIS];
             const typename GridView::IndexSet::IndexType wallelementInsideID = storedCorrespondingWallElementID[elementInsideID];
             double wallvelocityGradient = this->storedVelocityGradientTensor[wallelementInsideID][1-WALL_NORMAL_AXIS][WALL_NORMAL_AXIS];
             double shear_vel= sqrt(wallviscosity*abs(wallvelocityGradient));
             double yy=shear_vel*wallDistance/viscosity;
             double mixingLength =  karmanConstant*wallDistance*(1-exp(-yy/26));
             storedMixing[elementInsideID]=mixingLength;
             storedshearvel[elementInsideID]=this->storedVelocitiesAtElementCenter[elementInsideID][0]/shear_vel;
             storedyplus[elementInsideID]=yy;
             storedstress[elementInsideID] = -mixingLength * mixingLength*velocityGradient*velocityGradient;
             storededdy[elementInsideID] = mixingLength * mixingLength * std::abs(velocityGradient);
             this->storedKinematicEddyViscosity[elementInsideID]
               = mixingLength * mixingLength * std::abs(velocityGradient);
           }
     
     

                                                                 /*****     END OF VANDRIEST MODEL IMPLEMENTATION (1ST  MODIFICATION)    *********/                                      

          
          
          
                                                                 /*****     START OF ESCUDIER MODEL IMPLEMENTATION (2ND MODIFICATION)    *********/                                      
     
     

      template<typename ElementIterator, typename GFS, typename X>
      void prandtlMixingLengthmod2(const ElementIterator eit, const GFS& gfs, X& lastSolution)
      {
       const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.map(*eit);
          
           double wallDistance = storedDistanceToWall[elementInsideID];
           double velocityGradient = this->storedVelocityGradientTensor[elementInsideID][1-WALL_NORMAL_AXIS][WALL_NORMAL_AXIS];
           const typename GridView::IndexSet::IndexType wallelementInsideID = storedCorrespondingWallElementID[elementInsideID];
           double mixingLength =  karmanConstant*wallDistance;
           double Rex= storedVelMax[wallelementInsideID][0] * this->storedElementCentersGlobal[elementInsideID][0]/viscosity;
           //double boundthick = 0.382* this->storedElementCentersGlobal[elementInsideID][0]/pow(Rex,0.2);
           double boundthick = std::fmin(PIPE_HEIGHT*0.5,0.382* this->storedElementCentersGlobal[elementInsideID][0]/pow(Rex,0.2));
           storedBound[elementInsideID]=boundthick;
           double wallvelocityGradient = this->storedVelocityGradientTensor[wallelementInsideID][1-WALL_NORMAL_AXIS][WALL_NORMAL_AXIS];
                        double shear_vel= sqrt(wallviscosity*abs(wallvelocityGradient));
                        double yy=shear_vel*wallDistance/viscosity;
           double bb=0.09*boundthick;
           if (mixingLength>bb)
           {
              mixingLength = bb;
              //std::cout<<""<<mixingLength;
           }
           storedMixing[elementInsideID]=mixingLength;
           storedshearvel[elementInsideID]=this->storedVelocitiesAtElementCenter[elementInsideID][0]/shear_vel;
           storedyplus[elementInsideID]=yy;
           //storedshearvel[elementInsideID]=this->storedVelocitiesAtElementCenter[elementInsideID][0]/shear_vel;
                        //storedyplus[elementInsideID]=yy;
                        storedstress[elementInsideID] = -mixingLength * mixingLength*velocityGradient*velocityGradient;
                        storededdy[elementInsideID] = mixingLength * mixingLength * std::abs(velocityGradient);
           this->storedKinematicEddyViscosity[elementInsideID]
                       = mixingLength * mixingLength * std::abs(velocityGradient);
      }
     
     
     
     
                                                                  /*****     END OF ESCUDIER MODEL IMPLEMENTATION (2ND MODIFICATION)    *********/                                      




                                                                  /*****     START OF KIRSTEN MODEL IMPLEMENTATION (3RD MODIFICATION)    *********/                                      

     
     
     
     // Dune::FieldVector<double, GridView::dimension> storedVelmax;
      template<typename ElementIterator, typename GFS, typename X>
                void prandtlMixingLengthmod3(const ElementIterator eit, const GFS& gfs, X& lastSolution)
                {
                 const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.map(*eit);
                        
                     double wallDistance = storedDistanceToWall[elementInsideID];
                     double velocityGradient = this->storedVelocityGradientTensor[elementInsideID][1-WALL_NORMAL_AXIS][WALL_NORMAL_AXIS];
                     const typename GridView::IndexSet::IndexType wallelementInsideID = storedCorrespondingWallElementID[elementInsideID];
                     double wallvelocityGradient = this->storedVelocityGradientTensor[wallelementInsideID][1-WALL_NORMAL_AXIS][WALL_NORMAL_AXIS];
                                  double shear_vel= sqrt(wallviscosity*abs(wallvelocityGradient));
                                  double yy=shear_vel*wallDistance/viscosity;
                     double mixingLength =  karmanConstant*wallDistance;
                     double Rex= storedVelMax[wallelementInsideID][0] * this->storedElementCentersGlobal[elementInsideID][0]/viscosity;
                     //double boundthick = 0.382* this->storedElementCentersGlobal[elementInsideID][0]/pow(Rex,0.2);
                     double boundthick = std::fmin(PIPE_HEIGHT*0.5,0.382* this->storedElementCentersGlobal[elementInsideID][0]/pow(Rex,0.2));
                     storedBound[elementInsideID]=boundthick;
                     double func = 1.0 + 5.5*pow((wallDistance/boundthick),6.0); 
                     storedfunc1[elementInsideID]=1/func;
                     storedshearvel[elementInsideID]=this->storedVelocitiesAtElementCenter[elementInsideID][0]/shear_vel;
                     storedyplus[elementInsideID]=yy;
                     this->storedKinematicEddyViscosity[elementInsideID]
                                 = mixingLength/func * mixingLength * std::abs(velocityGradient);
                }
     
     
     
                                                                  /*****     END OF KIRSTEN MODEL IMPLEMENTATION (3RD MODIFICATION)     *********/



                                                                                   /*****    START OF CEBECCI SMITH MODEL IMPLEMENTATION     *********/
                       
     
     
     
    //  double ym=1;
      template<typename ElementIterator, typename GFS, typename X>
                            void determinevelthick(const ElementIterator eit, const GFS& gfs, X& lastSolution)
                            {
              
                const unsigned int numControlVolumeFaces =
                                                     Dune::ReferenceElements<double/*DF*/, 2/*dim*/>::general(eit->geometry().type()).size(1);
                                                     std::vector<Dune::FieldVector<double/*DF*/, 2/*dim*/> > faceCentersLocal(numControlVolumeFaces);
                                                     std::vector<Dune::FieldVector<double/*DF*/, 2/*dim*/> > faceCentersGlobal(numControlVolumeFaces);
                                                     for (unsigned int curFace = 0; curFace < numControlVolumeFaces; ++curFace)
                                                     {
                                                       std::fill(faceCentersLocal[curFace].begin(), faceCentersLocal[curFace].end(), 0.5);
                                                       faceCentersLocal[curFace][curFace/2] = curFace % 2;
                                                       faceCentersGlobal[curFace] = eit->geometry().global(faceCentersLocal[curFace]);
                                                     }
                                   
                                                     // calculate the staggered face volume (goes through cell center) perpendicular to each direction
                                                     Dune::FieldVector<double/*RF*/, 2/*dim*/> orthogonalFaceVolumes(0.0);
                                                     switch (2/*dim*/)
                                                     {
                                                       case 1:
                                                         orthogonalFaceVolumes[0] = 1;
                                                         break;
                                                       case 2:
                                                         orthogonalFaceVolumes[0] = std::abs(faceCentersGlobal[3][1] - faceCentersGlobal[2][1]);
                                                         orthogonalFaceVolumes[1] = std::abs(faceCentersGlobal[1][0] - faceCentersGlobal[0][0]);
                                                         break;
                                                       default:
                                                         DUNE_THROW(Dune::NotImplemented, "Staggered grid is not implemented for 3D cases.");
                                                     }
                                                    const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.map(*eit);
                                                    double wallDistance = storedDistanceToWall[elementInsideID];
                                                    double velocityGradient = this->storedVelocityGradientTensor[elementInsideID][1-WALL_NORMAL_AXIS][WALL_NORMAL_AXIS];
                                                    const typename GridView::IndexSet::IndexType wallelementInsideID = storedCorrespondingWallElementID[elementInsideID];
                                                   double Rex= storedVelMax[wallelementInsideID][0]* this->storedElementCentersGlobal[elementInsideID][0]/viscosity;
                                                    double boundthick = std::fmin(PIPE_HEIGHT*0.5,0.382* this->storedElementCentersGlobal[elementInsideID][0]/pow(Rex,0.2));
                                                  
                                                    if (wallDistance <= boundthick)
                                                    {
                                                   storedvelthick[wallelementInsideID] += orthogonalFaceVolumes[1]*(1-(this->storedVelocitiesAtElementCenter[elementInsideID][0]/storedVelMax[wallelementInsideID][0]));
                                                    }
                                                  // std::cout<<"orthog "<<orthogonalFaceVolumes[1]<<std::endl;
                            }
            double compi;
   
//       Dune::FieldVector<double, GridView::dimension> storedVelmax;

                 template<typename ElementIterator, typename GFS, typename X>
                 void determineym(const ElementIterator eit, const GFS& gfs, X& lastSolution)
                 {
                  
                                    
                       const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.map(*eit);
                        double wallDistance = storedDistanceToWall[elementInsideID];
                        double velocityGradient = this->storedVelocityGradientTensor[elementInsideID][1-WALL_NORMAL_AXIS][WALL_NORMAL_AXIS];
                        const typename GridView::IndexSet::IndexType wallelementInsideID = storedCorrespondingWallElementID[elementInsideID];
                               
                        double Rex= storedVelMax[wallelementInsideID][0]* this->storedElementCentersGlobal[elementInsideID][0]/viscosity;
                        double boundthick = std::fmin(PIPE_HEIGHT*0.5,0.385* this->storedElementCentersGlobal[elementInsideID][0]/pow(Rex,0.2));
                        storedBound[elementInsideID]=boundthick;
                        double func = 1.0 + 5.5*pow((wallDistance/boundthick),6.0);
                        storedfunc1[elementInsideID]= func;
                        double wallvelocityGradient = this->storedVelocityGradientTensor[wallelementInsideID][1-WALL_NORMAL_AXIS][WALL_NORMAL_AXIS];
                        double shear_vel= sqrt(wallviscosity*abs(wallvelocityGradient));
                        double yy=shear_vel*wallDistance/viscosity;
                       double bplus=(1.0+(wallDistance*(storedPressureGradient[elementInsideID][0])/(DENSITY*shear_vel*shear_vel)));
                     double aplus=26.0/sqrt(bplus);
                                       
                        double mixingLength =  karmanConstant*wallDistance*(1.0-exp(-yy/aplus));
                        storedMixing[elementInsideID] =mixingLength;
                       // double mixingLength =  karmanConstant*wallDistance*(1.0-exp(-yy/26));
                        storedshearvel[elementInsideID]=this->storedVelocitiesAtElementCenter[elementInsideID][0]/shear_vel;
                        storedyplus[elementInsideID]=yy;

                        //std::cout<<"length="<<mixingLength<<std::endl;
                        double velgradx=this->storedVelocityGradientTensor[elementInsideID][0][1]*this->storedVelocityGradientTensor[elementInsideID][0][1];
                        double velgrady=0.0;//this->storedVelocityGradientTensor[elementInsideID][1][0]*this->storedVelocityGradientTensor[elementInsideID][1][0];
                       storedviscinner[elementInsideID] = mixingLength*mixingLength*pow((velgradx+velgrady),0.5);
                       storedvell[elementInsideID]=storedvelthick[wallelementInsideID];
                     //std::cout<<"viscinner "<<storedviscinner[elementInsideID]<<"";
                       //double velthick = orthogonalFaceVolumes[1]*(1-(this->storedVelocitiesAtElementCenter[elementInsideID][0]/storedVelMax[wallelementInsideID][0]));
                     storedviscouter[elementInsideID] = 0.0168*storedVelMax[wallelementInsideID][0]*storedvelthick[wallelementInsideID]/func;
                    //std::cout<<"visc "<<storedviscouter[elementInsideID]<<std::endl;
                         compi= storedviscouter[elementInsideID]-storedviscinner[elementInsideID];
                       compi= fabs(compi);
                    // std::cout<<"compi is "<<compi<<std::endl;
                        if (compi<storedcomp[wallelementInsideID])
                        {
                       
                            storedcomp[wallelementInsideID]=compi;
                            storedym[wallelementInsideID]=wallDistance;
                        }
                      //std::cout<<"ym is "<<storedcomp[wallelementInsideID]<<std::endl;
//                      std::cout<<"thickness "<<ym;
//                    std::cout<<"comp "<<comp;
                 }
               
             ///// Cebeci - Smith Model Implementation
             
          
             template<typename ElementIterator, typename GFS, typename X>
                 void CebecciSmithmodel(const ElementIterator eit, const GFS& gfs, X& lastSolution)
                 {
                            const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.map(*eit);
                      double wallDistance = storedDistanceToWall[elementInsideID];
                      double velocityGradient = this->storedVelocityGradientTensor[elementInsideID][1-WALL_NORMAL_AXIS][WALL_NORMAL_AXIS];
                      const typename GridView::IndexSet::IndexType wallelementInsideID = storedCorrespondingWallElementID[elementInsideID];
                      //std::cout<<"ym is "<<storedym[wallelementInsideID]<<std::endl;
                    
                           if (storedym[wallelementInsideID]<=wallDistance)
                           this->storedKinematicEddyViscosity[elementInsideID]
                                                      = storedviscouter[elementInsideID];
                      else
                           this->storedKinematicEddyViscosity[elementInsideID]
                                                      = storedviscinner[elementInsideID];
                  }
//           
            

                                                                    /*****     END OF  CEBECCI SMITH MODEL IMPLEMENTATION     *********/




                                                                      /*****     START OF BALDWIN LOMAX MODEL IMPLEMENTATION     *********/
//            
            
            
            
//           
             template<typename ElementIterator, typename GFS, typename X>
                              void determineymax(const ElementIterator eit, const GFS& gfs, X& lastSolution)
                              {
                                    const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.map(*eit);
                                     double wallDistance = storedDistanceToWall[elementInsideID];
                                     double velocityGradient = this->storedVelocityGradientTensor[elementInsideID][1-WALL_NORMAL_AXIS][WALL_NORMAL_AXIS];
                                     const typename GridView::IndexSet::IndexType wallelementInsideID = storedCorrespondingWallElementID[elementInsideID];
                                  
                                     double wallvelocityGradient = this->storedVelocityGradientTensor[wallelementInsideID][1-WALL_NORMAL_AXIS][WALL_NORMAL_AXIS];
                                     double shear_vel= sqrt(wallviscosity*abs(wallvelocityGradient));
                                     double yy=shear_vel*wallDistance/viscosity;
                                     double mixingLength =  karmanConstant*wallDistance*(1-exp(-yy/26.0));
                                     double omega= fabs(this->storedVelocityGradientTensor[elementInsideID][0][1]-this->storedVelocityGradientTensor[elementInsideID][1][0]);
                                     double jj = fabs(mixingLength)*fabs(omega);
                                     
                                     if (storedomegamax[wallelementInsideID]<jj)
                                          {
                                    // std::cout<<"omegamax"<<storedomegamax[wallelementInsideID]<<std::endl;
//                                     std::cout<<"jj is"<<jj<<std::endl;
                                             storedomegamax[wallelementInsideID]=jj;
                                             storedymax[wallelementInsideID]=wallDistance;
                                             storedFmaxi[wallelementInsideID] =jj;
                                             storedFmax[wallelementInsideID]=1.0/karmanConstant* storedomegamax[wallelementInsideID];
                                            storedvelymax[wallelementInsideID]= this->storedVelocitiesAtElementCenter[elementInsideID][0];
                                                                         }
                                     //storedfmaxii[elementInsideID]=storedFmaxi[wallelementInsideID];
                                                                                                        
//                                     std::cout<<"Fmax is"<<storedFmax[wallelementInsideID]<<std::endl;
                              }
          
             template<typename ElementIterator, typename GFS, typename X>
                                           void determineymBald(const ElementIterator eit, const GFS& gfs, X& lastSolution)
                                           {
                 const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.map(*eit);
                 	 	 	 	 	 	 	 	 	 	 	 	   
                                                                   double wallDistance = storedDistanceToWall[elementInsideID];
                                                                   double velocityGradient = this->storedVelocityGradientTensor[elementInsideID][1-WALL_NORMAL_AXIS][WALL_NORMAL_AXIS];
                                                                   const typename GridView::IndexSet::IndexType wallelementInsideID = storedCorrespondingWallElementID[elementInsideID];
                                                                   //std::cout<<"ymax is "<<storedymax[wallelementInsideID]<<std::endl;
                                                                   double func = 1.0 + 5.5*pow((wallDistance*0.3/storedymax[wallelementInsideID]),6.0);
                                                                   storedfunc1[elementInsideID]= func;
                                                                   //std::cout<<"func is "<<func<<std::endl;
                                                                   double wallvelocityGradient = this->storedVelocityGradientTensor[wallelementInsideID][1-WALL_NORMAL_AXIS][WALL_NORMAL_AXIS];
                                                                   double shear_vel= sqrt(wallviscosity*abs(wallvelocityGradient));
                                                                   double yy=shear_vel*wallDistance/viscosity;
                                                                   double mixingLength =  karmanConstant*wallDistance*(1.0-exp(-yy/26.0));
                                                                   storedMixing[elementInsideID] =mixingLength;
                                                                   //std::cout<<"length is "<<mixingLength<<std::endl;
                                 //                                  double a=this->storedVelocityGradientTensor[wallelementInsideID][1][0];
                                 //                                  double b=this->storedVelocityGradientTensor[wallelementInsideID][0][1];                                        
                                                                   double omega= (this->storedVelocityGradientTensor[elementInsideID][0][1]-this->storedVelocityGradientTensor[elementInsideID][1][0]);
                                                                   //std::cout<<"omega is "<<omega<<std::endl;
                                                                   storedviscinner[elementInsideID]=mixingLength*mixingLength*fabs(omega);
                                                                  //std::cout<<"innervisc is "<<storedviscinner[elementInsideID]<<std::endl;
                                                                   double aa = storedymax[wallelementInsideID]*storedFmax[wallelementInsideID];
                                                                  // std::cout<<"aa is "<<aa<<std::endl;
                                                                   double Udiff=(storedVelMax[wallelementInsideID][0]/*- storedvelymax[wallelementInsideID]*/);
                                                                // std::cout<<"Udiff is "<<Udiff<<std::endl;
                                                                   double bb = 1.0*storedymax[wallelementInsideID]*Udiff*Udiff/storedFmax[wallelementInsideID];
                                                                   //std::cout<<"bb is "<<bb<<std::endl;
                                                                   double Fwake = std::min(aa,bb);
                                                                   storedshearvel[elementInsideID]=this->storedVelocitiesAtElementCenter[elementInsideID][0]/shear_vel;
                                                                   storedyplus[elementInsideID]=yy;
                                                                   
                                                                   storedfwake[elementInsideID]=Fwake;
                                                                   //std::cout<<"Fwake is "<<Fwake<<std::endl;
                                                                   storedviscouter[elementInsideID] = 0.0168*1.6*Fwake/func;
                                                                 
                                                               //std::cout<<"outervisc is "<<storedviscouter[elementInsideID]<<std::endl;
                                                                   compi= storedviscouter[elementInsideID]-storedviscinner[elementInsideID];
                                                                   compi= fabs(compi);
                                                                   //std::cout<<"compi is "<<compi<<std::endl;
                                                                   if (compi<storedcomp[wallelementInsideID])
                                                                     {
                                                                       //std::cout<<"compi"<<compi<<std::endl;
                                                                            storedcomp[wallelementInsideID]=compi;
                                                                             storedym[wallelementInsideID]=wallDistance;
                                                                      }
                                                                // std::cout<<"ym is "<<storedym[wallelementInsideID]<<std::endl;
                                                                   //std::cout<<"Fmax is "<<Fmax<<std::endl;
                                                                
                                           }
          
             template<typename ElementIterator, typename GFS, typename X>
                             void BaldwinLomaxmodel(const ElementIterator eit, const GFS& gfs, X& lastSolution)
                             {
                          
                                        const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.map(*eit);
                                  double wallDistance = storedDistanceToWall[elementInsideID];
                                  double velocityGradient = this->storedVelocityGradientTensor[elementInsideID][1-WALL_NORMAL_AXIS][WALL_NORMAL_AXIS];
                                  const typename GridView::IndexSet::IndexType wallelementInsideID = storedCorrespondingWallElementID[elementInsideID];
                                 //std::cout<<"ym is "<<storedym[wallelementInsideID]<<std::endl;
                                  if (storedym[wallelementInsideID]>=wallDistance)
                                                            this->storedKinematicEddyViscosity[elementInsideID]
                                                                                       = storedviscinner[elementInsideID];
                                  else
                                                            this->storedKinematicEddyViscosity[elementInsideID]
                                                                                       = storedviscouter[elementInsideID];
                                                
                              }
            
            
            
                                                    /*****     END OF BALDWIN LOMAX MODEL IMPLEMENTATION     *********/
            
            
            
                                                          /****  IMPLEMENTATION OF OTHER CEBECCI SMITH MODEL ***/
            
             
             
//        double compii;                 
//                         
//                                   template<typename ElementIterator, typename GFS, typename X>
//                                                   void storesignvalue(const ElementIterator eit, const GFS& gfs, X& lastSolution)
//                                                           {
//                                                                                
//                                                                                                  
//                                                       const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.map(*eit);
//                                                             double wallDistance = storedDistanceToWall[elementInsideID];
//                                                                  double velocityGradient = this->storedVelocityGradientTensor[elementInsideID][1-WALL_NORMAL_AXIS][WALL_NORMAL_AXIS];
//                                                                       const typename GridView::IndexSet::IndexType wallelementInsideID = storedCorrespondingWallElementID[elementInsideID];
//                                                                                             
//                                                                            double Rex= storedVelMax[wallelementInsideID][0]* this->storedElementCentersGlobal[elementInsideID][0]/viscosity;
//                                                                       double boundthick = std::fmin(PIPE_HEIGHT*0.5,0.382* this->storedElementCentersGlobal[elementInsideID][0]/pow(Rex,0.2));
//                                                                  double func = 1.0 + 5.5*pow((wallDistance/boundthick),6.0);
//                                                             double wallvelocityGradient = this->storedVelocityGradientTensor[wallelementInsideID][1-WALL_NORMAL_AXIS][WALL_NORMAL_AXIS];
//                                                       double shear_vel= sqrt(wallviscosity*abs(wallvelocityGradient));
//                                                   double yy=shear_vel*wallDistance/viscosity;
//                                                    double mixingLength =  karmanConstant*wallDistance*(1.0-exp(-yy/26.0));
//                                                                      //std::cout<<"length="<<mixingLength<<std::endl;
//                                                    double velgradx=this->storedVelocityGradientTensor[elementInsideID][0][1]*this->storedVelocityGradientTensor[elementInsideID][0][1];
//                                                       double velgrady=0.0;//this->storedVelocityGradientTensor[elementInsideID][1][0]*this->storedVelocityGradientTensor[elementInsideID][1][0];
//                                                             storedviscinner[elementInsideID] = mixingLength*mixingLength*pow((velgradx+velgrady),0.5);
//                                                                       //std::cout<<"viscinner "<<storedviscinner[elementInsideID]<<"";
//                                                                       //double velthick = orthogonalFaceVolumes[1]*(1-(this->storedVelocitiesAtElementCenter[elementInsideID][0]/storedVelMax[wallelementInsideID][0]));
//                                                             storedviscouter[elementInsideID] = 0.0168*storedVelMax[wallelementInsideID][0]*storedvelthick[wallelementInsideID]/func;
//                                                                      //std::cout<<"visc "<<storedviscouter[elementInsideID]<<std::endl;
//                                                             //std::cout<<"visc "<<storedviscouter[elementInsideID]<<std::endl;
//                                                             compii= storedviscouter[elementInsideID]-storedviscinner[elementInsideID];
//                                                             if (compii<0)    
//                                                                {
//                                                                storedsign[elementInsideID]=0;
//                                                            ///std::cout<<"ahhh"<<std::endl;
//                                                                }
//                                                             else
//                                                                storedsign[elementInsideID]=1;
//                                                                   
//                                                                
////                                                            std::cout<<"ahhh"<<std::endl;
//                                                                   
//                                                                   
//                                                            //std::cout<<"value of wall element"<<storedsign1[wallelementInsideID]<<std::endl;
//                                                                       }                                              
//                                   template<typename ElementIterator, typename GFS, typename X>
//                                                    void determineymCebother(const ElementIterator eit, const GFS& gfs, X& lastSolution)
//                                                            {
//                                           
//                                           const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.map(*eit);
//                                             double wallDistance = storedDistanceToWall[elementInsideID];
//                                               const typename GridView::IndexSet::IndexType wallelementInsideID = storedCorrespondingWallElementID[elementInsideID];
//                                               if (storedsign[wallelementInsideID] != storedsign[elementInsideID]
//                                                   && wallDistance < storedym[wallelementInsideID])
//                                               {
//                                                         storedym[wallelementInsideID]=wallDistance;
//                                                         std::cout<<"value of ym"<<storedym[wallelementInsideID]<<std::endl;
//                                                      }
//                                              //     std::cout<<"value of ym"<<storedym[wallelementInsideID]<<std::endl;
//                                                                     }
                                           
                                           
                                                         /*****     END OF OTHER CEBECCI SMITH MODEL IMPLEMENTATION     *********/
        
                                           
                                           
      /**
       * \brief Artificial function to check implementaion
       */
      template<typename ElementIterator, typename GFS, typename X>
      void linear(const ElementIterator eit, const GFS& gfs, X& lastSolution)
      {
        const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.map(*eit);
        double wallDistance = storedDistanceToWall[elementInsideID];
        double mixingLength = karmanConstant * wallDistance;
        this->storedKinematicEddyViscosity[elementInsideID] = mixingLength;
      }

      /**
       * \brief Writes data of an array with element size into textfile
       *
       * Please use the given script <tt>simulationsOutput.sh</tt> to visualize
       * the data with gnuplot. The pictures can be found in the subfolder
       * <tt>pics</tt> after processing.
       *
       * \param time Current time
       * \param timeStep Number of the current time step
       * \param gridCells Number of grid cells of the problem
       */
      void gnuplotElementOutput(double time, unsigned int timeStep, unsigned int gridCells)
      {
#if PRINT_GNUPLOT_OUTPUT
        ParentType::gnuplotElementOutput(time, timeStep, gridCells);

        const int dim = GridView::dimension;

        std::stringstream stream("");
        stream << "gnuplotZeroEqElement_";
        stream << std::setprecision(4) << std::setw(4) << std::setfill('0') << gridCells << "-";
        stream << std::setprecision(4) << std::setw(4) << std::setfill('0') << timeStep << ".csv";
        stream.clear();
        std::string string(stream.str());
        const char* fileName = string.c_str();
        std::fstream file;
        file.open(fileName, std::ios::out);
        file << "# time: " << time << std::endl;
        file << "# ";
        for (unsigned int curDim = 0; curDim < dim; ++curDim)
        {
          file << "global" << curDim << ",";
        }
        file << "eddyViscosity" << ",";
//         file << "storedCorrespondingWallGlobal[0]" << ",";
//         file << "storedCorrespondingWallGlobal[1]" << ",";
//         file << "storedCorrespondingWallElementID" << ",";
         file << "storedDistanceToWall" << ",";
//        file << "velGrad[0][0]" << ",";
        file << "velGrad[0][1]" << ",";
//        file << "velGrad[1][0]" << ",";
//        file << "velGrad[1][1]" << ",";
        file << "vel[0]" << ",";
        file << "velMax[0]" << ",";
        file << "yM" << ",";
        file << "yMax" << ",";
        file << "Outerviscosity" << ",";
        file << "Innerviscosity" << ",";
        file << "Bound" << ",";
        file << "Mixinglength" << ",";
        file << "Function" << ",";
        file << "Yplus" << ",";
                file << "eddy" << ",";
                file << "ShearVel" << ",";
                file << "Turbulent stress" << ",";
                       
        //file << "storedsign" << ",";
        // file << "Mixinglength" << ",";
       // file << "Mixing length" << ",";
       //file << "storedsign1" << ",";
                file << "dpdx" << ",";
                file << "dpdy"<< ",";
                file << "Fmax"<< ",";
                file << "fwake"<< ",";
                file << "velthick"<< ",";
        file << std::endl;

        for (unsigned int i = 0; i < this->storedElementCentersGlobal.size(); ++i)
        {
          for (unsigned int curDim = 0; curDim < dim; ++curDim)
          {
            file << this->storedElementCentersGlobal[i][curDim] << ",";
          }
          file << this->storedKinematicEddyViscosity[i] << ",";
//           file << storedCorrespondingWallGlobal[i][0] << ",";
//           file << storedCorrespondingWallGlobal[i][1] << ",";
//           file << storedCorrespondingWallElementID[i] << ",";
           file << storedDistanceToWall[i] << ",";
//          file << this->storedVelocityGradientTensor[i][0][0] << ",";
          file << this->storedVelocityGradientTensor[i][0][1] << ",";
//          file << this->storedVelocityGradientTensor[i][1][0] << ",";
//          file << this->storedVelocityGradientTensor[i][1][1] << ",";
          file << this->storedVelocitiesAtElementCenter[i][0] << ",";
          file << storedVelMax[i][0] << ",";
          file << storedym[i] << ",";
          file << storedymax[i] << ",";
          file << storedviscouter[i] << ",";
          file << storedviscinner[i] << ",";
         // file << storedsign[i] << ",";
          file << storedBound[i] << ",";
          file << storedMixing[i] << ",";
          file << storedfunc1[i] << ",";
          file << storedyplus[i] << ",";
                  file << storededdy[i] << ",";
    
           file << storedshearvel[i] << ",";
           file << storedstress[i] << ",";
          // file << storedMixing[i] << ",";
         // file << storedsign1[i] << ",";
           file << storedPressureGradient[i][0] << ",";
           file << storedPressureGradient[i][1]<< ",";
           file <<  storedFmaxi[i]<< ",";
           file <<  storedfwake[i]<< ",";
           file << storedvell[i]<<",";
          file << std::endl;
          if (i == 0) // compatibility for gnuplot use
          {
            file << std::endl;
          }
        }
        file.close();
#endif // PRINT_GNUPLOT_OUTPUT
      }

      /**
       * \brief Writes data of an array with intersection size into textfile
       *
       * Please use the given script <tt>simulationsOutput.sh</tt> to visualize
       * the data with gnuplot. The pictures can be found in the subfolder
       * <tt>pics</tt> after processing.
       *
       * \param time Current time
       * \param timeStep Number of the current time step
       * \param gridCells Number of grid cells of the problem
       */
      void gnuplotIntersectionOutput(double time, unsigned int timeStep, unsigned int gridCells)
      {
#if PRINT_GNUPLOT_OUTPUT
//        ParentType::gnuplotIntersectionOutput(time, timeStep, gridCells);
//
//        const int dim = GridView::dimension;
//
//        std::stringstream stream("");
//        stream << "gnuplotZeroEqIntersection_";
//        stream << std::setprecision(4) << std::setw(4) << std::setfill('0') << gridCells << "-";
//        stream << std::setprecision(4) << std::setw(4) << std::setfill('0') << timeStep << ".csv";
//        stream.clear();
//        std::string string(stream.str());
//        const char* fileName = string.c_str();
//        std::fstream file;
//        file.open(fileName, std::ios::out);
//        file << "# time: " << time << std::endl;
//        file << "# ";
//        for (unsigned int curDim = 0; curDim < dim; ++curDim)
//        {
//          file << "global" << curDim << ",";
//        }
////         file << "neighGlob[0][0]" << ",";
////         file << "neighGlob[0][1]" << ",";
////         file << "neighGlob[1][0]" << ",";
////         file << "neighGlob[1][1]" << ",";
//        file << "neighVel[0]" << ",";
//        file << "neighVel[1]" << ",";
//        file << "Mixinglength" << ",";
//        file << "Shearvel" << ",";
//        file << "Yplus" << ",";
//        file << "eddy" << ",";
//        file << "Turbulent stress" << ",";
//        file << "Bound" << ",";
//        file << std::endl;
//
//        for (unsigned int i = 0; i < this->storedIntersectionCentersGlobal.size(); ++i)
//        {
//          for (unsigned int curDim = 0; curDim < dim; ++curDim)
//          {
//            file << this->storedIntersectionCentersGlobal[i][curDim] << ",";
//          }
////           file << this->storedNeighborFaceCentersGlobal[i][0][0] << ",";
////           file << this->storedNeighborFaceCentersGlobal[i][0][1] << ",";
////           file << this->storedNeighborFaceCentersGlobal[i][1][0] << ",";
////           file << this->storedNeighborFaceCentersGlobal[i][1][1] << ",";
//          file << this->storedNeighborVelocities[i][0] << ",";
//          file << this->storedNeighborVelocities[i][1] << ",";
//          file << storedMixing[i] << ",";
//          file << storedshearvel[i] << ",";
//          file << storedyplus[i] << ",";
//          file << storededdy[i] << ",";
//          file << storedstress[i] << ",";
//          //file << storededdy[i] << ",";
//                  file << storedBound[i] << ",";
//          file << std::endl;
//          if (i == 0) // compatibility for gnuplot use
//          {
//            file << std::endl;
//          }
//        }
//        file.close();
#endif // PRINT_GNUPLOT_OUTPUT
      }

    private:
      const BC& bc;
      const SourceMomentumBalance& sourceMomentumBalance;
      const SourceMassBalance& sourceMassBalance;
      const DirichletVelocity& dirichletVelocity;
      const DirichletPressure& dirichletPressure;
      const NeumannVelocity& neumannVelocity;
      const NeumannPressure& neumannPressure;
      GridView gridView;
      unsigned int eddyViscosityModel;
      //! Mapper to get indices for elements
      MapperElement mapperElement;
      //! Mapper to get indices for intersections (codim=1)
      MapperIntersection mapperIntersection;
      //! zeroEq model
      const double karmanConstant = 0.41;
      //! wall distances and corresponding wall data
      StoredGlobalCoordinate storedCorrespondingWallGlobal;
      StoredGridViewID storedCorrespondingWallElementID;
      StoredScalar storedDistanceToWall;
      StoredGlobalCoordinate storedVelMax;
      StoredScalar storedviscinner;
      StoredScalar storedviscouter;
      StoredScalar storedym;
      StoredScalar storedcomp;
      StoredScalar storedvelthick;
      StoredScalar storedymax;
      StoredScalar storedFmax;
      StoredScalar storedvelymax;
      StoredScalar storedomegamax;
      StoredScalar storedsign;
      StoredScalar storedMixing;
      StoredScalar storedshearvel;
      StoredScalar storedyplus;
      StoredScalar storededdy;
      StoredScalar storedstress;
      StoredScalar storedBound;
      StoredScalar storedfunc1;
      StoredScalar storedFmaxi;
      StoredScalar storedfwake;
      StoredScalar storedvell; 
      //StoredScalar storedfmaxii;
      //StoredScalar storedsign1;
   // double storedmixfrFmax;
          
      //StoredScalar storedomegamax;
    };
  } // namespace PDELab
} // namespace Dune

#endif // DUMUX_ZEROEQ_STAGGERED_GRID_HH