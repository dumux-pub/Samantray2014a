/**
 * \file
 * \ingroup StaggeredGrid
 * \ingroup EddyViscosityStaggeredGrid
 *
 * \brief Local operator for staggered grid discretization for steady-state
 * Navier-Stokes equation, modelled with eddy viscosity models.
 *
 * Mass balance:
 * \f[
 *    \frac{\partial \varrho_\alpha}{\partial t}
 *    + \nabla \cdot \left( \varrho_\alpha v_\alpha \right)
 *    - q_{\varrho}
 *    = 0
 * \f]
 *
 * Momentum balance:
 * \f[
 *    \frac{\partial \left( \varrho_\alpha v_\alpha \right)}{\partial t}
 *    + \nabla \cdot \left( p_\alpha I
 *      - \varrho_\alpha \nu_\alpha
 *        \left( \nabla v_\alpha \right)
 *      - \varrho_\alpha \nu_{\alpha,\textrm{t}}
 *        \left( \nabla v_\alpha \right)
 *      \right)
 *    - \varrho_\alpha g
 *    - q_{\varrho v}
 *    = 0
 * \f]
 *
 * \note The transposed part of the viscous term is not included in the
 *       equations yet.
 *
 * The eddy viscosity depends on the chosen model, this could be
 * <ul>
 * <li>0-Equation or algebraic models (like Prandtl, Baldwin-Lomax)</li>
 * <li>1-Equation models (which are not part of dumux yet)</li>
 * <li>2-Equation models (like \f$ k-\varepsilon \f$ or \f$ k-\omega \f$)</li>
 * </ul>
 *
 * \note Be aware that the storage terms (the time dependent terms)
 * are not included here but are implemented in the transient part
 * of the standard Navier-Stokes local operator.
 */

#ifndef DUMUX_EDDY_VISCOSITY_STAGGERED_GRID_HH
#define DUMUX_EDDY_VISCOSITY_STAGGERED_GRID_HH

#if !ENABLE_NAVIER_STOKES
#warning "Eddy viscosity will not be enabled! Please set the macro ENABLE_NAVIER_STOKES 1."
#endif

#include<cstddef>
#include<vector>

#include<dune/common/exceptions.hh>
#include<dune/common/fmatrix.hh>
#include<dune/common/fvector.hh>

#include<dune/geometry/quadraturerules.hh>
#include<dune/geometry/referenceelements.hh>
#include<dune/geometry/type.hh>

#include<dune/grid/common/mcmgmapper.hh>

#include<dune/pdelab/gridfunctionspace/lfsindexcache.hh>
#include<dune/pdelab/gridfunctionspace/localfunctionspace.hh>
#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/localoperator/flags.hh>
#include<dune/pdelab/localoperator/idefault.hh>
#include<dune/pdelab/localoperator/pattern.hh>

#include<appl/staggeredgrid/navierstokes/basenavierstokesstaggeredgrid.hh>

namespace Dune
{
    /**
      * \brief Layout template for faces
      *
      * This layout template is for use in the MultipleCodimMultipleGeomTypeMapper.
      * It selects only etities with <tt>dim = dimgrid-1</tt>.
      *
      * \tparam dimgrid The dimension of the grid.
      */
    template<int dimgrid> struct MCMGIntersectionLayout
    {
      /**
      * \brief Test whether entities of the given geometry type should be
      * included in the map
      */
      bool contains(Dune::GeometryType gt)
      {
        return gt.dim() == (dimgrid - 1);
      }
    };

  namespace PDELab
  {

    /**
     * \brief Local operator for staggered grid discretization for steady-state
     * Navier-Stokes equation, modeled with eddy viscosity models.
     *
     * \tparam BC Boundary condition type
     * \tparam SourceMomentumBalance Source type for velocity
     * \tparam SourceMassBalance Source type for pressure
     * \tparam DirichletVelocity Dirchlet boundary condition for velocity
     * \tparam DirichletPressure Dirchlet boundary condition for pressure
     * \tparam NeumannVelocity Dirchlet boundary condition for velocity
     * \tparam NeumannPressure Dirchlet boundary condition for pressure
     * \tparam GV GridView type
     */
    template<typename BC, typename SourceMomentumBalance, typename SourceMassBalance,
             typename DirichletVelocity, typename DirichletPressure,
             typename NeumannVelocity, typename NeumannPressure, typename GridView>
    class EddyViscosityStaggeredGrid
    : public BaseNavierStokesStaggeredGrid<BC,
        SourceMomentumBalance, SourceMassBalance,
        DirichletVelocity, DirichletPressure,
        NeumannVelocity, NeumannPressure,
        GridView>
    {
    public:
      typedef BaseNavierStokesStaggeredGrid<BC,
        SourceMomentumBalance, SourceMassBalance,
        DirichletVelocity, DirichletPressure,
        NeumannVelocity, NeumannPressure,
        GridView> ParentType;
      typedef Dune::MultipleCodimMultipleGeomTypeMapper<GridView, Dune::MCMGIntersectionLayout>
        MapperIntersection;
      typedef Dune::MultipleCodimMultipleGeomTypeMapper<GridView, Dune::MCMGElementLayout>
        MapperElement;

      //! velocities and coordinates stored at faces
      typedef std::vector<Dune::FieldVector<double, GridView::dimension> > StoredGlobalCoordinate;
      typedef std::vector<Dune::FieldVector<Dune::FieldVector<double, GridView::dimension>, 2*GridView::dimension-2> >
        	  StoredNeighborFaceCentersGlobal;
      typedef std::vector<Dune::FieldVector<double, 2*GridView::dimension-2> >
        	  StoredNeighborVelocities;
      typedef std::vector<Dune::FieldVector<double, GridView::dimension> >
              StoredVelocitiesAtElementCenter;
      //typedef std::vector<Dune::FieldVector<double, GridView::dimension> >
              //StoredVelmax;
      typedef std::vector<Dune::FieldMatrix<double, GridView::dimension, GridView::dimension> >
        	  StoredVelocityGradientTensor;
      
      StoredGlobalCoordinate storedElementCentersGlobal;
      StoredGlobalCoordinate storedIntersectionCentersGlobal;
      StoredNeighborFaceCentersGlobal storedNeighborFaceCentersGlobal;
      StoredNeighborVelocities storedNeighborVelocities;
      StoredVelocityGradientTensor storedVelocityGradientTensor;
      StoredVelocitiesAtElementCenter storedVelocitiesAtElementCenter;
     // StoredVelmax storedVelmax;
      //! Eddy viscosity models
      mutable std::vector<double> storedKinematicEddyViscosity;

      //! \brief Pattern assembly flags
      enum { doPatternVolume = true,
             doPatternSkeleton = true };

      //! \brief Residual assembly flags
      enum { doAlphaVolume = true,
             doAlphaSkeleton = true,
             doAlphaBoundary = true };

      //! \brief Index of unknowns
      enum { velocityIdx = 0,
             pressureIdx = 1 };

      //! \brief Constructor
      EddyViscosityStaggeredGrid(const BC& bc_,
                          const SourceMomentumBalance& sourceMomentumBalance_, const SourceMassBalance& sourceMassBalance_,
                          const DirichletVelocity& dirichletVelocity_, const DirichletPressure& dirichletPressure_,
                          const NeumannVelocity& neumannVelocity_, const NeumannPressure& neumannPressure_,
                          GridView gridView_)
        : ParentType(bc_,
              sourceMomentumBalance_, sourceMassBalance_,
              dirichletVelocity_, dirichletPressure_,
              neumannVelocity_, neumannPressure_,
              gridView_),
          bc(bc_),
          sourceMomentumBalance(sourceMomentumBalance_), sourceMassBalance(sourceMassBalance_),
          dirichletVelocity(dirichletVelocity_), dirichletPressure(dirichletPressure_),
          neumannVelocity(neumannVelocity_), neumannPressure(neumannPressure_),
          gridView(gridView_),
          mapperElement(gridView_), mapperIntersection(gridView_)
      {
        initialize(gridView);
      }

      /**
       * \brief Contribution to volume integral.
       *
       * Volume integral depending on test and ansatz functions
       *
       * \tparam EG element geometry
       * \tparam LFSU local function space for ansatz functions
       * \tparam X coefficient vector
       * \tparam LFSV local function space for test functions
       * \tparam R residual vector
       * \param eg element geometry
       * \param lfsu local functions space for ansatz functions
       * \param x coefficient vector
       * \param lfsv local function space for test functions
       * \param r residual vector
       */
      template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_volume(const EG& eg, const LFSU& lfsu, const X& x,
                        const LFSV& lfsv, R& r) const
      {
        ParentType::alpha_volume(eg, lfsu, x, lfsv, r);

        // select the two components from the subspaces
        typedef typename LFSU::template Child<velocityIdx>::Type LFSU_V;
        const LFSU_V& lfsu_v = lfsu.template child<velocityIdx>();
        typedef typename LFSU::template Child<pressureIdx>::Type LFSU_P;
        const LFSU_P& lfsu_p = lfsu.template child<pressureIdx>();

        // domain and range field type
        typedef typename LFSU_V::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::DomainFieldType DF;
        typedef typename LFSU_V::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeFieldType RF;
        typedef typename LFSU_V::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::JacobianType JacobianVelocity;
        typedef typename LFSU_V::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeType RangeVelocity;
        typedef typename LFSU_P::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeType RangePressure;

        // /////////////////////
        // geometry information

        // dimension
        static const unsigned int dim = EG::Geometry::dimension;

        // calculate velocity DoF positions
        const unsigned int numControlVolumeFaces =
          Dune::ReferenceElements<DF, dim>::general(eg.geometry().type()).size(1);
        std::vector<Dune::FieldVector<DF, dim> > faceCentersLocal(numControlVolumeFaces);
        std::vector<Dune::FieldVector<DF, dim> > faceCentersGlobal(numControlVolumeFaces);
        for (unsigned int curFace = 0; curFace < numControlVolumeFaces; ++curFace)
        {
          std::fill(faceCentersLocal[curFace].begin(), faceCentersLocal[curFace].end(), 0.5);
          faceCentersLocal[curFace][curFace/2] = curFace % 2;
          faceCentersGlobal[curFace] = eg.geometry().global(faceCentersLocal[curFace]);
        }

        // calculate the staggered face volume (goes through cell center) perpendicular to each direction
        Dune::FieldVector<RF, dim> orthogonalFaceVolumes(0.0);
        switch (dim)
        {
          case 1:
            orthogonalFaceVolumes[0] = 1;
            break;
          case 2:
            orthogonalFaceVolumes[0] = std::abs(faceCentersGlobal[3][1] - faceCentersGlobal[2][1]);
            orthogonalFaceVolumes[1] = std::abs(faceCentersGlobal[1][0] - faceCentersGlobal[0][0]);
            break;
          default:
            DUNE_THROW(Dune::NotImplemented, "Staggered grid is not implemented for 3D cases.");
        }

        // /////////////////////
        // velocities

        // evaluate shape functions for velocities
        std::vector<std::vector<RangeVelocity> > velocityBasis(numControlVolumeFaces);
        for (unsigned int curFace = 0; curFace < numControlVolumeFaces; ++curFace)
        {
          velocityBasis[curFace].resize(lfsu_v.size());
          lfsu_v.finiteElement().localBasis().evaluateFunction(
            faceCentersLocal[curFace], velocityBasis[curFace]);
        }

        // evaluate velocity on intersection
        std::vector<RangeVelocity> velocityFaces(numControlVolumeFaces);
        for (unsigned int curFace = 0; curFace < numControlVolumeFaces; ++curFace)
        {
          velocityFaces[curFace] = RangeVelocity(0.0);
          for (unsigned int i = 0; i < lfsu_v.size(); i++)
          {
            velocityFaces[curFace].axpy(x(lfsu_v, i), velocityBasis[curFace][i]);
          }
        }

        // /////////////////////
        // evaluation of unknown, upwinding and averaging

        // evaluate cell values
        DF pressure = x(lfsu_p, 0);
#if !COMPRESSIBILITY
        DF density = pressure * 0.0 + DENSITY;
#else
        DF density = pressure / (SPECIFIC_GAS_CONSTANT * TEMPERATURE);
#endif

        //! \note storedKinematicEddyViscosity[elementID] has to be set by the child function
        const typename GridView::IndexSet::IndexType elementID = mapperElement.map(eg.entity());
        DF kinematicEddyViscosity = storedKinematicEddyViscosity[elementID];

#if PRINT_EDDY_VISCOSITY
        // local and global position of cell center
        const Dune::FieldVector<DF, dim>& cellCenterLocal =
          Dune::ReferenceElements<DF, dim>::general(eg.geometry().type()).position(0, 0);
        Dune::FieldVector<DF, dim> cellCenterGlobal =
          eg.geometry().global(cellCenterLocal);
        DF eps = 0.1;
        DF velocityGradient = storedVelocityGradientTensor[mapperElement.map(eg.entity())][0][1];
        if ((cellCenterGlobal[0] > 5.0 - eps) && (cellCenterGlobal[0] < 5.0 + eps))
        {
          std::cout << "EDDY VISCOSITY @" <<
          " cellCenterGlobal: " << cellCenterGlobal <<
          " kinematicEddyViscosity: " << kinematicEddyViscosity <<
          " kinematicViscosity: " << KINEMATIC_VISCOSITY <<
          " velocityGradient: " << velocityGradient <<
          std::endl;
        }
#endif // PRINT_EDDY_VISCOSITY

        // /////////////////////
        // contribution to residual from element
        // calculate flux over staggered interface in each dimension

        for (unsigned int curDim = 0; curDim < dim; ++curDim)
        {
          /**
           * (1) \b Eddy term of \b momentum balance equation
           *
           * \f[
           *    - \boldsymbol{\tau}_\textrm{t}
           *    = - \nu_\textrm{t} \rho \nabla v
           *    \Rightarrow \int_\gamma - \boldsymbol{\tau} \cdot n
           *    = \int_\gamma - \nu_\textrm{t} \rho \nabla v \cdot n
           * \f]
           * normal case for all coordinate axes
           * \f[
           *    \alpha_\textrm{self}
           *    = - |\gamma| \left( \boldsymbol{\tau} \cdot n \right) \cdot n
           *    = - |\gamma| \varrho \nu_\textrm{t}
           *      \frac{\textrm{d} v_\textrm{curDim}}{\textrm{d} x_\textrm{curDim}}
           *    = - |\gamma| \varrho \nu_\textrm{t}
           *      \frac{v_{\textrm{right,curDim}} - v_{\textrm{left,curDim}}}
           *           {x_{\textrm{right,curDim}} - x_{\textrm{left,curDim}}}
           * \f]
           */
#if ENABLE_NAVIER_STOKES
          r.accumulate(lfsu_v, 2*curDim,
                       // normal is always positive
                       -1.0 * kinematicEddyViscosity * density
                       * (velocityFaces[curDim*2+1][curDim] - velocityFaces[curDim*2][curDim])
                       / (faceCentersGlobal[curDim*2+1][curDim] - faceCentersGlobal[curDim*2][curDim])
                       * orthogonalFaceVolumes[curDim]); // face volume
          r.accumulate(lfsu_v, 2*curDim+1,
                       // normal is always negative
                       1.0 * kinematicEddyViscosity * density
                       * (velocityFaces[curDim*2+1][curDim] - velocityFaces[curDim*2][curDim])
                       / (faceCentersGlobal[curDim*2+1][curDim] - faceCentersGlobal[curDim*2][curDim])
                       * orthogonalFaceVolumes[curDim]); // face volume
#if PRINT_ACCUMULATION_TERM || PRINT_ACCUMULATION_TERM_EDDY
std::cout << "EDDY MOMENTUM NORMAL @" <<
" faceCentersGlobal[curDim*2]: " << faceCentersGlobal[curDim*2] <<
" accumulate: " << (-1.0 * kinematicEddyViscosity * density
                       * (velocityFaces[curDim*2+1][curDim] - velocityFaces[curDim*2][curDim])
                       / (faceCentersGlobal[curDim*2+1][curDim] - faceCentersGlobal[curDim*2][curDim])
                       * orthogonalFaceVolumes[curDim]) <<
std::endl;
std::cout << "EDDY MOMENTUM NORMAL @" <<
" faceCentersGlobal[curDim*2+1]: " << faceCentersGlobal[curDim*2+1] <<
" accumulate: " << (1.0 * kinematicEddyViscosity * density
                       * (velocityFaces[curDim*2+1][curDim] - velocityFaces[curDim*2][curDim])
                       / (faceCentersGlobal[curDim*2+1][curDim] - faceCentersGlobal[curDim*2][curDim])
                       * orthogonalFaceVolumes[curDim]) <<
std::endl;
#endif // PRINT_ACCUMULATION_TERM || PRINT_ACCUMULATION_TERM_EDDY
#endif // ENABLE_NAVIER_STOKES
        }
      }


      /**
       * \brief Skeleton integral depending on test and ansatz functions.
       *
       * Contribution of flux over interface. Each face is only visited once!
       *
       * \tparam IG interface geometry over which the fluxes are evaluated
       * \tparam LFSU local function space for ansatz functions
       * \tparam X coefficient vector
       * \tparam LFSV local function space for test functions
       * \tparam R residual vector
       * \param ig interface geometry
       * \param lfsu_s local functions space for ansatz functions of self/inside
       * \param x_s coefficient vector of self/inside
       * \param lfsv_s local function space for test functions of self/inside
       * \param lfsu_n local functions space for ansatz functions of neighbor/outside
       * \param x_n coefficient vector of neighbor/outside
       * \param lfsv_n local function space for test functions of neighbor/outside
       * \param r_s residual vector of self/inside
       * \param r_n residual vector of neighbor/outside
       */
      template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_skeleton(const IG& ig,
                          const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s,
                          const LFSU& lfsu_n, const X& x_n, const LFSV& lfsv_n,
                          R& r_s, R& r_n) const
      {
        ParentType::alpha_skeleton(ig, lfsu_s, x_s, lfsv_s, lfsu_n, x_n,  lfsv_n, r_s, r_n);

        // select the two components from the subspaces
        typedef typename LFSU::template Child<velocityIdx>::Type LFSU_V;
        const LFSU_V& lfsu_v_s = lfsu_s.template child<velocityIdx>();
        const LFSU_V& lfsu_v_n = lfsu_n.template child<velocityIdx>();
        typedef typename LFSU::template Child<pressureIdx>::Type LFSU_P;
        const LFSU_P& lfsu_p_s = lfsu_s.template child<pressureIdx>();
        const LFSU_P& lfsu_p_n = lfsu_n.template child<pressureIdx>();

        // domain and range field type
        typedef typename LFSU_V::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::DomainFieldType DF;
        typedef typename LFSU_V::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeFieldType RF;
        typedef typename LFSU_V::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::JacobianType JacobianVelocity;
        typedef typename LFSU_V::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeType RangeVelocity;
        typedef typename LFSU_P::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeType RangePressure;

        // /////////////////////
        // geometry information

        // dimension
        static const unsigned int dim = IG::Geometry::dimension;

        // local position of cell and face centers
        const Dune::FieldVector<DF, dim>& insideCellCenterLocal =
          Dune::ReferenceElements<DF, dim>::general(ig.inside()->type()).position(0, 0);
        const Dune::FieldVector<DF, dim>& outsideCellCenterLocal =
          Dune::ReferenceElements<DF, dim>::general(ig.outside()->type()).position(0, 0);
        const Dune::FieldVector<DF, dim-1>& faceCenterLocal =
          Dune::ReferenceElements<DF, dim-1>::general(ig.geometry().type()).position(0, 0);

        // global position of cell and face centers
        Dune::FieldVector<DF, dim> insideCellCenterGlobal =
          ig.inside()->geometry().global(insideCellCenterLocal);
        Dune::FieldVector<DF, dim> outsideCellCenterGlobal =
          ig.outside()->geometry().global(outsideCellCenterLocal);
        Dune::FieldVector<DF, dim> faceCenterGlobal =
          ig.geometry().global(faceCenterLocal);
        //! \todo the next line is only to avoid warnings
        faceCenterGlobal = faceCenterGlobal;

        // face normal
        const Dune::FieldVector<DF, dim>& faceUnitOuterNormal = ig.centerUnitOuterNormal();

        // evaluate orientation of intersection
        unsigned int normDim = 0;
        unsigned int tangDim = 1;
        for (unsigned int curDim = 0; curDim < dim; ++curDim)
        {
          if (std::abs(faceUnitOuterNormal[curDim]) > 1e-10 && dim > 1)
          {
            normDim = curDim;
            tangDim = 1 - curDim;
          }
        }

        // face midpoints of all faces
        const unsigned int numFaces =
          Dune::ReferenceElements<DF, dim>::general(ig.inside()->type()).size(1);
        std::vector<Dune::FieldVector<DF, dim> > faceCentersLocal_s(numFaces);
        std::vector<Dune::FieldVector<DF, dim> > faceCentersLocal_n(numFaces);
        std::vector<Dune::FieldVector<DF, dim> > faceCentersGlobal_s(numFaces);
        std::vector<Dune::FieldVector<DF, dim> > faceCentersGlobal_n(numFaces);
        for (unsigned int curFace = 0; curFace < numFaces; ++curFace)
        {
          faceCentersLocal_s[curFace] =
            Dune::ReferenceElements<DF, dim>::general(ig.inside()->geometry().type()).position(curFace, 1);
          faceCentersLocal_n[curFace] =
            Dune::ReferenceElements<DF, dim>::general(ig.outside()->geometry().type()).position(curFace, 1);
          faceCentersGlobal_s[curFace] = ig.inside()->geometry().global(faceCentersLocal_s[curFace]);
          faceCentersGlobal_n[curFace] = ig.outside()->geometry().global(faceCentersLocal_n[curFace]);
        }

        // face volume for integration
        RF faceVolume = ig.geometry().integrationElement(faceCenterLocal)
                        * Dune::ReferenceElements<DF, dim-1>::general(ig.geometry().type()).volume();
        if (dim > 1) // this is needed for .dfg grids, which otherwise would get a 0 faceVolume
        {
          faceVolume = std::abs(faceCentersGlobal_s[2*tangDim+1][tangDim] - faceCentersGlobal_s[2*tangDim][tangDim]);
        }

        // /////////////////////
        // velocities

        // evaluate shape functions at all face midpoints
        std::vector<std::vector<RangeVelocity> > velocityBasis_s(numFaces);
        std::vector<std::vector<RangeVelocity> > velocityBasis_n(numFaces);
        for (unsigned int curFace = 0; curFace < numFaces; ++curFace)
        {
          velocityBasis_s[curFace].resize(lfsu_v_s.size());
          velocityBasis_n[curFace].resize(lfsu_v_n.size());
          lfsu_v_s.finiteElement().localBasis().evaluateFunction(
            faceCentersLocal_s[curFace], velocityBasis_s[curFace]);
          lfsu_v_n.finiteElement().localBasis().evaluateFunction(
            faceCentersLocal_n[curFace], velocityBasis_n[curFace]);
        }

        // evaluate velocity on midpoints of all faces
        std::vector<RangeVelocity> velocities_s(numFaces);
        std::vector<RangeVelocity> velocities_n(numFaces);
        for (unsigned int curFace = 0; curFace < numFaces; ++curFace)
        {
          velocities_s[curFace] = RangeVelocity(0.0);
          velocities_n[curFace] = RangeVelocity(0.0);
          for (unsigned int i = 0; i < lfsu_v_s.size(); i++)
          {
            velocities_s[curFace].axpy(x_s(lfsu_v_s, i), velocityBasis_s[curFace][i]);
            velocities_n[curFace].axpy(x_n(lfsu_v_n, i), velocityBasis_n[curFace][i]);
          }
        }

        // load coordinates and velocities from last Newton / time step
        typedef typename GridView::template Codim<1>::EntityPointer IntersectionPointer;
        IntersectionPointer intersectionPointer = ig.inside()->template subEntity<1>(ig.indexInInside());
        Dune::FieldVector<Dune::FieldVector<DF, dim>, 2*dim-2> storedFaceCentersGlobal =
          storedNeighborFaceCentersGlobal[mapperIntersection.map(*intersectionPointer)];
        Dune::FieldVector<RF, 2*dim-2> storedVelocities =
          storedNeighborVelocities[mapperIntersection.map(*intersectionPointer)];

        // distances between face center and cell centers for rectangular shapes
        DF distanceInsideToFace = faceCenterGlobal[normDim] - insideCellCenterGlobal[normDim];
        DF distanceOutsideToFace = outsideCellCenterGlobal[normDim] - faceCenterGlobal[normDim];

        // /////////////////////
        // evaluation of unknown, upwinding and averaging

        // evaluate cell values
        DF pressure_s = x_s(lfsu_p_s, 0);
        DF pressure_n = x_n(lfsu_p_n, 0);
#if !COMPRESSIBILITY
        DF density_s = pressure_s * 0.0 + DENSITY;
        DF density_n = pressure_n * 0.0 + DENSITY;
#else
        DF density_s = pressure_s / (SPECIFIC_GAS_CONSTANT * TEMPERATURE);
        DF density_n = pressure_n / (SPECIFIC_GAS_CONSTANT * TEMPERATURE);
#endif

        //! \note storedKinematicEddyViscosity[elementID] has to be set by the child function
        const typename GridView::IndexSet::IndexType elementID_s = mapperElement.map(*ig.inside());
        const typename GridView::IndexSet::IndexType elementID_n = mapperElement.map(*ig.outside());
        DF kinematicEddyViscosity_s = storedKinematicEddyViscosity[elementID_s];
        DF kinematicEddyViscosity_n = storedKinematicEddyViscosity[elementID_n];

#if PRINT_EDDY_VISCOSITY
        DF eps = 0.1;
        DF velocityGradient_s = storedVelocityGradientTensor[mapperElement.map(*ig.inside())][0][1];
        DF velocityGradient_n = storedVelocityGradientTensor[mapperElement.map(*ig.outside())][0][1];
        if ((faceCenterGlobal[0] > 5.0 - eps) && (faceCenterGlobal[0] < 5.0 + eps))
        {
          std::cout << "EDDY VISCOSITY_S @" <<
          " faceCenterGlobal: " << faceCenterGlobal <<
          " kinematicEddyViscosity_s: " << kinematicEddyViscosity_s <<
          " kinematicViscosity_s: " << KINEMATIC_VISCOSITY <<
          " velocityGradient_s: " << velocityGradient_s <<
          std::endl;
          std::cout << "EDDY VISCOSITY_N @" <<
          " faceCenterGlobal: " << faceCenterGlobal <<
          " kinematicEddyViscosity_n: " << kinematicEddyViscosity_n <<
          " kinematicViscosity_n: " << KINEMATIC_VISCOSITY <<
          " velocityGradient_n: " << velocityGradient_n <<
          std::endl;
        }
#endif // PRINT_EDDY_VISCOSITY

        // upwinding (from self to neighbor)
        DF velocityNormal = (velocities_s[ig.indexInInside()] * faceUnitOuterNormal);
        DF density_up = density_s;
        std::vector<RangeVelocity> velocities_up(velocities_s);
        if (velocityNormal < 0)
        {
          velocities_up = velocities_n;
          density_up = density_n;
        }

        // averaging: distance weighted average for diffusion term
        DF density_avg = (distanceInsideToFace * density_n + distanceOutsideToFace * density_s)
                         / (distanceInsideToFace + distanceOutsideToFace);
        DF kinematicEddyViscosity_avg = (distanceInsideToFace * kinematicEddyViscosity_n
                                          + distanceOutsideToFace * kinematicEddyViscosity_s)
                                        / (distanceInsideToFace + distanceOutsideToFace);

#if ENABLE_DIFFUSION_HARMONIC
        // averaging: harmonic averages for diffusion term
        density_avg = (2.0 * density_n * density_s) / (density_n + density_s);
        kinematicEddyViscosity_avg = (2.0 * kinematicEddyViscosity_n * kinematicEddyViscosity_s)
                                     / (kinematicEddyViscosity_n + kinematicEddyViscosity_s);
#endif // ENABLE_DIFFUSION_HARMONIC

        // /////////////////////
        // contribution to residual on inside element, other residual is computed by symmetric call
        Dune::FieldVector<DF, dim> ones(1.0);

        for (unsigned int curDim = 0; curDim < dim; ++curDim)
        {
          /**
           * (1) \b Eddy term of \b momentum balance equation
           *
           * \f[
           *    - \boldsymbol{\tau}_\textrm{t}
           *    = - \nu_\textrm{t} \rho \nabla v 
           *    \Rightarrow - \int_\gamma \boldsymbol{\tau} \cdot n
           *    = - \int_\gamma \nu_\textrm{t} \rho \left( \nabla v \cdot n \right)
           * \f]
           * Tangential cases for all coordinate axes (given for \b 2-D,
           * \b rectangular grids and \f$n=(0,1)^T\f$ which means balancing
           * momentum for \f$v_\textrm{0}\f$)<br>
           * \f[
           *    \alpha_\textrm{self}
           *    = - \left( \boldsymbol{\tau} \cdot n \right) \cdot t
           *    = - |\gamma| \varrho_\textrm{avg} \nu_\textrm{t,avg}
           *      \frac{\partial v_{0}}{\partial x_{1}} 
           * \f]
           * Tangential case, for: \f$\frac{\partial v_{0}}{\partial x_{1}}\f$,
           * right and left means along \f$x_1\f$ axis, \f$n\f$ means 1st entry,
           * \f$t\f$ means 0th entry
           * \f[
           *    A
           *    = - |\gamma| \varrho_\textrm{avg} \nu_\textrm{t,avg}
           *      \frac{\partial v_{0}}{\partial x_{1}}
           *    = - |\gamma| \varrho_\textrm{avg} \nu_\textrm{t,avg}
           *      \left( 0.5 \left[\frac{\left(v_\textrm{right} - v_\textrm{left} \right|_\textrm{t}}
           *                  {\left| x_\textrm{right} - x_\textrm{left} \right|_\textrm{n}}
           *                \right]_\textrm{curDim}
           *           + 0.5 \left[ \frac{\left(v_\textrm{right} - v_\textrm{left} \right|_\textrm{t}}
           *                  {\left| x_\textrm{right} - x_\textrm{left} \right|_\textrm{n}}
           *                \right]_\textrm{curDim+1}
           *      \right)
           * \f]
           *
           * The default procedure for averaging
           * \f$\varrho_\textrm{avg}\f$, \f$\nu_\textrm{t,avg}\f$ is a distance
           * weighted average, by using the macro <tt>ENABLE_DIFFUSION_HARMONIC 1</tt>
           * one can do an harmonic averaging instead.
           */
#if ENABLE_NAVIER_STOKES
          // only tangential case, exclude normal case
          if (curDim != normDim && dim > 1)
          {
            r_s.accumulate(lfsu_v_s, 2*tangDim,
                           -0.5 * density_avg * kinematicEddyViscosity_avg
                           * (velocities_n[2*tangDim][tangDim] - velocities_s[2*tangDim][tangDim])
                             / (outsideCellCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                           * faceUnitOuterNormal[normDim] * faceVolume);
            r_n.accumulate(lfsu_v_n, 2*tangDim,
                           0.5 * density_avg * kinematicEddyViscosity_avg
                           * (velocities_n[2*tangDim][tangDim] - velocities_s[2*tangDim][tangDim])
                             / (outsideCellCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                           * faceUnitOuterNormal[normDim] * faceVolume);

            r_s.accumulate(lfsu_v_s, 2*tangDim+1,
                           -0.5 * density_avg * kinematicEddyViscosity_avg
                           * (velocities_n[2*tangDim+1][tangDim] - velocities_s[2*tangDim+1][tangDim])
                             / (outsideCellCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                           * faceUnitOuterNormal[normDim] * faceVolume);
            r_n.accumulate(lfsu_v_n, 2*tangDim+1,
                           0.5 * density_avg * kinematicEddyViscosity_avg
                           * (velocities_n[2*tangDim+1][tangDim] - velocities_s[2*tangDim+1][tangDim])
                             / (outsideCellCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                           * faceUnitOuterNormal[normDim] * faceVolume);

#if PRINT_ACCUMULATION_TERM || PRINT_ACCUMULATION_TERM_EDDY
std::cout << "EDDY MOMENTUM TANGENTIAL @" <<
" faceCentersGlobal_s[2*tangDim]: " << faceCentersGlobal_s[2*tangDim] <<
" accu: " << (-0.5 * density_avg * kinematicEddyViscosity_avg
                           * (velocities_n[2*tangDim][tangDim] - velocities_s[2*tangDim][tangDim])
                             / (outsideCellCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                           * faceUnitOuterNormal[normDim] * faceVolume) <<
std::endl;
std::cout << "EDDY MOMENTUM TANGENTIAL @" <<
" faceCentersGlobal_n[2*tangDim]: " << faceCentersGlobal_n[2*tangDim] <<
" accu: " << (0.5 * density_avg * kinematicEddyViscosity_avg
                           * (velocities_n[2*tangDim][tangDim] - velocities_s[2*tangDim][tangDim])
                             / (outsideCellCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                           * faceUnitOuterNormal[normDim] * faceVolume) <<
std::endl;
std::cout << "EDDY MOMENTUM TANGENTIAL @" <<
" faceCentersGlobal_s[2*tangDim+1]: " << faceCentersGlobal_s[2*tangDim+1] <<
" accu: " << (-0.5 * density_avg * kinematicEddyViscosity_avg
                           * (velocities_n[2*tangDim+1][tangDim] - velocities_s[2*tangDim+1][tangDim])
                             / (outsideCellCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                           * faceUnitOuterNormal[normDim] * faceVolume) <<
std::endl;
std::cout << "EDDY MOMENTUM TANGENTIAL @" <<
" faceCentersGlobal_n[2*tangDim+1]: " << faceCentersGlobal_n[2*tangDim+1] <<
" accu: " << (0.5 * density_avg * kinematicEddyViscosity_avg
                           * (velocities_n[2*tangDim+1][tangDim] - velocities_s[2*tangDim+1][tangDim])
                             / (outsideCellCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                           * faceUnitOuterNormal[normDim] * faceVolume) <<
std::endl;
#endif // PRINT_ACCUMULATION_TERM || PRINT_ACCUMULATION_TERM_EDDY
          }
#endif // ENABLE_NAVIER_STOKES
        }
      }

      /**
       * \brief Boundary integral depending on test and ansatz functions
       *
       * We put the Dirchlet evaluation also in the alpha term to save
       * some geometry evaluations.
       *
       * \tparam IG interface geometry over which the fluxes are evaluated
       * \tparam LFSU local function space for ansatz functions
       * \tparam X coefficient vector
       * \tparam LFSV local function space for test functions
       * \tparam R residual vector
       * \param ig interface geometry
       * \param lfsu_s local functions space for ansatz functions
       * \param x_s coefficient vector
       * \param lfsv_s local function space for test functions
       * \param r_s residual vector
       */
      template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_boundary(const IG& ig, const LFSU& lfsu_s, const X& x_s,
                          const LFSV& lfsv_s, R& r_s) const
      {
        ParentType::alpha_boundary(ig, lfsu_s, x_s, lfsv_s, r_s);

        // select the two components from the subspaces
        typedef typename LFSU::template Child<velocityIdx>::Type LFSU_V;
        const LFSU_V& lfsu_v_s = lfsu_s.template child<velocityIdx>();
        typedef typename LFSU::template Child<pressureIdx>::Type LFSU_P;
        const LFSU_P& lfsu_p_s = lfsu_s.template child<pressureIdx>();
        typedef typename BC::template Child<velocityIdx>::Type BCVelocity;
        const BCVelocity& bcVelocity = bc.template child<velocityIdx>();

        // domain and range field type
        typedef typename LFSU_V::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::DomainFieldType DF;
        typedef typename LFSU_V::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeFieldType RF;
        typedef typename LFSU_V::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeType RangeVelocity;

        // dimensions
        const int dim = IG::dimension;

        // center in face's reference element
        const Dune::FieldVector<DF, IG::dimension-1>& faceCenterLocal =
          Dune::ReferenceElements<DF, IG::dimension-1>::general(ig.geometry().type()).position(0, 0);
        const Dune::FieldVector<DF, dim>& faceCenterGlobal =
          ig.geometry().global(faceCenterLocal);

        // face properties (coordinates and normal)
        const Dune::FieldVector<DF, dim>& insideFaceCenterLocal =
          ig.geometryInInside().global(faceCenterLocal);
        const Dune::FieldVector<DF,dim>& faceUnitOuterNormal = ig.centerUnitOuterNormal();

        // evaluate orientation of intersection
        unsigned int normDim = 0;
        unsigned int tangDim = 1;
        for (unsigned int curDim = 0; curDim < dim; ++curDim)
        {
          if (std::abs(ig.centerUnitOuterNormal()[curDim]) > 1e-10 && dim > 1)
          {
            normDim = curDim;
            tangDim = 1 - curDim;
          }
        }

        // face volume for integration
        RF faceVolume = ig.geometry().integrationElement(faceCenterLocal)
          * Dune::ReferenceElements<DF,IG::dimension-1>::general(ig.geometry().type()).volume();

        // calculate velocity DoF positions
        const unsigned int numControlVolumeFaces =
          Dune::ReferenceElements<DF, dim>::general(ig.inside()->geometry().type()).size(1);
        std::vector<Dune::FieldVector<DF, dim> > faceCentersLocal(numControlVolumeFaces);
        std::vector<Dune::FieldVector<DF, dim> > faceCentersGlobal(numControlVolumeFaces);
        for (unsigned int curFace = 0; curFace < numControlVolumeFaces; ++curFace)
        {
          std::fill(faceCentersLocal[curFace].begin(), faceCentersLocal[curFace].end(), 0.5);
          faceCentersLocal[curFace][curFace / 2] = curFace % 2;
          faceCentersGlobal[curFace] =
            ig.inside()->geometry().global(faceCentersLocal[curFace]);
        }

        // cell center in reference element
        const Dune::FieldVector<DF,IG::dimension>&
          insideCellCenterLocal = Dune::ReferenceElements<DF,IG::dimension>::general(ig.inside()->type()).position(0, 0);
        const Dune::FieldVector<DF, dim> insideCellCenterGlobal =
          ig.inside()->geometry().global(insideCellCenterLocal);

        // evaluate transformation which must be linear
        typename IG::Entity::Geometry::JacobianInverseTransposed jac;
        jac = ig.inside()->geometry().jacobianInverseTransposed(insideCellCenterLocal);
        jac.invert();

        //RF det_s = ig.inside()->geometry().integrationElement(insideCellCenterLocal);

        // evaluate shape functions for velocities
        std::vector<std::vector<RangeVelocity> > velocityBasis(numControlVolumeFaces);
        for (unsigned int curFace = 0; curFace < numControlVolumeFaces; ++curFace)
        {
          velocityBasis[curFace].resize(lfsu_v_s.size());
          lfsu_v_s.finiteElement().localBasis().evaluateFunction(
            faceCentersLocal[curFace], velocityBasis[curFace]);
        }

        // /////////////////////
        // velocities
        std::vector<RangeVelocity> velocityFaces(numControlVolumeFaces);
        for (unsigned int curFace = 0; curFace < numControlVolumeFaces; ++curFace)
        {
          velocityFaces[curFace] = RangeVelocity(0.0);
          for (unsigned int i = 0; i < lfsu_v_s.size(); i++)
          {
            velocityFaces[curFace].axpy(x_s(lfsu_v_s, i), velocityBasis[curFace][i]);
          }
        }

        // load coordinates and velocities from last Newton / time step
        typedef typename GridView::template Codim<1>::EntityPointer IntersectionPointer;
        IntersectionPointer intersectionPointer = ig.inside()->template subEntity<1>(ig.indexInInside());
        Dune::FieldVector<Dune::FieldVector<DF, dim>, 2*dim-2> storedFaceCentersGlobal =
          storedNeighborFaceCentersGlobal[mapperIntersection.map(*intersectionPointer)];
        Dune::FieldVector<RF, 2*dim-2> storedVelocities =
          storedNeighborVelocities[mapperIntersection.map(*intersectionPointer)];

        // /////////////////////
        // evaluation of unknown, upwinding and averaging
        DF pressure_s = x_s(lfsu_p_s, 0);
#if !COMPRESSIBILITY
        DF density_s = pressure_s * 0.0 + DENSITY;
#else
        DF density_s = pressure_s / (SPECIFIC_GAS_CONSTANT * TEMPERATURE);
#endif

        // averaging: distance weighted average quantities on intersection
        //! \todo if there is a Dirichlet BC for pressure, one could think of averaging
        DF density_avg = density_s;
        //! \note storedKinematicEddyViscosity[elementID_s] has to be set by the child function
        const typename GridView::IndexSet::IndexType elementID_s = mapperElement.map(*ig.inside());
        DF kinematicEddyViscosity_s = storedKinematicEddyViscosity[elementID_s];

        // Wall or Inflow boundary for velocity
        if (bcVelocity.isWall(ig, faceCenterLocal) || bcVelocity.isInflow(ig, faceCenterLocal))
        {
          // evaluate boundary condition functions
          typename DirichletVelocity::Traits::RangeType dirichletVelocityCorner0;
          typename DirichletVelocity::Traits::RangeType dirichletVelocityCorner1;
          Dune::FieldVector<DF, dim> insideFaceCornerLocal0;
          Dune::FieldVector<DF, dim> insideFaceCornerLocal1;
          insideFaceCornerLocal0[normDim] = insideFaceCenterLocal[normDim];
          insideFaceCornerLocal1[normDim] = insideFaceCenterLocal[normDim];
          if (dim > 1)
          {
            insideFaceCornerLocal0[tangDim] = 0.0;
            insideFaceCornerLocal1[tangDim] = 1.0;
          }
          dirichletVelocity.evaluate(*(ig.inside()), insideFaceCornerLocal0, dirichletVelocityCorner0);
          dirichletVelocity.evaluate(*(ig.inside()), insideFaceCornerLocal1, dirichletVelocityCorner1);
          /**
           * Dirichlet boundary handling for velocity/ momentum balance<br>
           * (1) \b Eddy term of \b momentum balance equation
           *
           * \f[
           *    - \boldsymbol{\tau}_\textrm{t}
           *    = - \nu_\textrm{t} \rho \left( \nabla v \right)
           *    \Rightarrow - \int_\gamma \boldsymbol{\tau} \cdot n
           *    = - \int_\gamma \nu_\textrm{t} \rho \nabla v \cdot n
           * \f]
           * Only the tangential case is regarded here, as if there is a Dirichlet value
           * for the velocity in face normal direction, it is automatically fixed.
           * \f[
           *    \alpha_\textrm{self}
           *    = - \left( \boldsymbol{\tau} \cdot n \right) \cdot t
           *    = - |\gamma| \varrho \nu_\textrm{t}
           *      \frac{\partial v_\textrm{t}}{\partial x_\textrm{n}}
           * \f]
           * The first tangential case \f$\left( \frac{\partial v_\textrm{t}}{\partial x_\textrm{n}} \right)\f$,
           * is calculated from the velocities of the perpendicular faces of the
           * inside element and the dirichlet value for the normal velocity.<br>
           * Currently no averaging is done for kinematic viscosity and density.
           */
#if ENABLE_NAVIER_STOKES
          // only tangential case, exclude normal case
          if (dim > 1)
          {
            r_s.accumulate(lfsu_v_s, 2*tangDim,
                            -0.5 * density_avg * kinematicEddyViscosity_s
                            * (velocityFaces[2*tangDim][tangDim] - dirichletVelocityCorner0[tangDim])
                              / (insideCellCenterGlobal[normDim] - faceCenterGlobal[normDim])
                            * faceUnitOuterNormal[normDim]
                            * faceVolume);

            r_s.accumulate(lfsu_v_s, 2*tangDim+1,
                            -0.5 * density_s * kinematicEddyViscosity_s
                            * (velocityFaces[2*tangDim+1][tangDim] - dirichletVelocityCorner1[tangDim])
                              / (insideCellCenterGlobal[normDim] - faceCenterGlobal[normDim])
                            * faceUnitOuterNormal[normDim]
                            * faceVolume);

#if PRINT_ACCUMULATION_TERM || PRINT_ACCUMULATION_TERM_EDDY
std::cout << "EDDY MOMENTUM BOUNDARY @" <<
" faceCentersGlobal[2*tangDim]: " << faceCentersGlobal[2*tangDim] <<
" accu: " << (-0.5 * density_avg * kinematicEddyViscosity_s
                            * (velocityFaces[2*tangDim][tangDim] - dirichletVelocityCorner0[tangDim])
                              / (insideCellCenterGlobal[normDim] - faceCenterGlobal[normDim])
                            * faceUnitOuterNormal[normDim] * faceVolume) <<
std::endl;
std::cout << "EDDY MOMENTUM BOUNDARY @" <<
" faceCentersGlobal[2*tangDim+1]: " << faceCentersGlobal[2*tangDim+1] <<
" accu: " << (-0.5 * density_avg * kinematicEddyViscosity_s
                            * (velocityFaces[2*tangDim+1][tangDim] - dirichletVelocityCorner1[tangDim])
                              / (insideCellCenterGlobal[normDim] - faceCenterGlobal[normDim])
                            * faceUnitOuterNormal[normDim] * faceVolume) <<
std::endl;
#endif // PRINT_ACCUMULATION_TERM || PRINT_ACCUMULATION_TERM_EDDY
          }
#endif // ENABLE_NAVIER_STOKES
        }
        // Outflow boundary for velocity
        else if (bcVelocity.isOutflow(ig, faceCenterLocal))
        {
          //! Nothing has to be done here.
        }
        // Symmetry boundary for velocity
        else if (bcVelocity.isSymmetry(ig, faceCenterLocal))
        {
          //! Nothing has to be done in case of Symmetry Condition for velocity.
          //! Ensure normal velocity = 0 is done by the parent function.
        }
        else
        {
          DUNE_THROW(Dune::NotImplemented, "Wrong boundary condition type for velocity.");
        }
      }

      /**
       * \brief Initialize vector-stored values used for alpha_* routines.
       *
       * This function calls the ParentType and additionally 
       * Traverses the grid and initialize values, needed for the eddy viscosity models.
       *
       * \tparam GridView GridView type
       */
      void initialize(const GridView& gridView)
      {
        ParentType::initialize(gridView);
//         std::cout << "initialize eddy" << std::endl;

        // velocities and coordinates stored at faces
        storedElementCentersGlobal.resize(mapperElement.size());
        storedIntersectionCentersGlobal.resize(mapperIntersection.size());
        storedNeighborFaceCentersGlobal.resize(mapperIntersection.size());
        storedNeighborVelocities.resize(mapperIntersection.size());
        storedVelocityGradientTensor.resize(mapperElement.size());
        storedVelocitiesAtElementCenter.resize(mapperElement.size());
       // storedVelmax.resize(mapperElement.size());
        
        // Eddy viscosity models
        storedKinematicEddyViscosity.resize(mapperElement.size());
        for (unsigned int i = 0; i < storedKinematicEddyViscosity.size(); ++i)
        {
          storedKinematicEddyViscosity[i] = 0.0;
        }

        // constants and types
        const int dim = GridView::dimension;
        typedef typename GridView::Traits::template Codim<0>::Iterator ElementIterator;
        typedef typename GridView::IntersectionIterator IntersectionIterator;
        typedef double RF;
        typedef double DF;

        // loop over grid view to get elemets with a wall intersection
        for (ElementIterator eit = gridView.template begin<0>();
            eit != gridView.template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.map(*eit);
          const Dune::FieldVector<DF, dim>& cellCenterLocal =
            Dune::ReferenceElements<DF, dim>::general(eit->geometry().type()).position(0, 0);
          Dune::FieldVector<DF, dim> cellCenterGlobal = eit->geometry().global(cellCenterLocal);
          storedElementCentersGlobal[elementInsideID] = cellCenterGlobal;

          for (IntersectionIterator is = gridView.ibegin(*eit);
              is != gridView.iend(*eit); ++is)
          {
            typedef typename GridView::template Codim<1>::EntityPointer IntersectionPointer;
            IntersectionPointer intersectionPointer = is->inside()->template subEntity<1>(is->indexInInside());
            const Dune::FieldVector<DF, dim-1>& faceCenterLocal =
              Dune::ReferenceElements<DF, dim-1>::general(is->geometry().type()).position(0, 0);
            Dune::FieldVector<DF, dim> faceCenterGlobal = is->geometry().global(faceCenterLocal);
            storedIntersectionCentersGlobal[mapperIntersection.map(*intersectionPointer)] = faceCenterGlobal;
          }
        }
      }

      /**
       * \brief Update vector-stored values used for alpha_* routines.
       *
       * This function calls the ParentType and
       * for the eddy viscosity models also the velocity gradient tensor containing
       * the gradient of each velocity component in all spatial dimension is stored
       * per element.
       *
       * \tparam GFS GridFunctionSpace type
       * \tparam X Coefficient vector
       * \param gfs Grid function space
       * \param lastSolution Coefficient vector from last Newton step or time step
       */
      template<typename GFS, typename X>
      void updateStoredValues(const GFS& gfs, X& lastSolution)
      {
//         std::cout << "updateStoredValues eddy" << std::endl;

        // constants and types
        const int dim = GridView::dimension;
        typedef typename GridView::Traits::template Codim<0>::Iterator ElementIterator;
        typedef typename GridView::IntersectionIterator IntersectionIterator;

        // make local function space
        typedef Dune::PDELab::LocalFunctionSpace<GFS> LFS;
        typedef typename LFS::template Child<velocityIdx>::Type LFS_V;
        typedef Dune::PDELab::LFSIndexCache<LFS> LFSCache;
        typedef typename X::template ConstLocalView<LFSCache> XView;

        typedef typename LFS_V::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits FETraits;
        typedef typename FETraits::DomainFieldType DF;
        typedef typename FETraits::RangeFieldType RF;
        typedef typename FETraits::RangeType RangeType;

        LFS lfs(gfs);
        LFSCache lfsCache(lfs);
        XView xView(lastSolution);
        std::vector<RF> xLocal(lfs.maxSize());
        std::vector<RangeType> basisLocal(lfs.maxSize());

        if (dim > 1)
        {
          // loop over grid view
          for (ElementIterator eit = gfs.gridView().template begin<0>();
              eit != gfs.gridView().template end<0>(); ++eit)
          {
            const typename GridView::IndexSet::IndexType elementIdInside = mapperElement.map(*eit);

            for (IntersectionIterator is = gridView.ibegin(*eit);
                is != gridView.iend(*eit); ++is)
            {
              if (is->neighbor())
              {
                const typename GridView::IndexSet::IndexType elementIdOutside = mapperElement.map(*(is->outside()));

                if (elementIdInside < elementIdOutside)
                {
                  // get adjacent faces and corners
                  unsigned int adjacentFaceInside0, adjacentFaceInside1, adjacentFaceOutside0, adjacentFaceOutside1;
                  Dune::FieldVector<DF, dim> adjacentFaceCenterInsideLocal0(0.0), adjacentFaceCenterInsideLocal1(0.0);
                  Dune::FieldVector<DF, dim> adjacentFaceCenterOutsideLocal0(0.0), adjacentFaceCenterOutsideLocal1(0.0);
                  unsigned int isOrientationIdxInside, isOrientationIdxOutside;
                  switch (is->indexInInside())
                  {
                    case 0:
                      adjacentFaceInside0 = 2;
                      adjacentFaceInside1 = 3;
                      adjacentFaceCenterInsideLocal0[0] = 0.5;
                      adjacentFaceCenterInsideLocal0[1] = 0.0;
                      adjacentFaceCenterInsideLocal1[0] = 0.5;
                      adjacentFaceCenterInsideLocal1[1] = 1.0;
                      isOrientationIdxInside = 0;
                      break;
                    case 1: //! \todo TODO: case 1 == case 0, only isOrientationIdxInside is different
                            //!             remove one of them?
                      adjacentFaceInside0 = 2;
                      adjacentFaceInside1 = 3;
                      adjacentFaceCenterInsideLocal0[0] = 0.5;
                      adjacentFaceCenterInsideLocal0[1] = 0.0;
                      adjacentFaceCenterInsideLocal1[0] = 0.5;
                      adjacentFaceCenterInsideLocal1[1] = 1.0;
                      isOrientationIdxInside = 1;
                      break;
                    case 2:
                      adjacentFaceInside0 = 0;
                      adjacentFaceInside1 = 1;
                      adjacentFaceCenterInsideLocal0[0] = 0.0;
                      adjacentFaceCenterInsideLocal0[1] = 0.5;
                      adjacentFaceCenterInsideLocal1[0] = 1.0;
                      adjacentFaceCenterInsideLocal1[1] = 0.5;
                      isOrientationIdxInside = 0;
                      break;
                    case 3: //! \todo TODO: case 31 == case 2, only isOrientationIdxInside is different
                            //!             remove one of them?
                      adjacentFaceInside0 = 0;
                      adjacentFaceInside1 = 1;
                      adjacentFaceCenterInsideLocal0[0] = 0.0;
                      adjacentFaceCenterInsideLocal0[1] = 0.5;
                      adjacentFaceCenterInsideLocal1[0] = 1.0;
                      adjacentFaceCenterInsideLocal1[1] = 0.5;
                      isOrientationIdxInside = 1;
                      break;
                    default:
                      DUNE_THROW(Dune::NotImplemented, "This indexInInside is not known by the local operator.");
                  }
                  switch (is->indexInOutside())
                  {
                    case 0:
                      adjacentFaceOutside0 = 2;
                      adjacentFaceOutside1 = 3;
                      adjacentFaceCenterOutsideLocal0[0] = 0.5;
                      adjacentFaceCenterOutsideLocal0[1] = 0.0;
                      adjacentFaceCenterOutsideLocal1[0] = 0.5;
                      adjacentFaceCenterOutsideLocal1[1] = 1.0;
                      isOrientationIdxOutside = 0;
                      break;
                    case 1: //! \todo TODO: case 1 == case 0, only isOrientationIdxInside is different
                            //!             remove one of them?
                      adjacentFaceOutside0 = 2;
                      adjacentFaceOutside1 = 3;
                      adjacentFaceCenterOutsideLocal0[0] = 0.5;
                      adjacentFaceCenterOutsideLocal0[1] = 0.0;
                      adjacentFaceCenterOutsideLocal1[0] = 0.5;
                      adjacentFaceCenterOutsideLocal1[1] = 1.0;
                      isOrientationIdxOutside = 1;
                      break;
                    case 2:
                      adjacentFaceOutside0 = 0;
                      adjacentFaceOutside1 = 1;
                      adjacentFaceCenterOutsideLocal0[0] = 0.0;
                      adjacentFaceCenterOutsideLocal0[1] = 0.5;
                      adjacentFaceCenterOutsideLocal1[0] = 1.0;
                      adjacentFaceCenterOutsideLocal1[1] = 0.5;
                      isOrientationIdxOutside = 0;
                      break;
                    case 3: //! \todo TODO: case 3 == case 2, only isOrientationIdxInside is different
                            //!             remove one of them?
                      adjacentFaceOutside0 = 0;
                      adjacentFaceOutside1 = 1;
                      adjacentFaceCenterOutsideLocal0[0] = 0.0;
                      adjacentFaceCenterOutsideLocal0[1] = 0.5;
                      adjacentFaceCenterOutsideLocal1[0] = 1.0;
                      adjacentFaceCenterOutsideLocal1[1] = 0.5;
                      isOrientationIdxOutside = 1;
                      break;
                    default:
                      DUNE_THROW(Dune::NotImplemented, "This indexInOutside is not known by the local operator.");
                  }

                  // convert local to global coordinates
                  Dune::FieldVector<DF, dim> adjacentFaceCenterInsideGlobal0(0.0), adjacentFaceCenterInsideGlobal1(0.0);
                  Dune::FieldVector<DF, dim> adjacentFaceCenterOutsideGlobal0(0.0), adjacentFaceCenterOutsideGlobal1(0.0);
                  adjacentFaceCenterInsideGlobal0 = is->inside()->geometry().global(adjacentFaceCenterInsideLocal0);
                  adjacentFaceCenterInsideGlobal1 = is->inside()->geometry().global(adjacentFaceCenterInsideLocal1);
                  adjacentFaceCenterOutsideGlobal0 = is->outside()->geometry().global(adjacentFaceCenterOutsideLocal0);
                  adjacentFaceCenterOutsideGlobal1 = is->outside()->geometry().global(adjacentFaceCenterOutsideLocal1);

                  // bind local function space to inside element
                  lfs.bind(*is->inside());
                  LFS_V lfs_v = lfs.template child<velocityIdx>();
                  lfsCache.update();
                  xView.bind(lfsCache);
                  xView.read(xLocal);
                  xView.unbind();

                  // evaluate solution bound to grid function space for face mid-points
                  RangeType vInside0(0.0), vInside1(0.0);
                  lfs_v.finiteElement().localBasis().evaluateFunction(adjacentFaceCenterInsideLocal0, basisLocal);
                  for (unsigned int i = 0; i < lfs_v.size(); ++i)
                  {
                    vInside0.axpy(xLocal[lfs_v.localIndex(i)], basisLocal[i]);
                  }
                  lfs_v.finiteElement().localBasis().evaluateFunction(adjacentFaceCenterInsideLocal1, basisLocal);
                  for (unsigned int i = 0; i < lfs_v.size(); ++i)
                  {
                    vInside1.axpy(xLocal[lfs_v.localIndex(i)], basisLocal[i]);
                  }

                  // bind local function space to outside element
                  lfs.bind(*is->outside());
                  lfs_v = lfs.template child<velocityIdx>();
                  lfsCache.update();
                  xView.bind(lfsCache);
                  xView.read(xLocal);
                  xView.unbind();

                  // evaluate solution bound to grid function space for face mid-points
                  RangeType vOutside0(0.0), vOutside1(0.0);
                  lfs_v.finiteElement().localBasis().evaluateFunction(adjacentFaceCenterOutsideLocal0, basisLocal);
                  for (unsigned int i = 0; i < lfs_v.size(); ++i)
                  {
                    vOutside0.axpy(xLocal[lfs_v.localIndex(i)], basisLocal[i]);
                  }
                  lfs_v.finiteElement().localBasis().evaluateFunction(adjacentFaceCenterOutsideLocal1, basisLocal);
                  for (unsigned int i = 0; i < lfs_v.size(); ++i)
                  {
                    vOutside1.axpy(xLocal[lfs_v.localIndex(i)], basisLocal[i]);
                  }

                  // evaluate orientation of intersection
                  unsigned int normDim = 0;
                  unsigned int tangDim = 1;
                  for (unsigned int curDim = 0; curDim < dim; ++curDim)
                  {
                    if (std::abs(is->centerUnitOuterNormal()[curDim]) > 1e-10 && dim > 1)
                    {
                      normDim = curDim;
                      tangDim = 1 - curDim;
                    }
                  }

                  // attach velocities to adjacent intersections on other side
                  typedef typename GridView::template Codim<1>::EntityPointer IntersectionPointer;
                  //! \todo TODO: is->indexInInside() % 2 assumes SGrid, general case calculates global coordinates
                  IntersectionPointer intersectionPointer = is->inside()->template subEntity<1>(adjacentFaceInside0);
                  storedNeighborFaceCentersGlobal[mapperIntersection.map(*intersectionPointer)][isOrientationIdxInside] = adjacentFaceCenterOutsideGlobal0;
                  storedNeighborVelocities[mapperIntersection.map(*intersectionPointer)][isOrientationIdxInside] = vOutside0[tangDim];

                  intersectionPointer = is->inside()->template subEntity<1>(adjacentFaceInside1);
                  storedNeighborFaceCentersGlobal[mapperIntersection.map(*intersectionPointer)][isOrientationIdxInside] = adjacentFaceCenterOutsideGlobal1;
                  storedNeighborVelocities[mapperIntersection.map(*intersectionPointer)][isOrientationIdxInside] = vOutside1[tangDim];

                  intersectionPointer = is->outside()->template subEntity<1>(adjacentFaceOutside0);
                  storedNeighborFaceCentersGlobal[mapperIntersection.map(*intersectionPointer)][isOrientationIdxOutside] = adjacentFaceCenterInsideGlobal0;
                  storedNeighborVelocities[mapperIntersection.map(*intersectionPointer)][isOrientationIdxOutside] = vInside0[tangDim];

                  intersectionPointer = is->outside()->template subEntity<1>(adjacentFaceOutside1);
                  storedNeighborFaceCentersGlobal[mapperIntersection.map(*intersectionPointer)][isOrientationIdxOutside] = adjacentFaceCenterInsideGlobal1;
                  storedNeighborVelocities[mapperIntersection.map(*intersectionPointer)][isOrientationIdxOutside] = vInside1[tangDim];
                }
              }
            }
          }

          // loop over grid view
          for (ElementIterator eit = gfs.gridView().template begin<0>();
              eit != gfs.gridView().template end<0>(); ++eit)
          {
            // store velocities next to the boundary
            for (IntersectionIterator is = gridView.ibegin(*eit);
                is != gridView.iend(*eit); ++is)
            {
              if (!is->neighbor())
              {
                const Dune::FieldVector<DF, dim-1>& faceCenterLocal =
                  Dune::ReferenceElements<DF, dim-1>::general(is->geometry().type()).position(0, 0);
                Dune::FieldVector<DF, dim> faceCenterGlobal =
                  is->geometry().global(faceCenterLocal);

                // get adjacent faces and corners
                unsigned int adjacentFaceInside0, adjacentFaceInside1;
                Dune::FieldVector<DF, dim> adjacentFaceCenterInsideLocal0(0.0), adjacentFaceCenterInsideLocal1(0.0);
                switch (is->indexInInside())
                {
                  case 0: //! \todo TODO: case 0 == case 1, remove one of them?
                    adjacentFaceInside0 = 2;
                    adjacentFaceInside1 = 3;
                    adjacentFaceCenterInsideLocal0[0] = 0.5;
                    adjacentFaceCenterInsideLocal0[1] = 0.0;
                    adjacentFaceCenterInsideLocal1[0] = 0.5;
                    adjacentFaceCenterInsideLocal1[1] = 1.0;
                    break;
                  case 1:
                    adjacentFaceInside0 = 2;
                    adjacentFaceInside1 = 3;
                    adjacentFaceCenterInsideLocal0[0] = 0.5;
                    adjacentFaceCenterInsideLocal0[1] = 0.0;
                    adjacentFaceCenterInsideLocal1[0] = 0.5;
                    adjacentFaceCenterInsideLocal1[1] = 1.0;
                    break;
                  case 2: //! \todo TODO: case 2 == case 3, remove one of them?
                    adjacentFaceInside0 = 0;
                    adjacentFaceInside1 = 1;
                    adjacentFaceCenterInsideLocal0[0] = 0.0;
                    adjacentFaceCenterInsideLocal0[1] = 0.5;
                    adjacentFaceCenterInsideLocal1[0] = 1.0;
                    adjacentFaceCenterInsideLocal1[1] = 0.5;
                    break;
                  case 3:
                    adjacentFaceInside0 = 0;
                    adjacentFaceInside1 = 1;
                    adjacentFaceCenterInsideLocal0[0] = 0.0;
                    adjacentFaceCenterInsideLocal0[1] = 0.5;
                    adjacentFaceCenterInsideLocal1[0] = 1.0;
                    adjacentFaceCenterInsideLocal1[1] = 0.5;
                    break;
                  default:
                    DUNE_THROW(Dune::NotImplemented, "This indexInInside is not known by the local operator.");
                }

                // convert local to global coordinates
                Dune::FieldVector<DF, dim> adjacentFaceCenterInsideGlobal0(0.0), adjacentFaceCenterInsideGlobal1(0.0);
                adjacentFaceCenterInsideGlobal0 = is->inside()->geometry().global(adjacentFaceCenterInsideLocal0);
                adjacentFaceCenterInsideGlobal1 = is->inside()->geometry().global(adjacentFaceCenterInsideLocal1);

                // bind local function space to inside element
                lfs.bind(*is->inside());
                LFS_V lfs_v = lfs.template child<velocityIdx>();
                lfsCache.update();
                xView.bind(lfsCache);
                xView.read(xLocal);
                xView.unbind();

                // evaluate solution bound to grid function space for face mid-points
                RangeType vInside0(0.0), vInside1(0.0);
                lfs_v.finiteElement().localBasis().evaluateFunction(adjacentFaceCenterInsideLocal0, basisLocal);
                for (unsigned int i = 0; i < lfs_v.size(); ++i)
                {
                  vInside0.axpy(xLocal[lfs_v.localIndex(i)], basisLocal[i]);
                }
                lfs_v.finiteElement().localBasis().evaluateFunction(adjacentFaceCenterInsideLocal1, basisLocal);
                for (unsigned int i = 0; i < lfs_v.size(); ++i)
                {
                  vInside1.axpy(xLocal[lfs_v.localIndex(i)], basisLocal[i]);
                }

                // evaluate orientation of intersection
                unsigned int normDim = 0;
                unsigned int tangDim = 1;
                for (unsigned int curDim = 0; curDim < dim; ++curDim)
                {
                  if (std::abs(is->centerUnitOuterNormal()[curDim]) > 1e-10 && dim > 1)
                  {
                    normDim = curDim;
                    tangDim = 1 - curDim;
                  }
                }
                unsigned int isOrientationIdxInside = 0;
                if (is->centerUnitOuterNormal()[normDim] > 0)
                {
                  isOrientationIdxInside = 1;
                }

                // STANDARD CASE for undefined boundaries
                // attach velocities to adjacent intersections, extrapolate velocity with same gradient as to inner neighbor
                typedef typename GridView::template Codim<1>::EntityPointer IntersectionPointer;
                //! \todo TODO: is->indexInInside() % 2 assumes SGrid, general case calculates global coordinates
                IntersectionPointer intersectionPointer = is->inside()->template subEntity<1>(adjacentFaceInside0);
                storedNeighborFaceCentersGlobal[mapperIntersection.map(*intersectionPointer)][isOrientationIdxInside][normDim] =
                  faceCenterGlobal[normDim];
                storedNeighborFaceCentersGlobal[mapperIntersection.map(*intersectionPointer)][isOrientationIdxInside][tangDim] =
                  adjacentFaceCenterInsideGlobal0[tangDim];
                storedNeighborVelocities[mapperIntersection.map(*intersectionPointer)][isOrientationIdxInside] =
                  (storedNeighborVelocities[mapperIntersection.map(*intersectionPointer)][1-isOrientationIdxInside]
                  - vInside0[tangDim])
                  / (storedNeighborFaceCentersGlobal[mapperIntersection.map(*intersectionPointer)][1-isOrientationIdxInside][normDim]
                    - adjacentFaceCenterInsideGlobal0[normDim])
                  * (adjacentFaceCenterInsideGlobal0[normDim] - faceCenterGlobal[normDim])
                  + vInside0[tangDim];

                intersectionPointer = is->inside()->template subEntity<1>(adjacentFaceInside1);
                storedNeighborFaceCentersGlobal[mapperIntersection.map(*intersectionPointer)][isOrientationIdxInside][normDim] =
                  faceCenterGlobal[normDim];
                storedNeighborFaceCentersGlobal[mapperIntersection.map(*intersectionPointer)][isOrientationIdxInside][tangDim] =
                  adjacentFaceCenterInsideGlobal1[tangDim];
                storedNeighborVelocities[mapperIntersection.map(*intersectionPointer)][isOrientationIdxInside] =
                  (storedNeighborVelocities[mapperIntersection.map(*intersectionPointer)][1-isOrientationIdxInside]
                  - vInside1[tangDim])
                  / (storedNeighborFaceCentersGlobal[mapperIntersection.map(*intersectionPointer)][1-isOrientationIdxInside][normDim]
                    - adjacentFaceCenterInsideGlobal1[normDim])
                  * (adjacentFaceCenterInsideGlobal1[normDim] - faceCenterGlobal[normDim])
                  + vInside0[tangDim];

                // BOUNDARY CASE for known Dirichlet boundaries
                typedef typename BC::template Child<velocityIdx>::Type BCVelocity;
                const BCVelocity& bcVelocity = bc.template child<velocityIdx>();
                if (bcVelocity.isWall(*is, faceCenterLocal) || bcVelocity.isInflow(*is, faceCenterLocal))
                {
                  // evaluate boundary condition functions
                  typename DirichletVelocity::Traits::RangeType dirichletVelocityCorner0, dirichletVelocityCorner1;
                  Dune::FieldVector<DF, dim> insideFaceCornerLocal0 = is->geometryInInside().global(faceCenterLocal);
                  Dune::FieldVector<DF, dim> insideFaceCornerLocal1 = is->geometryInInside().global(faceCenterLocal);
                  insideFaceCornerLocal0[tangDim] = 0.0;
                  insideFaceCornerLocal1[tangDim] = 1.0;
                  dirichletVelocity.evaluate(*(is->inside()), insideFaceCornerLocal0, dirichletVelocityCorner0);
                  dirichletVelocity.evaluate(*(is->inside()), insideFaceCornerLocal1, dirichletVelocityCorner1);
                  typedef typename GridView::template Codim<1>::EntityPointer IntersectionPointer;
                  //! \todo TODO: is->indexInInside() % 2 assumes SGrid, general case calculates global coordinates
                  IntersectionPointer intersectionPointer = is->inside()->template subEntity<1>(adjacentFaceInside0);
                  storedNeighborVelocities[mapperIntersection.map(*intersectionPointer)][isOrientationIdxInside] =
                    dirichletVelocityCorner0[tangDim];

                  intersectionPointer = is->inside()->template subEntity<1>(adjacentFaceInside1);
                  storedNeighborVelocities[mapperIntersection.map(*intersectionPointer)][isOrientationIdxInside] =
                    dirichletVelocityCorner1[tangDim];
                }
              }
            }
          }
        } // if (dim > 1) for evaluation of stored velocities

        // loop over grid view
        for (ElementIterator eit = gfs.gridView().template begin<0>();
            eit != gfs.gridView().template end<0>(); ++eit)
        {
          for (IntersectionIterator is = gridView.ibegin(*eit);
               is != gridView.iend(*eit); ++is)
          {
            // evaluate orientation of intersection
            unsigned int normDim = 0;
            unsigned int tangDim = 1;
            for (unsigned int curDim = 0; curDim < dim; ++curDim)
            {
              if (std::abs(is->centerUnitOuterNormal()[curDim]) > 1e-10 && dim > 1)
              {
                normDim = curDim;
                tangDim = 1 - curDim;
              }
            }
            // local position of cell and face centers
            const Dune::FieldVector<DF, dim-1>& faceCenterLocal =
              Dune::ReferenceElements<DF, dim-1>::general(is->geometry().type()).position(0, 0);

            // global position of cell and face centers
            Dune::FieldVector<DF, dim> faceCenterGlobal =
              is->geometry().global(faceCenterLocal);

            // bind local function space to element
            lfs.bind(*is->inside());
            LFS_V lfs_v = lfs.template child<velocityIdx>();
            lfsCache.update();
            xView.bind(lfsCache);
            xView.read(xLocal);
            xView.unbind();

            if (dim > 1)
            {
              typedef typename GridView::template Codim<1>::EntityPointer IntersectionPointer;
              IntersectionPointer intersectionPointer = is->inside()->template subEntity<1>(is->indexInInside());
              if ((storedNeighborFaceCentersGlobal[mapperIntersection.map(*intersectionPointer)][1][normDim]
                  > storedNeighborFaceCentersGlobal[mapperIntersection.map(*intersectionPointer)][0][normDim] + 1e-6)
                  || (storedNeighborFaceCentersGlobal[mapperIntersection.map(*intersectionPointer)][1][normDim]
                  < storedNeighborFaceCentersGlobal[mapperIntersection.map(*intersectionPointer)][0][normDim] - 1e-6))
              {
                std::cout << " global normDim = " << normDim << " has wrong value @ " << faceCenterGlobal << std::endl <<
                " storedNeighborFaceCentersGlobal[mapperIntersection.map(*intersectionPointer)][0] " << storedNeighborFaceCentersGlobal[mapperIntersection.map(*intersectionPointer)][0] <<
                " storedNeighborFaceCentersGlobal[mapperIntersection.map(*intersectionPointer)][1] " << storedNeighborFaceCentersGlobal[mapperIntersection.map(*intersectionPointer)][1] <<
                std::endl;
                std::cout << " in " <<  __FILE__ " line " << __LINE__ << std::endl;
                exit(1);
              }
              if (!(storedNeighborFaceCentersGlobal[mapperIntersection.map(*intersectionPointer)][1][tangDim]
                  > storedNeighborFaceCentersGlobal[mapperIntersection.map(*intersectionPointer)][0][tangDim]))
              {
                std::cout << " global tangDim = " << tangDim << " has wrong value @ " << faceCenterGlobal << std::endl <<
                " storedNeighborFaceCentersGlobal[mapperIntersection.map(*intersectionPointer)][0] " << storedNeighborFaceCentersGlobal[mapperIntersection.map(*intersectionPointer)][0] <<
                " storedNeighborFaceCentersGlobal[mapperIntersection.map(*intersectionPointer)][1] " << storedNeighborFaceCentersGlobal[mapperIntersection.map(*intersectionPointer)][1] <<
                std::endl;
                std::cout << " in " <<  __FILE__ " line " << __LINE__ << std::endl;
                exit(1);
              }
            }
          }
        }

        // loop over grid view
        for (ElementIterator eit = gfs.gridView().template begin<0>();
            eit != gfs.gridView().template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementIdInside = mapperElement.map(*eit);

          // normal case
          for (unsigned int curDim = 0; curDim < dim; ++curDim)
          {
            Dune::FieldVector<DF, dim> faceCenterLocalLeft(0.5), faceCenterLocalRight(0.5);
            faceCenterLocalLeft[curDim] = 0.0;
            faceCenterLocalRight[curDim] = 1.0;

            // bind local function space to element
            lfs.bind(*eit);
            LFS_V lfs_v = lfs.template child<velocityIdx>();
            lfsCache.update();
            xView.bind(lfsCache);
            xView.read(xLocal);
            xView.unbind();

            // evaluate solution bound to grid function space for face mid-points
            RangeType vLeft(0.0), vRight(0.0);
            lfs_v.finiteElement().localBasis().evaluateFunction(faceCenterLocalLeft, basisLocal);
            for (unsigned int i = 0; i < lfs_v.size(); ++i)
            {
              vLeft.axpy(xLocal[i], basisLocal[i]);
            }
            lfs_v.finiteElement().localBasis().evaluateFunction(faceCenterLocalRight, basisLocal);
            for (unsigned int i = 0; i < lfs_v.size(); ++i)
            {
              vRight.axpy(xLocal[i], basisLocal[i]);
            }

            Dune::FieldVector<DF, dim> faceCenterGlobalLeft(0.0), faceCenterGlobalRight(0.0);
            faceCenterGlobalLeft = eit->geometry().global(faceCenterLocalLeft);
            faceCenterGlobalRight = eit->geometry().global(faceCenterLocalRight);

            storedVelocityGradientTensor[elementIdInside][curDim][curDim]
              = (vRight[curDim] - vLeft[curDim]) / (faceCenterGlobalRight[curDim] - faceCenterGlobalLeft[curDim]);
          
            storedVelocitiesAtElementCenter[elementIdInside][curDim] = (vRight[curDim] + vLeft[curDim]) / 2.0;
          }

          // tangential case
          if (dim > 1)
          {
            for (unsigned int vDim = 0; vDim < dim; ++vDim)
            {
              for (unsigned int xDim = 0; xDim < dim; ++xDim)
              {
                if (xDim != vDim)
                {
                  typedef typename GridView::template Codim<1>::EntityPointer IntersectionPointer;
                  IntersectionPointer intersectionLeft = eit->template subEntity<1>(2*vDim);
                                 IntersectionPointer intersectionRight = eit->template subEntity<1>(2*vDim+1);
                  Dune::FieldVector<DF, dim> faceCenterLocalLeft(0.5), faceCenterLocalRight(0.5);
                  faceCenterLocalLeft[xDim] = 0.0;
                  faceCenterLocalRight[xDim] = 1.0;
                  Dune::FieldVector<DF, dim> faceCenterGlobalLeft(0.0), faceCenterGlobalRight(0.0);
                  faceCenterGlobalLeft = eit->geometry().global(faceCenterLocalLeft);
                  faceCenterGlobalRight = eit->geometry().global(faceCenterLocalRight);

                  DF velocityDifference =
                    ((storedNeighborVelocities[mapperIntersection.map(*intersectionLeft)][1]
                      + storedNeighborVelocities[mapperIntersection.map(*intersectionRight)][1]) / 2
                    - (storedNeighborVelocities[mapperIntersection.map(*intersectionLeft)][0]
                        + storedNeighborVelocities[mapperIntersection.map(*intersectionRight)][0]) / 2);

                  DF distance =
                    ((storedNeighborFaceCentersGlobal[mapperIntersection.map(*intersectionLeft)][1][xDim]
                      + storedNeighborFaceCentersGlobal[mapperIntersection.map(*intersectionRight)][1][xDim]) / 2.0
                    - (storedNeighborFaceCentersGlobal[mapperIntersection.map(*intersectionLeft)][0][xDim]
                        + storedNeighborFaceCentersGlobal[mapperIntersection.map(*intersectionRight)][0][xDim]) / 2.0);

                  if (std::abs(distance) < 1e-6)
                  {
                    std::cout <<
                    " velocityDifference: " << velocityDifference <<
                    " distance: " << distance <<
                    " @faceCenterGlobalLeft " << faceCenterGlobalLeft <<
                    " faceCenterGlobalRight " << faceCenterGlobalRight <<
                    " with xDim = " << xDim << std::endl <<
                    " storedNeighborFaceCentersGlobal[intersectionLeft][0]: " << storedNeighborFaceCentersGlobal[mapperIntersection.map(*intersectionLeft)][0] <<
                    " storedNeighborFaceCentersGlobal[intersectionRight][0]: " << storedNeighborFaceCentersGlobal[mapperIntersection.map(*intersectionRight)][0] <<
                    std::endl <<
                    " storedNeighborFaceCentersGlobal[intersectionLeft][1]: " << storedNeighborFaceCentersGlobal[mapperIntersection.map(*intersectionLeft)][1] <<
                    " storedNeighborFaceCentersGlobal[intersectionRight][1]: " << storedNeighborFaceCentersGlobal[mapperIntersection.map(*intersectionRight)][1] <<
                    std::endl;
                  }

                  if (!(storedNeighborFaceCentersGlobal[mapperIntersection.map(*intersectionLeft)][0][xDim]
                      < storedNeighborFaceCentersGlobal[mapperIntersection.map(*intersectionLeft)][1][xDim]))
                  {
                    std::cout << " global xDim = " << xDim << " has wrong value" <<
                    " storedNeighborFaceCentersGlobal[mapperIntersection.map(*intersectionLeft)][0] " << storedNeighborFaceCentersGlobal[mapperIntersection.map(*intersectionLeft)][0] <<
                    " storedNeighborFaceCentersGlobal[mapperIntersection.map(*intersectionLeft)][1] " << storedNeighborFaceCentersGlobal[mapperIntersection.map(*intersectionLeft)][1] <<
                    std::endl;
                    std::cout << " in " <<  __FILE__ " line " << __LINE__ << std::endl;
                    exit(1);
                  }

                  if (!(storedNeighborFaceCentersGlobal[mapperIntersection.map(*intersectionRight)][0][xDim]
                      < storedNeighborFaceCentersGlobal[mapperIntersection.map(*intersectionRight)][1][xDim]))
                  {
                    std::cout << " global xDim = " << xDim << " has wrong value" <<
                    " storedNeighborFaceCentersGlobal[mapperIntersection.map(*intersectionRight)][0] " << storedNeighborFaceCentersGlobal[mapperIntersection.map(*intersectionRight)][0] <<
                    " storedNeighborFaceCentersGlobal[mapperIntersection.map(*intersectionRight)][1] " << storedNeighborFaceCentersGlobal[mapperIntersection.map(*intersectionRight)][1] <<
                    std::endl;
                    std::cout << " in " <<  __FILE__ " line " << __LINE__ << std::endl;
                    exit(1);
                  }
                  storedVelocityGradientTensor[elementIdInside][vDim][xDim] = velocityDifference / distance;
                }
              }
            }
          }
        }
      }

    private:
      const BC& bc;
      const SourceMomentumBalance& sourceMomentumBalance;
      const SourceMassBalance& sourceMassBalance;
      const DirichletVelocity& dirichletVelocity;
      const DirichletPressure& dirichletPressure;
      const NeumannVelocity& neumannVelocity;
      const NeumannPressure& neumannPressure;
      GridView gridView;
      //! Mapper to get indices for elements
      MapperElement mapperElement;
      //! Mapper to get indices for intersections (codim=1)
      MapperIntersection mapperIntersection;
    };
  } // namespace PDELab
} // namespace Dune

#endif // DUMUX_EDDY_VISCOSITY_STAGGERED_GRID_HH
