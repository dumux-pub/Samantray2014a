/**
 * \file
 * \ingroup LauferPipe
 *
 * \brief Boundary condition types and boundary condition values for Laufer's
 *        pipe flow experiment.
 */

#ifndef DUMUX_ZEROEQ_PROBLEM_LAUFERPIPE_HH
#define DUMUX_ZEROEQ_PROBLEM_LAUFERPIPE_HH

#include<cmath>
#include<dune/pdelab/common/function.hh>
#include<dune/pdelab/constraints/common/constraintsparameters.hh>

#define ENABLE_NAVIER_STOKES 1

// EDDY VISCOSITY MODEL
// 0 no eddy viscosity model
// 1 prandtl mixing length
// 9 linear eddy visosity distribution
#define WALL_NORMAL_AXIS 1
#define BBOXMIN_IS_WALL 1
#define BBOXMAX_IS_WALL 1
 
// PIPE PROPERTIES
#define VELOCITY 25.0
#define PIPE_LENGTH 20.0
#define PIPE_HEIGHT 0.2469
#define PARABOLIC_VELOCITY_PROFILE 0

// FLUID PROPERTIES
#define KINEMATIC_VISCOSITY 12e-6   // GAS
#define DENSITY 1.21                // GAS 

// #define COMPRESSIBILITY 0
//   #define SPECIFIC_GAS_CONSTANT 287.058
//   #define TEMPERATURE 293
// #define KINEMATIC_VISCOSITY 1e-6 // WATER
// #define DENSITY 1000             // WATER

#define EPSILON 1e-6


/**
 * \brief Boundary condition function for the velocity components.
 */
class BCVelocity
: public Dune::PDELab::DirichletConstraintsParameters
{
public:
  //! \brief Return whether Intersection is Wall Boundary for velocity
  //! \tparam I Intersection type
  template<typename I>
  bool isWall(const I& intersection,
              const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
  {
    Dune::FieldVector<typename I::ctype, I::dimension>
      global = intersection.geometry().global(coord);
    return (!isInflow(intersection, coord) && !isOutflow(intersection, coord));
//             global[1] < EPSILON
//             || global[1] > PIPE_HEIGHT - EPSILON);
  }

  //! \brief Return whether Intersection is Inflow Boundary for velocity
  //! \tparam I Intersection type
  template<typename I>
  bool isInflow(const I& intersection,
                const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
  {
    Dune::FieldVector<typename I::ctype, I::dimension>
      global = intersection.geometry().global(coord);
    return (global[0] < EPSILON);
  }

  //! \brief Return whether Intersection is Outflow Boundary for velocity
  //! \tparam I Intersection type
  template<typename I>
  bool isOutflow(const I& intersection,
                 const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
  {
    Dune::FieldVector<typename I::ctype, I::dimension>
      global = intersection.geometry().global(coord);
    return (global[0] > PIPE_LENGTH - EPSILON);
  }

  //! \brief Return whether Intersection is Symmetry Boundary for velocity
  //! \tparam I Intersection type
  template<typename I>
  bool isSymmetry(const I& intersection,
                  const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
  {
    return false;
  }
};

/**
 * \brief Boundary condition function for the pressure component.
 */
class BCPressure
: public Dune::PDELab::DirichletConstraintsParameters
{
public:
  //! \brief Return whether Intersection is Dirichlet Boundary for pressure
  //! \tparam I Intersection type
  template<typename I>
  bool isDirichlet(const I& intersection,
                   const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
  {
    const Dune::FieldVector<typename I::ctype, I::dimension>
      global = intersection.geometry().global(coord);
    return (global[0] > PIPE_LENGTH - EPSILON);
  }

  //! \brief Return whether Intersection is Outflow Boundary for pressure
  //! \tparam I Intersection type
  template<typename I>
  bool isOutflow(const I& intersection,
                 const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
  {
    return !isDirichlet(intersection, coord); // everywhere else
  }
};


/**
 * \brief Initial velocity field
 *
 * \tparam GV GridView type
 * \tparam RF Range type
 */
template<typename GV, typename RF>
class InitialVelocity
  : public Dune::PDELab::AnalyticGridFunctionBase<
      Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, GV::dimension>, InitialVelocity<GV, RF> >
{
public:
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, GV::dimension> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, InitialVelocity<GV, RF> > BaseT;

  //! \brief Constructor
  InitialVelocity (const GV& gv)
  : BaseT(gv) {}

  //! \brief Evaluate the function globally
  inline void evaluateGlobal(const typename Traits::DomainType& x,
                             typename Traits::RangeType& y) const
  {
    y = 0.0;
    if (x[1] > EPSILON && x[1] < PIPE_HEIGHT - EPSILON)
    {
#if PARABOLIC_VELOCITY_PROFILE
      y[0] = VELOCITY * (4.0 * x[1] * (PIPE_HEIGHT - x[1]) / PIPE_HEIGHT / PIPE_HEIGHT);
#else
      y[0] = VELOCITY;
#endif
    }
  }
};

/**
 * \brief Initial pressure field
 *
 * \tparam GV GridView type
 * \tparam RF Range type
 */
template<typename GV, typename RF>
class InitialPressure
  : public Dune::PDELab::AnalyticGridFunctionBase<
      Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, 1>, InitialPressure<GV, RF> >
{
public:
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, 1> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, InitialPressure<GV, RF> > BaseT;

  //! \brief Constructor
  InitialPressure (const GV& gv)
  : BaseT(gv) {}

  //! \brief Evaluate the function globally
  inline void evaluateGlobal(const typename Traits::DomainType& x,
                             typename Traits::RangeType& y) const
  {
    y = 2.0e5 - 1.0e5 * (PIPE_LENGTH / 10.0);
//     y = 1.0e5;
  }
};


/**
 * \brief Function for velocity Dirichlet boundary conditions and initialization.
 *
 * \tparam GV GridView type
 * \tparam RF Range type
 */
template<typename GV, typename RF>
class DirichletVelocity
: public Dune::PDELab::AnalyticGridFunctionBase<
    Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, GV::dimension>, DirichletVelocity<GV, RF> >,
  public Dune::PDELab::InstationaryFunctionDefaults
{
public:
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, GV::dimension> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, DirichletVelocity<GV, RF> > BaseT;

  //! \brief Constructor
  DirichletVelocity (const GV& gv)
  : BaseT(gv) {}

  //! \brief Evaluate the function globally
  inline void evaluateGlobal(const typename Traits::DomainType& x,
                             typename Traits::RangeType& y) const
  {
    y = 0.0;
    if (x[1] > EPSILON && x[1] < PIPE_HEIGHT - EPSILON)
    {
#if PARABOLIC_VELOCITY_PROFILE
      y[0] = VELOCITY * (4.0 * x[1] * (PIPE_HEIGHT - x[1]) / PIPE_HEIGHT / PIPE_HEIGHT);
#else
      y[0] = VELOCITY;
#endif
    }
  }
};

/**
 * \brief Function for pressure Dirichlet boundary conditions and initialization.
 *
 * \tparam GV GridView type
 * \tparam RF Range type
 */
//template<typename I>
//  bool isDirichlet(const I& intersection,
//                   const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
//  {
//    const Dune::FieldVector<typename I::ctype, I::dimension>
//      global = intersection.geometry().global(coord);
//    return (global[0] > PIPE_LENGTH - EPSILON);
//  }
//
//  //! \brief Return whether Intersection is Outflow Boundary for pressure
//  //! \tparam I Intersection type
//  template<typename I>
//  bool isOutflow(const I& intersection,
//                 const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
//  {
//    return !isDirichlet(intersection, coord); // everywhere else
//  }
//};
template<typename GV, typename RF>
class DirichletPressure
: public Dune::PDELab::AnalyticGridFunctionBase<
    Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, 1>, DirichletPressure<GV, RF> >,
  public Dune::PDELab::InstationaryFunctionDefaults
{
public:
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, 1> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, DirichletPressure<GV, RF> > BaseT;

  //! \brief Constructor
  DirichletPressure (const GV& gv)
  : BaseT(gv) {}

  //! \brief Evaluate the function globally
  inline void evaluateGlobal(const typename Traits::DomainType& x,
                             typename Traits::RangeType& y) const
  {
	 y=0;
    //y = 2.0e5 - 1.0e5 * (PIPE_LENGTH / 10.0);
//     y = 1.0e5;
  }
};


/**
 * \brief Function for velocity Neumann boundary conditions and initialization.
 *
 * \tparam GV GridView type
 * \tparam RF Range type
 */
template<typename GV, typename RF>
class NeumannVelocity
: public Dune::PDELab::AnalyticGridFunctionBase<
    Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, GV::dimension>, NeumannVelocity<GV, RF> >
{
public:
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, GV::dimension> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, NeumannVelocity<GV, RF> > BaseT;

  //! \brief Constructor
  NeumannVelocity (const GV& gv)
  : BaseT(gv) {}

  //! \brief Evaluate the function globally
  inline void evaluateGlobal(const typename Traits::DomainType& x,
                             typename Traits::RangeType& y) const
  {
    y = 0;
  }
};

/**
 * \brief Function for pressure Neumann boundary conditions and initialization.
 *
 * \tparam GV GridView type
 * \tparam RF Range type
 */
template<typename GV, typename RF>
class NeumannPressure
: public Dune::PDELab::AnalyticGridFunctionBase<
    Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, 1>, NeumannPressure<GV, RF> >
{
public:
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, 1> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, NeumannPressure<GV, RF> > BaseT;

  //! \brief Constructor
  NeumannPressure (const GV& gv)
  : BaseT(gv) {}

  //! \brief Evaluate the function globally
  inline void evaluateGlobal(const typename Traits::DomainType& x,
                             typename Traits::RangeType& y,
                             int test = 1) const
  {
    y = 0;
  }
};


/**
 * \brief Source term function for the momentum balance.
 *
 * \tparam GV GridView type
 * \tparam RF Range type
 */
template<typename GV, typename RF>
class SourceMomentumBalance
  : public Dune::PDELab::AnalyticGridFunctionBase<
      Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, GV::dimension>, SourceMomentumBalance<GV, RF> >
{
public:
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, GV::dimension> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, SourceMomentumBalance<GV, RF> > BaseT;

  //! \brief Constructor
  SourceMomentumBalance (const GV& gv)
  : BaseT(gv) {}

  //! \brief Evaluate the function globally
  inline void evaluateGlobal(const typename Traits::DomainType& x,
                             typename Traits::RangeType& y) const
  {
    y = 0.0;
  }
};

/**
 * \brief Source term function for the mass balance.
 *
 * \tparam GV GridView type
 * \tparam RF Range type
 */
template<typename GV, typename RF>
class SourceMassBalance
  : public Dune::PDELab::AnalyticGridFunctionBase<
      Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, 1>, SourceMassBalance<GV, RF> >
{
public:
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, 1> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, SourceMassBalance<GV, RF> > BaseT;

  //! \brief Constructor
  SourceMassBalance (const GV& gv)
  : BaseT(gv) {}

  //! \brief Evaluate the function globally
  inline void evaluateGlobal(const typename Traits::DomainType& x,
                             typename Traits::RangeType& y) const
  {
    y = 0.0;
  }
};

#endif // DUMUX_ZEROEQ_PROBLEM_LAUFERPIPE_HH
