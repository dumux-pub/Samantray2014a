/**
 * \file
 * \ingroup StaggeredGrid
 * \ingroup ZeroEqStaggeredGrid
 *
 * \copydoc EddyViscosityStaggeredGrid
 *
 * \brief Local operator for staggered grid discretization for steady-state
 * Navier-Stokes equation, modeled with algebraic turbulence models.
 *
 * The eddy viscosity depends on the chosen model, but looks in general like this:
 * \f[
 *    \nu_{\alpha,\textrm{t}}
 *    = \nu_{\alpha,\textrm{t}} \left( y, \left| \frac{\partial u}{\partial y} \right| \right)
 *    > 0
 * \f]
 *
 * The following models are available and can be chosen via setting <tt>eddyViscosityModel</tt>,
 * for detailed description have look at the individual functions in the local operator.<br>
 * (0) No eddy viscosity model<br>
 * (1) Prandtl's mixing length<br>
 * (2) Van Driest's modification<br>
 * (3) Cebeci-Smith model<br>
 * (4) Baldwin-Lomax model<br>
 * (9) artificial linear eddy viscosity distribution<br>
 *
 * \note Be aware that the storage terms (the time dependent terms)
 * are not included here but are implemented in the transient part
 * of the local operator.
 */

#ifndef DUMUX_BASE_ZEROEQ_STAGGERED_GRID_HH
#define DUMUX_BASE_ZEROEQ_STAGGERED_GRID_HH

#include<cstddef>
#include<vector>

#include<dune/common/exceptions.hh>
#include<dune/common/fvector.hh>
#include<dune/common/fmatrix.hh>

#include<dune/geometry/referenceelements.hh>
#include<dune/geometry/type.hh>

#include<dune/grid/common/mcmgmapper.hh>

#include<dune/pdelab/gridfunctionspace/localfunctionspace.hh>
#include<dune/pdelab/gridfunctionspace/lfsindexcache.hh>
#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/localoperator/idefault.hh>
#include<dune/pdelab/localoperator/flags.hh>
#include<dune/pdelab/localoperator/pattern.hh>

#include<dune/geometry/quadraturerules.hh>

#include<appl/staggeredgrid/zeroeq/eddyviscositystaggeredgrid.hh>

namespace Dune
{
  namespace PDELab
  {
    /**
     * \brief Local operator for staggered grid discretization solving
     * the steady-state Navier-Stokes equation.
     *
     * \tparam BC Boundary condition type
     * \tparam SourceMomentumBalance Source type for velocity
     * \tparam SourceMassBalance Source type for pressure
     * \tparam DirichletVelocity Dirchlet boundary condition for velocity
     * \tparam DirichletPressure Dirchlet boundary condition for pressure
     * \tparam NeumannVelocity Dirchlet boundary condition for velocity
     * \tparam NeumannPressure Dirchlet boundary condition for pressure
     * \tparam GridView Grid view type
     */
    template<typename BC, typename SourceMomentumBalance, typename SourceMassBalance,
             typename DirichletVelocity, typename DirichletPressure,
             typename NeumannVelocity, typename NeumannPressure,
             typename GridView>
    class BaseZeroEqStaggeredGrid
    : public EddyViscosityStaggeredGrid<BC,
        SourceMomentumBalance, SourceMassBalance,
        DirichletVelocity, DirichletPressure,
        NeumannVelocity, NeumannPressure,
        GridView>
    {
    public:
      typedef EddyViscosityStaggeredGrid<BC,
        SourceMomentumBalance, SourceMassBalance,
        DirichletVelocity, DirichletPressure,
        NeumannVelocity, NeumannPressure,
        GridView> ParentType;
      typedef Dune::MultipleCodimMultipleGeomTypeMapper<GridView, Dune::MCMGIntersectionLayout>
        MapperIntersection;
      typedef Dune::MultipleCodimMultipleGeomTypeMapper<GridView, Dune::MCMGElementLayout>
        MapperElement;
      std::vector<double> storedRoughness;
      std::vector<double> storedAdditionalRoughnessLength;
      std::vector<double> storedVelocityThickness;
      std::vector<double> storedBoundaryLayerThickness;
      std::vector<double> storedSwitchingPosition;
      std::vector<double> storedFMax;
      std::vector<double> storedYFMax;
      std::vector<double> storedKinematicEddyViscosityInner;
      std::vector<double> storedKinematicEddyViscosityOuter;
      std::vector<double> storedKinematicEddyViscosityDifference;
      std::vector<Dune::FieldVector<double, GridView::dimension> > storedPressureGradient;
      std::vector<Dune::FieldVector<double, GridView::dimension> > storedVelocityMaximum;
      std::vector<Dune::FieldVector<double, GridView::dimension> > storedVelocityMinimum;

      //! \brief Pattern assembly flags
      enum { doPatternVolume = true,
             doPatternSkeleton = true };

      //! \brief Residual assembly flags
      enum { doAlphaVolume = true,
             doAlphaSkeleton = true,
             doAlphaBoundary = true };

      //! \brief Index of unknowns
      enum { velocityIdx = 0,
             pressureIdx = 1 };

      //! \brief Constructor
      BaseZeroEqStaggeredGrid(const BC& bc_,
                              const SourceMomentumBalance& sourceMomentumBalance_, const SourceMassBalance& sourceMassBalance_,
                              const DirichletVelocity& dirichletVelocity_, const DirichletPressure& dirichletPressure_,
                              const NeumannVelocity& neumannVelocity_, const NeumannPressure& neumannPressure_,
                              GridView gridView_, const unsigned int eddyViscosityModel)
        : ParentType(bc_,
              sourceMomentumBalance_, sourceMassBalance_,
              dirichletVelocity_, dirichletPressure_,
              neumannVelocity_, neumannPressure_,
              gridView_),
          bc(bc_),
          sourceMomentumBalance(sourceMomentumBalance_), sourceMassBalance(sourceMassBalance_),
          dirichletVelocity(dirichletVelocity_), dirichletPressure(dirichletPressure_),
          neumannVelocity(neumannVelocity_), neumannPressure(neumannPressure_),
          gridView(gridView_),
          mapperElement(gridView_), mapperIntersection(gridView_)

      {
        initialize(gridView);
      }

      /**
       * \brief Contribution to volume integral.
       *
       * Volume integral depending on test and ansatz functions.
       *
       * \tparam EG element geometry
       * \tparam LFSU local function space for ansatz functions
       * \tparam X coefficient vector
       * \tparam LFSV local function space for test functions
       * \tparam R residual vector
       * \param eg element geometry
       * \param lfsu local functions space for ansatz functions
       * \param x coefficient vector
       * \param lfsv local function space for test functions
       * \param r residual vector
       */
      template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_volume(const EG& eg, const LFSU& lfsu, const X& x,
                        const LFSV& lfsv, R& r) const
      {
        ParentType::alpha_volume(eg, lfsu, x, lfsv, r);
      }


      /**
       * \brief Skeleton integral depending on test and ansatz functions.
       *
       * Contribution of flux over interface. Each face is only visited once!
       *
       * \tparam IG interface geometry over which the fluxes are evaluated
       * \tparam LFSU local function space for ansatz functions
       * \tparam X coefficient vector
       * \tparam LFSV local function space for test functions
       * \tparam R residual vector
       * \param ig interface geometry
       * \param lfsu_s local functions space for ansatz functions of self/inside
       * \param x_s coefficient vector of self/inside
       * \param lfsv_s local function space for test functions of self/inside
       * \param lfsu_n local functions space for ansatz functions of neighbor/outside
       * \param x_n coefficient vector of neighbor/outside
       * \param lfsv_n local function space for test functions of neighbor/outside
       * \param r_s residual vector of self/inside
       * \param r_n residual vector of neighbor/outside
       */
      template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_skeleton(const IG& ig,
                          const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s,
                          const LFSU& lfsu_n, const X& x_n, const LFSV& lfsv_n,
                          R& r_s, R& r_n) const
      {
        ParentType::alpha_skeleton(ig, lfsu_s, x_s, lfsv_s, lfsu_n, x_n,  lfsv_n, r_s, r_n);
      }

      /**
       * \brief Boundary integral depending on test and ansatz functions
       *
       * We put the Dirchlet evaluation also in the alpha term to save
       * some geometry evaluations.
       *
       * \tparam IG interface geometry over which the fluxes are evaluated
       * \tparam LFSU local function space for ansatz functions
       * \tparam X coefficient vector
       * \tparam LFSV local function space for test functions
       * \tparam R residual vector
       * \param ig interface geometry
       * \param lfsu_s local functions space for ansatz functions
       * \param x_s coefficient vector
       * \param lfsv_s local function space for test functions
       * \param r_s residual vector
       */
      template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_boundary(const IG& ig, const LFSU& lfsu_s, const X& x_s,
                          const LFSV& lfsv_s, R& r_s) const
      {
        ParentType::alpha_boundary(ig, lfsu_s, x_s, lfsv_s, r_s);
      }

      /**
       * \brief Initialize vector-stored values used for alpha_* routines.
       *
       * Travers grid and store values like relation to next wall.<br>
       * In a first step, this method writes the ID and coordinates of all elements
       * having a boundary face into a vector. In the second traversing grid step,
       * to each individual elements the relation to it's shortest corresponding
       * wall element is found and stored.
       *
       * \tparam GridView GridView type
       */
      void initialize(const GridView& gridView)
      {
        ParentType::initialize(gridView);
//         std::cout << "initialize zeroeq" << std::endl;

        const int dim = GridView::dimension;
        typedef typename GridView::Traits::template Codim<0>::Iterator ElementIterator;

        storedRoughness.resize(mapperElement.size());
        storedAdditionalRoughnessLength.resize(mapperElement.size());
        storedVelocityThickness.resize(mapperElement.size());
        storedBoundaryLayerThickness.resize(mapperElement.size());
        storedSwitchingPosition.resize(mapperElement.size());
        storedFMax.resize(mapperElement.size());
        storedYFMax.resize(mapperElement.size());
        storedKinematicEddyViscosityInner.resize(mapperElement.size());
        storedKinematicEddyViscosityOuter.resize(mapperElement.size());
        storedKinematicEddyViscosityDifference.resize(mapperElement.size());
        storedPressureGradient.resize(mapperElement.size());
        storedVelocityMaximum.resize(mapperElement.size());
        storedVelocityMinimum.resize(mapperElement.size());

        for (unsigned int i = 0; i < mapperElement.size(); ++i)
        {
          storedRoughness[i] = 0.0;
          storedAdditionalRoughnessLength[i] = 0.0;
          storedVelocityThickness[i] = 0.0;
          storedBoundaryLayerThickness[i] = 0.001;
          storedSwitchingPosition[i] = 0.0;
          storedFMax[i] = 0.0;
          storedYFMax[i] = 0.0;
          storedKinematicEddyViscosityInner[i] = 0.0;
          storedKinematicEddyViscosityOuter[i] = 0.0;
          storedKinematicEddyViscosityDifference[i] = 0.0;
          for (unsigned int j = 0; j < dim; ++j)
          {
            storedPressureGradient[i][j] = 0.0;
            storedVelocityMaximum[i][j] = 0.0;
            storedVelocityMinimum[i][j] = 0.0;
          }
        }

        // loop over grid view
        for (ElementIterator eit = gridView.template begin<0>();
            eit != gridView.template end<0>(); ++eit)
        {
          const Dune::FieldVector<double, dim>& cellCenterLocal =
            Dune::ReferenceElements<double, dim>::general(eit->geometry().type()).position(0, 0);
          Dune::FieldVector<double, dim> cellCenterGlobal = eit->geometry().global(cellCenterLocal);
          storedRoughness[mapperElement.map(*eit)] = dirichletVelocity.roughness(*eit, cellCenterGlobal);
        }

        std::cout << "Used eddy viscosity model = ";
        switch (eddyViscosityModel)
        {
          case 0:
            std::cout << "no eddy viscosity model" << std::endl;
            break;
          case 1:
            std::cout << "prandtl" << std::endl;
            break;
          case 2:
            std::cout << "vanDriest" << std::endl;
            break;
          case 3:
            std::cout << "cebeciSmith" << std::endl;
            break;
          case 4:
            std::cout << "baldwinLomax" << std::endl;
            break;
          case 9:
            std::cout << "linear" << std::endl;
              break;
          default:
            DUNE_THROW(Dune::NotImplemented, "This eddy viscosity method is not implemented.");
        }
      }

      /**
       * \brief Update vector-stored values used for alpha_* routines.
       *
       * In a first step, this method writes the ID and coordinates of all elements
       * having a boundary face into a vector. In the second traversing grid step,
       * to each individual elements the relation to it's shortest corresponding
       * wall element is found and stored.
       * The velocities are copied across each intersection to be available on the other
       * side adjacent face. This is only necessary for the tangential case of the viscous
       * term, which means <tt>dim > 1</tt>.
       * For the eddy viscosity models also the velocity gradient tensor containing
       * the gradient of each velocity component in all spatial dimension is stored
       * per element.
       *
       * \tparam GFS GridFunctionSpace type
       * \tparam X Coefficient vector
       * \param gfs Grid function space
       * \param lastSolution Coefficient vector from last Newton step or time step
       */
      template<typename GFS, typename X>
      void updateStoredValues(const GFS& gfs, X& lastSolution)
      {
        ParentType::updateStoredValues(gfs, lastSolution);
//         std::cout << "updateStoredValues zeroeq" << std::endl;

        // constants and types
        const int dim = GridView::dimension;
        typedef typename GridView::Traits::template Codim<0>::Iterator ElementIterator;
        typedef typename GridView::IntersectionIterator IntersectionIterator;

        // loop over grid view
        for (ElementIterator eit = gfs.gridView().template begin<0>();
            eit != gfs.gridView().template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.map(*eit);

          // reset values
          storedBoundaryLayerThickness[elementInsideID] = 0.0;
          storedVelocityThickness[elementInsideID] = 0.0;
          storedSwitchingPosition[elementInsideID] = 999999.9;
          storedFMax[elementInsideID] = 0.0;
          for (unsigned int j = 0; j < dim; ++j)
          {
            storedVelocityMaximum[elementInsideID][j] = 0.0;
            storedVelocityMinimum[elementInsideID][j] = 999999.9;
          }

          /*dirty hack to recalculate u+*/
          double uStarDividedByViscosity = this->storedDistanceInWallCoordinates[elementInsideID]
                                           / this->storedDistanceToWall[elementInsideID];
          if (uStarDividedByViscosity > 0.0)
          {
            double dimensionlessEquivalentSandGrainRoughness 
                    = storedRoughness[this->storedCorrespondingWallElementID[elementInsideID]]
                      * uStarDividedByViscosity;
            storedAdditionalRoughnessLength[elementInsideID]
              = 0.9 / uStarDividedByViscosity
                * (std::sqrt(dimensionlessEquivalentSandGrainRoughness)
                  - dimensionlessEquivalentSandGrainRoughness * std::exp(-dimensionlessEquivalentSandGrainRoughness / 6.0));
          }

          // make local function space
          typedef Dune::PDELab::LocalFunctionSpace<GFS> LFS;
          typedef typename LFS::template Child<pressureIdx>::Type LFS_P;
          typedef Dune::PDELab::LFSIndexCache<LFS> LFSCache;
          typedef typename X::template ConstLocalView<LFSCache> XView;

          typedef typename LFS_P::Traits::FiniteElementType::
            Traits::LocalBasisType::Traits FETraits;
          typedef typename FETraits::DomainFieldType DF;
          typedef typename FETraits::RangeFieldType RF;
          typedef typename FETraits::RangeType RangeType;

          LFS lfs(gfs);
          LFSCache lfsCache(lfs);
          XView xView(lastSolution);
          std::vector<RF> xLocal(lfs.maxSize());
          std::vector<RangeType> basisLocal(lfs.maxSize());

          for (unsigned int i = 0; i < dim; ++i)
          {
            storedPressureGradient[elementInsideID][i] = 0.0;
          }
          for (IntersectionIterator is = gridView.ibegin(*eit);
              is != gridView.iend(*eit); ++is)
          {
            if (is->neighbor())
            {
              // evaluate orientation of intersection
              unsigned int normDim = 0;
              for (unsigned int curDim = 0; curDim < dim; ++curDim)
              {
                if (std::abs(is->centerUnitOuterNormal()[curDim]) > 1e-10)
                {
                  normDim = curDim;
                }
              }

              // local position of cell and face centers
              const Dune::FieldVector<DF, dim>& insideCellCenterLocal =
                Dune::ReferenceElements<DF, dim>::general(is->inside()->type()).position(0, 0);
              const Dune::FieldVector<DF, dim>& outsideCellCenterLocal =
                Dune::ReferenceElements<DF, dim>::general(is->outside()->type()).position(0, 0);

              // global position of cell and face centers
              Dune::FieldVector<DF, dim> insideCellCenterGlobal =
                is->inside()->geometry().global(insideCellCenterLocal);
              Dune::FieldVector<DF, dim> outsideCellCenterGlobal =
                is->outside()->geometry().global(outsideCellCenterLocal);

              // bind local function space to inside element
              lfs.bind(*is->inside());
              LFS_P lfs_p_s = lfs.template child<pressureIdx>();
              lfsCache.update();
              xView.bind(lfsCache);
              xView.read(xLocal);
              xView.unbind();

              // evaluate solution bound to grid function space for face mid-points
              RangeType pressure_s(0.0);
              lfs_p_s.finiteElement().localBasis().evaluateFunction(insideCellCenterGlobal, basisLocal);
              for (unsigned int i = 0; i < lfs_p_s.size(); ++i)
              {
                pressure_s.axpy(xLocal[lfs_p_s.localIndex(i)], basisLocal[i]);
              }

              // bind local function space to outside element
              lfs.bind(*is->outside());
              LFS_P lfs_p_n = lfs.template child<pressureIdx>();
              lfsCache.update();
              xView.bind(lfsCache);
              xView.read(xLocal);
              xView.unbind();

              // evaluate solution bound to grid function space for face mid-points
              RangeType pressure_n(0.0);
              lfs_p_n.finiteElement().localBasis().evaluateFunction(outsideCellCenterGlobal, basisLocal);
              for (unsigned int i = 0; i < lfs_p_n.size(); ++i)
              {
                pressure_n.axpy(xLocal[lfs_p_n.localIndex(i)], basisLocal[i]);
              }

              storedPressureGradient[elementInsideID][normDim] += (pressure_n - pressure_s)
                                                                  / (insideCellCenterGlobal[normDim] - outsideCellCenterGlobal[normDim]);
            }
          }

          //! \bug correct values for elements without a boundary (two values have been summed up)
          //! worked only with this strange procedure
          for (IntersectionIterator is = gridView.ibegin(*eit);
              is != gridView.iend(*eit); ++is)
          {
            if (is->neighbor())
            {
              // evaluate orientation of intersection
              unsigned int normDim = 0;
              for (unsigned int curDim = 0; curDim < dim; ++curDim)
              {
                if (std::abs(is->centerUnitOuterNormal()[curDim]) > 1e-10)
                {
                  normDim = curDim;
                }
              }
              storedPressureGradient[elementInsideID][normDim] /= 2.0;
            }
          }
          // strange behavior, but this line is needed
          for (unsigned int i = 0; i < dim; ++i)
          {
            storedPressureGradient[elementInsideID][i] *= 2.0;
          }
        }

        // get maximum and minimum velocities
        for (ElementIterator eit = gfs.gridView().template begin<0>();
            eit != gfs.gridView().template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.map(*eit);
          const typename GridView::IndexSet::IndexType wallElementID = this->storedCorrespondingWallElementID[mapperElement.map(*eit)];

          for (unsigned int curDim = 0; curDim < dim; ++curDim)
          {
            // maximum velocity
            if (storedVelocityMaximum[wallElementID][curDim] < this->storedVelocitiesAtElementCenter[elementInsideID][curDim])
            {
              storedVelocityMaximum[wallElementID][curDim] = this->storedVelocitiesAtElementCenter[elementInsideID][curDim];
            }

            // minimum velocity
            if (storedVelocityMinimum[wallElementID][curDim] > this->storedVelocitiesAtElementCenter[elementInsideID][curDim])
            {
              storedVelocityMinimum[wallElementID][curDim] = this->storedVelocitiesAtElementCenter[elementInsideID][curDim];
            }
          }
        }

        // calculate boundary layer thickness
        for (ElementIterator eit = gfs.gridView().template begin<0>();
            eit != gfs.gridView().template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.map(*eit);
          const typename GridView::IndexSet::IndexType wallElementID = this->storedCorrespondingWallElementID[mapperElement.map(*eit)];

          unsigned int wallNormalAxis = this->storedCorrespondingWallNormalAxis[elementInsideID];
          if (storedBoundaryLayerThickness[wallElementID] < this->storedDistanceToWall[elementInsideID]
              && this->storedVelocitiesAtElementCenter[elementInsideID][1-wallNormalAxis]
                 < 0.99 * this->storedVelocityMaximum[wallElementID][1-wallNormalAxis])
          {
            storedBoundaryLayerThickness[wallElementID] = this->storedDistanceToWall[elementInsideID];
          }
        }

        switch (eddyViscosityModel)
        {
          case 0:
            for (ElementIterator eit = gfs.gridView().template begin<0>();
                  eit != gfs.gridView().template end<0>(); ++eit)
            {
              const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.map(*eit);
              this->storedKinematicEddyViscosity[elementInsideID] = 0.0;
            }
            break;
          case 1:
            prandtlMixingLength(gfs, gridView);
            break;
          case 2:
              vanDriest(gfs, gridView);
            break;
          case 3:
            cebeciSmith(gfs, gridView);
            break;
          case 4:
            baldwinLomax(gfs, gridView);
            break;
          case 9:
            linear(gfs, gridView);
            break;
          default:
            DUNE_THROW(Dune::NotImplemented, "This eddy viscosity method is not implemented.");
        }
#if PRINT_EDDY_VISCOSITY
Dune::FieldVector<double, GridView::dimension> cellCenterLocal(0.5);
Dune::FieldVector<double, GridView::dimension> cellCenterGlobal = eit->geometry().global(cellCenterLocal);

double eps = 0.1;
double velocityGradient = this->storedVelocityGradientTensor[mapperElement.map(*eit)][1-WALL_NORMAL_AXIS][WALL_NORMAL_AXIS];
double wallDistance = this->storedDistanceToWall[mapperElement.map(*eit)];
double kinematicEddyViscosity = this->storedKinematicEddyViscosity[mapperElement.map(*eit)];
if ((cellCenterGlobal[0] > 5.0 - eps) && (cellCenterGlobal[0] < 5.0 + eps))
{
  std::cout << "EDDY VISCOSITY @" <<
  " cellCenterGlobal: " << cellCenterGlobal <<
  " velocityGradient: " << velocityGradient <<
  " wallDistance: " << wallDistance <<
  " kinematicEddyViscosity: " << kinematicEddyViscosity <<
  std::endl;
}
#endif // PRINT_EDDY_VISCOSITY
      }

      /**
       * \brief Calculates the eddy viscosity based on Prandtl's mixing length approach
       *
       * \f[ \nu_\textrm{t} = l_\textrm{mix}^2 \left| \frac{\partial u}{\partial y} \right| \f]
       * with \f$ l_\textrm{mix} = \kappa y \f$ and \f$ \kappa = 0.4 \f$.
       */
      template<typename GFS>
      void prandtlMixingLength(const GFS& gfs, const GridView& gridView)
      {
        typedef typename GridView::Traits::template Codim<0>::Iterator ElementIterator;
        for (ElementIterator eit = gfs.gridView().template begin<0>();
             eit != gfs.gridView().template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.map(*eit);

          double wallDistance = this->storedDistanceToWall[elementInsideID]
                                + storedAdditionalRoughnessLength[elementInsideID];
          unsigned wallNormalAxis = this->storedCorrespondingWallNormalAxis[elementInsideID];
          double velocityGradient = this->storedVelocityGradientTensor[elementInsideID][1-wallNormalAxis][wallNormalAxis];
          double mixingLength = KARMAN_CONSTANT * wallDistance;
          this->storedKinematicEddyViscosity[elementInsideID]
            = mixingLength * mixingLength * std::abs(velocityGradient);
        }
      }

      /**
       * \brief Calculates the eddy viscosity based on van Driest's formula
       *
       * \f[ \nu_\textrm{t} = l_\textrm{mix}^2 \left| \frac{\partial u}{\partial y} \right| \f]
       * with \f$ l_\textrm{mix} = \kappa y \left[ 1 - exp \left( - y^+ / A^+ \right) \right] \f$
       * and \f$ \kappa = 0.4 \f$, \f$ A^+ = 26 \f$.
       */
      template<typename GFS>
      void vanDriest(const GFS& gfs, const GridView& gridView)
      {
        typedef typename GridView::Traits::template Codim<0>::Iterator ElementIterator;
        for (ElementIterator eit = gfs.gridView().template begin<0>();
             eit != gfs.gridView().template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.map(*eit);

          double wallDistance = this->storedDistanceToWall[elementInsideID]
                                + storedAdditionalRoughnessLength[elementInsideID];
          unsigned wallNormalAxis = this->storedCorrespondingWallNormalAxis[elementInsideID];
          double velocityGradient = this->storedVelocityGradientTensor[elementInsideID][1-wallNormalAxis][wallNormalAxis];
          double yPlus =  this->storedDistanceInWallCoordinates[elementInsideID];
          double aPlus = 26.0;
          double mixingLength = KARMAN_CONSTANT * wallDistance * (1.0 - exp(-yPlus / aPlus));
          this->storedKinematicEddyViscosity[elementInsideID]
            = mixingLength * mixingLength * std::abs(velocityGradient);
        }
      }

      /**
       * \brief Calculates the eddy viscosity based on Cebeci Smith's model
       */
      template<typename GFS>
      void cebeciSmith(const GFS& gfs, const GridView& gridView)
      {
        typedef typename GridView::Traits::template Codim<0>::Iterator ElementIterator;

        // (1) calculate velocity thickness
        for (ElementIterator eit = gfs.gridView().template begin<0>();
             eit != gfs.gridView().template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.map(*eit);
          const typename GridView::IndexSet::IndexType wallElementID = this->storedCorrespondingWallElementID[mapperElement.map(*eit)];
          unsigned wallNormalAxis = this->storedCorrespondingWallNormalAxis[elementInsideID];

          // calculate velocity DoF positions
          const int dim = GridView::dimension;
          const unsigned int numControlVolumeFaces =
            Dune::ReferenceElements<double, dim>::general(eit->geometry().type()).size(1);
          std::vector<Dune::FieldVector<double, dim> > faceCentersLocal(numControlVolumeFaces);
          std::vector<Dune::FieldVector<double, dim> > faceCentersGlobal(numControlVolumeFaces);
          for (unsigned int curFace = 0; curFace < numControlVolumeFaces; ++curFace)
          {
            std::fill(faceCentersLocal[curFace].begin(), faceCentersLocal[curFace].end(), 0.5);
            faceCentersLocal[curFace][curFace/2] = curFace % 2;
            faceCentersGlobal[curFace] = eit->geometry().global(faceCentersLocal[curFace]);
          }

          // calculate the staggered face volume (goes through cell center) perpendicular to each direction
          Dune::FieldVector<double, dim> orthogonalFaceVolumes(0.0);
          switch (dim)
          {
            case 2:
              orthogonalFaceVolumes[0] = std::abs(faceCentersGlobal[3][1] - faceCentersGlobal[2][1]);
              orthogonalFaceVolumes[1] = std::abs(faceCentersGlobal[1][0] - faceCentersGlobal[0][0]);
              break;
            default:
              DUNE_THROW(Dune::NotImplemented, "Cebeci Smith model is not implemented for 1D and 3D cases.");
          }

          // evaluate over total height, because if boundary layer edge is reached, 0 is summed up
          this->storedVelocityThickness[wallElementID] += (1.0 - this->storedVelocitiesAtElementCenter[elementInsideID][1-wallNormalAxis]
                                                                 / this->storedVelocityMaximum[wallElementID][1-wallNormalAxis])
                                                          * orthogonalFaceVolumes[1-wallNormalAxis];
        }

        // (2) calculate inner and outer viscosity
        for (ElementIterator eit = gfs.gridView().template begin<0>();
             eit != gfs.gridView().template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.map(*eit);
          const typename GridView::IndexSet::IndexType wallElementID = this->storedCorrespondingWallElementID[mapperElement.map(*eit)];

          double wallDistance = this->storedDistanceToWall[elementInsideID]
                                + storedAdditionalRoughnessLength[elementInsideID];
          unsigned wallNormalAxis = this->storedCorrespondingWallNormalAxis[elementInsideID];
          double yPlus = this->storedDistanceInWallCoordinates[elementInsideID];
          double kinematicViscosityAtWall = KINEMATIC_VISCOSITY;
          double densityAtWall = DENSITY;
          double uStar = std::sqrt(kinematicViscosityAtWall * densityAtWall
                                   * std::abs(this->storedVelocityGradientTensor[wallElementID][1-wallNormalAxis][wallNormalAxis]))
                         / DENSITY;
          double aPlus = 26.0 / std::sqrt(1.0 + wallDistance * this->storedPressureGradient[elementInsideID][1-wallNormalAxis]
                                                / (DENSITY * uStar * uStar));
          double mixingLength = KARMAN_CONSTANT * wallDistance * (1.0 - exp(-yPlus / aPlus));
          this->storedKinematicEddyViscosityInner[elementInsideID]
            = mixingLength * mixingLength
              * std::sqrt(std::pow(this->storedVelocityGradientTensor[elementInsideID][1][0], 2.0)
                          + std::pow(this->storedVelocityGradientTensor[elementInsideID][0][1], 2.0));

          const double alpha = 0.0168;
          double fKleb = 1.0 / (1.0 + 5.5 * std::pow(wallDistance / this->storedBoundaryLayerThickness[wallElementID], 6.0));
          this->storedKinematicEddyViscosityOuter[elementInsideID]
            = alpha * this->storedVelocityMaximum[wallElementID][1-wallNormalAxis]
              * this->storedVelocityThickness[wallElementID] * fKleb;

          this->storedKinematicEddyViscosityDifference[elementInsideID]
            = this->storedKinematicEddyViscosityInner[elementInsideID]
              - this->storedKinematicEddyViscosityOuter[elementInsideID];
        }

        // (3) switching point
        for (ElementIterator eit = gfs.gridView().template begin<0>();
             eit != gfs.gridView().template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.map(*eit);
          const typename GridView::IndexSet::IndexType wallElementID = this->storedCorrespondingWallElementID[mapperElement.map(*eit)];

          // checks if sign switches, by multiplication
          double check = this->storedKinematicEddyViscosityDifference[wallElementID] * this->storedKinematicEddyViscosityDifference[elementInsideID];
          if (check < 0 // means sign has switched
              && storedSwitchingPosition[wallElementID] > this->storedDistanceToWall[elementInsideID])
          {
            storedSwitchingPosition[wallElementID] = this->storedDistanceToWall[elementInsideID];
          }
        }

        // (4) finally determine eddy viscosity
        for (ElementIterator eit = gfs.gridView().template begin<0>();
             eit != gfs.gridView().template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.map(*eit);
          const typename GridView::IndexSet::IndexType wallElementID = this->storedCorrespondingWallElementID[mapperElement.map(*eit)];

          if (this->storedDistanceToWall[elementInsideID] >= storedSwitchingPosition[wallElementID])
          {
            this->storedKinematicEddyViscosity[elementInsideID] = storedKinematicEddyViscosityOuter[elementInsideID];
          }
          else
          {
            this->storedKinematicEddyViscosity[elementInsideID] = storedKinematicEddyViscosityInner[elementInsideID];
          }
        }
      }

      /**
       * \brief Calculates the eddy viscosity based on Baldwin-Lomax's model
       */
      template<typename GFS>
      void baldwinLomax(const GFS& gfs, const GridView& gridView)
      {
        typedef typename GridView::Traits::template Codim<0>::Iterator ElementIterator;
        const double aPlus = 26.0;
        const double k = 0.0168;
        const double cCP = 1.6;
        const double cWake = 0.25;
        const double cKleb = 0.3;

        // (1) calculate inner viscosity and Klebanoff function
        for (ElementIterator eit = gfs.gridView().template begin<0>();
             eit != gfs.gridView().template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.map(*eit);
          const typename GridView::IndexSet::IndexType wallElementID = this->storedCorrespondingWallElementID[mapperElement.map(*eit)];

          double wallDistance = this->storedDistanceToWall[elementInsideID]
                                + storedAdditionalRoughnessLength[elementInsideID];
          unsigned wallNormalAxis = this->storedCorrespondingWallNormalAxis[elementInsideID];
          double omegaAbs = std::abs(this->storedVelocityGradientTensor[elementInsideID][1-wallNormalAxis][wallNormalAxis]
                                     - this->storedVelocityGradientTensor[elementInsideID][wallNormalAxis][1-wallNormalAxis]);
          double yPlus = this->storedDistanceInWallCoordinates[elementInsideID];
          double mixingLength = KARMAN_CONSTANT * wallDistance * (1.0 - exp(-yPlus / aPlus));
          this->storedKinematicEddyViscosityInner[elementInsideID]
            = mixingLength * mixingLength * omegaAbs;

          double f = wallDistance * omegaAbs * (1.0 - exp(-yPlus / aPlus));
          if (f > storedFMax[wallElementID])
          {
            storedFMax[wallElementID] = f;
            storedYFMax[wallElementID] = wallDistance;
          }
        }

        // (2) calculate outer viscosity
        for (ElementIterator eit = gfs.gridView().template begin<0>();
             eit != gfs.gridView().template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.map(*eit);
          const typename GridView::IndexSet::IndexType wallElementID = this->storedCorrespondingWallElementID[mapperElement.map(*eit)];

          double wallDistance = this->storedDistanceToWall[elementInsideID];

          double deltaU = std::sqrt(storedVelocityMaximum[elementInsideID][0] * storedVelocityMaximum[elementInsideID][0]
                                    + storedVelocityMaximum[elementInsideID][1] * storedVelocityMaximum[elementInsideID][1])
                          - std::sqrt(storedVelocityMinimum[elementInsideID][0] * storedVelocityMinimum[elementInsideID][0]
                                      + storedVelocityMinimum[elementInsideID][1] * storedVelocityMinimum[elementInsideID][1]);
          double yFMax = storedYFMax[wallElementID];
          double fMax = storedFMax[wallElementID];
          double fWake = std::min(yFMax * fMax, cWake * yFMax * deltaU * deltaU / fMax);
          double fKleb = 1.0 / (1.0 + 5.5 * std::pow(cKleb * wallDistance / yFMax, 6.0));
          this->storedKinematicEddyViscosityOuter[elementInsideID]
            = k * cCP * fWake * fKleb;

          this->storedKinematicEddyViscosityDifference[elementInsideID]
            = this->storedKinematicEddyViscosityInner[elementInsideID]
              - this->storedKinematicEddyViscosityOuter[elementInsideID];
        }

        // (3) switching point
        for (ElementIterator eit = gfs.gridView().template begin<0>();
             eit != gfs.gridView().template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.map(*eit);
          const typename GridView::IndexSet::IndexType wallElementID = this->storedCorrespondingWallElementID[mapperElement.map(*eit)];

          // checks if sign switches, by multiplication
          double check = this->storedKinematicEddyViscosityDifference[wallElementID] * this->storedKinematicEddyViscosityDifference[elementInsideID];
          if (check < 0 // means sign has switched
              && storedSwitchingPosition[wallElementID] > this->storedDistanceToWall[elementInsideID])
          {
            storedSwitchingPosition[wallElementID] = this->storedDistanceToWall[elementInsideID];
          }
        }

        // (4) finally determine eddy viscosity
        for (ElementIterator eit = gfs.gridView().template begin<0>();
             eit != gfs.gridView().template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.map(*eit);
          const typename GridView::IndexSet::IndexType wallElementID = this->storedCorrespondingWallElementID[mapperElement.map(*eit)];

          if (this->storedDistanceToWall[elementInsideID] >= storedSwitchingPosition[wallElementID])
          {
            this->storedKinematicEddyViscosity[elementInsideID] = storedKinematicEddyViscosityOuter[elementInsideID];
          }
          else
          {
            this->storedKinematicEddyViscosity[elementInsideID] = storedKinematicEddyViscosityInner[elementInsideID];
          }
        }
      }

      /**
       * \brief Artificial function to check implementation
       */
      template<typename GFS>
      void linear(const GFS& gfs, const GridView& gridView)
      {
        typedef typename GridView::Traits::template Codim<0>::Iterator ElementIterator;
        for (ElementIterator eit = gfs.gridView().template begin<0>();
             eit != gfs.gridView().template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.map(*eit);

          double wallDistance = this->storedDistanceToWall[elementInsideID]
                                + storedAdditionalRoughnessLength[elementInsideID];
          double mixingLength = KARMAN_CONSTANT * wallDistance;
          this->storedKinematicEddyViscosity[elementInsideID] = mixingLength;
        }
      }

      /**
       * \brief Writes data of an array with element size into textfile
       *
       * Please use the given script <tt>simulationsOutput.sh</tt> to visualize
       * the data with gnuplot. The pictures can be found in the subfolder
       * <tt>pics</tt> after processing.
       *
       * \param time Current time
       * \param timeStep Number of the current time step
       * \param gridCells Number of grid cells of the problem
       */
      void gnuplotElementOutput(double time, unsigned int timeStep, unsigned int gridCells)
      {
#if PRINT_GNUPLOT_OUTPUT
        ParentType::gnuplotElementOutput(time, timeStep, gridCells);
#endif // PRINT_GNUPLOT_OUTPUT
      }

      /**
       * \brief Writes data of an array with intersection size into textfile
       *
       * Please use the given script <tt>simulationsOutput.sh</tt> to visualize
       * the data with gnuplot. The pictures can be found in the subfolder
       * <tt>pics</tt> after processing.
       *
       * \param time Current time
       * \param timeStep Number of the current time step
       * \param gridCells Number of grid cells of the problem
       */
      void gnuplotIntersectionOutput(double time, unsigned int timeStep, unsigned int gridCells)
      {
#if PRINT_GNUPLOT_OUTPUT
        ParentType::gnuplotElementOutput(time, timeStep, gridCells);
#endif // PRINT_GNUPLOT_OUTPUT
      }

    private:
      const BC& bc;
      const SourceMomentumBalance& sourceMomentumBalance;
      const SourceMassBalance& sourceMassBalance;
      const DirichletVelocity& dirichletVelocity;
      const DirichletPressure& dirichletPressure;
      const NeumannVelocity& neumannVelocity;
      const NeumannPressure& neumannPressure;
      GridView gridView;
      //! Mapper to get indices for elements
      MapperElement mapperElement;
      //! Mapper to get indices for intersections (codim=1)
      MapperIntersection mapperIntersection;
    };
  } // namespace PDELab
} // namespace Dune

#endif // DUMUX_BASE_ZEROEQ_STAGGERED_GRID_HH
