/** \file
 *  \ingroup StaggeredLocalfunctions
 *
 *  \brief The basis functions for staggered Q0 finite elements
*/

#ifndef DUMUX_STAGGERED_Q0_LOCALBASIS_HH
#define DUMUX_STAGGERED_Q0_LOCALBASIS_HH

#include <algorithm>

#include <dune/common/fmatrix.hh>

#include <dune/localfunctions/common/localbasis.hh>

namespace Dune 
{
  /**
   * \brief Constant shape function with a jump in the middle
   *
   * \tparam DF Type to represent the field in the domain
   * \tparam RF Type to represent the field in the range
   * \tparam dim Dimension of the reference element
   */
  template<class DF, class RF, unsigned int dim>
  class StaggeredQ0LocalBasis
  {
  public:
    //! \brief export type traits for function signature
    typedef LocalBasisTraits<DF, dim, Dune::FieldVector<DF, dim>, RF, dim,
      Dune::FieldVector<DF, dim>, Dune::FieldMatrix<RF, dim, dim> > Traits;

    //! \brief number of shape functions
    unsigned int size() const
    {
      return 2 * dim;
    }

    //! \brief Evaluate all shape functions
    inline void evaluateFunction(const typename Traits::DomainType& in,
                                 std::vector<typename Traits::RangeType>& out) const
    {
      out.resize(size());

      for (unsigned int curDim = 0; curDim < dim; ++curDim)
      {
        std::fill(out[2*curDim].begin(), out[2*curDim].end(), 0.0);
        std::fill(out[2*curDim+1].begin(), out[2*curDim+1].end(), 0.0);

        if (in[curDim] < 0.5)
        {
          out[2*curDim][curDim] = 1.0;
        }
        else
        {
          out[2*curDim+1][curDim] = 1.0;
        }
      }
    }

    //! \brief Evaluate Jacobian of all shape functions
    inline void
    evaluateJacobian(const typename Traits::DomainType& in,                 // position
                     std::vector<typename Traits::JacobianType>& out) const // return value
    {
      out.resize(size());
      for (unsigned int i = 0; i < size(); ++i)
      {
        for (unsigned int j = 0; j < dim; ++j)
        {
          for (unsigned int k = 0; k < dim; ++k)
          {
            out[i][j][k] = 0;
          }
        }
      }
    }

    //! \brief Polynomial order of the shape functions
    unsigned int order() const
    {
      return 0;
    }
  };
}

#endif // DUMUX_STAGGERED_Q0_LOCALBASIS_HH
