/** \file
 *  \ingroup StaggeredLocalfunctions
 *
 *  \brief The local coefficients for staggered Q0 finite elements
*/

#ifndef DUMUX_STAGGERED_Q0_LOCALCOEFFICIENTS_HH
#define DUMUX_STAGGERED_Q0_LOCALCOEFFICIENTS_HH

#include <vector>

#include <dune/localfunctions/common/localkey.hh>

namespace Dune 
{
  /**
   * \brief Layout map for staggered Q0 finite elements
   * \implements Dune::LocalCoefficientsVirtualImp
   *
   * \tparam dim Dimension of the reference element
   */
  template<unsigned int dim>
  class StaggeredQ0LocalCoefficients
  {
  public:
    //! \brief Standard constructor
    StaggeredQ0LocalCoefficients() : li(2 * dim)
    {
      // LocalKey(s, c, i)
      // s  Local number of the associated subentity
      // c  Codimension of the associated subentity
      // i  Index in the set of all functions associated to this subentity
      // set codim to one, because the dof are located there (c)
      // associate one dof to each of both shape functions (s, i)
      for (unsigned int curDim = 0; curDim < dim; ++curDim)
      {
        li[2*curDim] = LocalKey(2*curDim, 1, 0);
        li[2*curDim+1] = LocalKey(2*curDim + 1, 1, 0);
      }
    }

    //! \brief Number of coefficients
    std::size_t size() const
    {
      return 2 * dim;
    }

    //! \brief Get i'th index
    const LocalKey& localKey(std::size_t i) const
    {
      return li[i];
    }

  private:
    std::vector<LocalKey> li;
  };
}
#endif // DUMUX_STAGGERED_Q0_LOCALCOEFFICIENTS_HH
