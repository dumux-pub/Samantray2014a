#ifndef DUMUX_PDELAB_CALLBACK_NEWTON_HH
#define DUMUX_PDELAB_CALLBACK_NEWTON_HH

#include <iostream>
#include <iomanip>
#include <cmath>

#include <math.h>

#include <dune/common/exceptions.hh>
#include <dune/common/ios_state.hh>
#include <dune/common/timer.hh>

#include <dune/pdelab/backend/solver.hh>
#include <dune/pdelab/newton/newton.hh>

namespace Dune
{
    namespace PDELab
    {
        /*!
         * \brief Same as PDELab's NewtonPrepareStep but calls a function for
         * every prepare step.
         */
        template<class GOS, class LocalOperator, class TrlV, class TstV>
        class CallbackNewtonPrepareStep : public virtual NewtonBase<GOS,TrlV,TstV>
        {
            typedef GOS GridOperator;
            typedef TrlV TrialVector;

            typedef typename TstV::ElementType RFType;
            typedef typename GOS::Traits::Jacobian Matrix;

        public:
            CallbackNewtonPrepareStep(GridOperator& go, LocalOperator& lop, TrialVector& u_)
                : NewtonBase<GOS,TrlV,TstV>(go,u_)
                , localOperator(lop)
                , min_linear_reduction(1e-3)
                , fixed_linear_reduction(0.0)
                , reassemble_threshold(0.0)
            {}

            CallbackNewtonPrepareStep(GridOperator& go, LocalOperator& lop)
                : NewtonBase<GOS,TrlV,TstV>(go)
                , localOperator(lop)
                , min_linear_reduction(1e-3)
                , fixed_linear_reduction(0.0)
                , reassemble_threshold(0.0)
            {}

            /* with min_linear_reduction > 0, the linear reduction will be
               determined as mininum of the min_linear_reduction and the
               linear_reduction needed to achieve second order
               Newton convergence. */
            void setMinLinearReduction(RFType min_linear_reduction_)
            {
                min_linear_reduction = min_linear_reduction_;
            }

            /* with fixed_linear_reduction > 0, the linear reduction
               rate will always be fixed to min_linear_reduction. */
            void setFixedLinearReduction(bool fixed_linear_reduction_)
            {
                fixed_linear_reduction = fixed_linear_reduction_;
            }

            void setReassembleThreshold(RFType reassemble_threshold_)
            {
                reassemble_threshold = reassemble_threshold_;
            }

            virtual void prepare_step(Matrix& A, TstV& testVector)
            {
                this->reassembled = false;
                if (this->res.defect/this->prev_defect > reassemble_threshold)
                {
//                     localOperator.updateStoredValues(this->gridoperator.trialGridFunctionSpace(), testVector);
//                     localOperator.updateStoredValues(this->gridoperator.trialGridFunctionSpace(),
//                                                      (*this->u));
                    if (this->verbosity_level >= 3)
                        std::cout << "      Reassembling matrix..." << std::endl;
                    A = 0.0;                                    // TODO: Matrix interface
                    this->gridoperator.jacobian(*this->u, A);
//                     Dune::printmatrix(std::cout, A.base(), "global stiffness matrix", "row", 9, 1);
                    this->reassembled = true;
                }

                if (fixed_linear_reduction == true)
                    this->linear_reduction = min_linear_reduction;
                else {
                    // determine maximum defect, where Newton is converged.
                    RFType stop_defect =
                        std::max(this->res.first_defect * this->reduction,
                                 this->abs_limit);

                    /*
                       To achieve second order convergence of newton
                       we need a linear reduction of at least
                       current_defect^2/prev_defect^2.
                       For the last newton step a linear reduction of
                       1/10*end_defect/current_defect
                       is sufficient for convergence.
                    */
                    if ( stop_defect/(10*this->res.defect) >
                         this->res.defect*this->res.defect/(this->prev_defect*this->prev_defect) )
                        this->linear_reduction =
                            stop_defect/(10*this->res.defect);
                    else
                        this->linear_reduction =
                            std::min(min_linear_reduction,this->res.defect*this->res.defect/(this->prev_defect*this->prev_defect));
                }

                this->prev_defect = this->res.defect;

                ios_base_all_saver restorer(std::cout); // store old ios flags

                if (this->verbosity_level >= 3)
                    std::cout << "      requested linear reduction:       "
                              << std::setw(12) << std::setprecision(4) << std::scientific
                              << this->linear_reduction << std::endl;
            }

        private:
            LocalOperator localOperator;
            RFType min_linear_reduction;
            bool fixed_linear_reduction;
            RFType reassemble_threshold;
        };

        template<class GOS, class LocalOperator, class S, class TrlV, class TstV = TrlV>
        class CallbackNewton : public NewtonSolver<GOS,S,TrlV,TstV>
                     , public NewtonTerminate<GOS,TrlV,TstV>
                     , public NewtonLineSearch<GOS,TrlV,TstV>
                     , public CallbackNewtonPrepareStep<GOS,LocalOperator,TrlV,TstV>
        {
            typedef GOS GridOperator;
            typedef S Solver;
            typedef TrlV TrialVector;

        public:
            CallbackNewton(GridOperator& go, LocalOperator& lop, TrialVector& u_, Solver& solver_)
                : NewtonBase<GOS,TrlV,TstV>(go,u_)
                , NewtonSolver<GOS,S,TrlV,TstV>(go,u_,solver_)
                , NewtonTerminate<GOS,TrlV,TstV>(go,u_)
                , NewtonLineSearch<GOS,TrlV,TstV>(go,u_)
                , CallbackNewtonPrepareStep<GOS,LocalOperator,TrlV,TstV>(go,lop,u_)
            {}
            CallbackNewton(GridOperator& go, LocalOperator& lop, Solver& solver_)
                : NewtonBase<GOS,TrlV,TstV>(go)
                , NewtonSolver<GOS,S,TrlV,TstV>(go,solver_)
                , NewtonTerminate<GOS,TrlV,TstV>(go)
                , NewtonLineSearch<GOS,TrlV,TstV>(go)
                , CallbackNewtonPrepareStep<GOS,LocalOperator,TrlV,TstV>(go,lop)
            {}
        };
    }
}

#endif // DUMUX_PDELAB_CALLBACK_NEWTON_HH
