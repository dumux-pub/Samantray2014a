/** \file
  *  \ingroup StaggeredModel
  *
  * \brief This file contains different wall function approaches for
  *        the normal and tangential velocity component.
  *
  * The wall functions have to be called from the Dirichlet function of
  * the problem files.
  * The term "stored" in front of the variable name indicates, that this
  * variable is updated in the updateStoredValues function.
  */

#ifndef DUMUX_VELOCITY_WALLFUNCTIONS_HH
#define DUMUX_VELOCITY_WALLFUNCTIONS_HH

namespace Dune {
  namespace PDELab {

/**
  * \brief Returns wall functions values for the velocity components.
  *
  * This class contains different wall function approaches and has to be called
  * from the Dirichlet function of the problem files
  * The term "stored" in front of the variable name indicates, that this
  * variable is updated in the updateStoredValues function.
  *
  * \tparam GV GV type
  * \tparam RF RangeField type
  */
template<typename GV, typename RF>
class VelocityWallFunctions
{
public:
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GV, RF, 1> Traits;
  typedef Dune::MultipleCodimMultipleGeomTypeMapper<GV, Dune::MCMGElementLayout> MapperElement;

  //! \brief Constructor
  VelocityWallFunctions(const GV& gv)
  : mapperElement(gv)
  {
    useWallFunction_.resize(mapperElement.size());
    wallNormalAxis_.resize(mapperElement.size());
    storedVelocityGradientTensor_.resize(mapperElement.size());
    for (unsigned int i = 0; i < mapperElement.size(); ++i)
    {
      useWallFunction_[i] = false;
      wallNormalAxis_[i] = 1;
      storedVelocityGradientTensor_[i] = 0.0;
      for (unsigned int j = 0; j < GV::dimension; ++j)
      {
        for (unsigned int k = 0; k < GV::dimension; ++k)
        {
          storedVelocityGradientTensor_[i][j][k] = 0.0;
        }
      }
    }
  }

  //! \brief Check if wall functions should be used
  bool useWallFunction(const typename Traits::ElementType& e) const
  {
    return useWallFunction_[mapperElement.map(e)];
  }

  //! \brief Roughness Value \f$ m \f$
  double roughness(const typename Traits::ElementType& e,
                   const typename Traits::DomainType& x) const
  {
    return 0;
  }

  //! \brief Constant value for velocity
  double constantValueVelocity(const typename Traits::ElementType& e,
                               double velocityValue) const
  {
    return velocityValue;
  }

  //! \brief Beavers-Joseph slip-velocity condition
  double beaversJosephSlipVelocity(const typename Traits::ElementType& e,
                                   double permeability, double alphaBJ//, unsigned int normDim, unsigned int tangDim // this is required for 3D
                                   ) const
  {
    //! \todo only works for isotropic porous media and normals along domain axes
    unsigned int normDim = wallNormalAxis_[mapperElement.map(e)];
    unsigned int tangDim = 1 - normDim;
    return std::sqrt(permeability) / alphaBJ
#if ENABLE_SYMMETRIZED_VELOCITY_GRADIENT
           * (storedVelocityGradientTensor_[mapperElement.map(e)][normDim][tangDim]
              + storedVelocityGradientTensor_[mapperElement.map(e)][tangDim][normDim]);
#else
           * storedVelocityGradientTensor_[mapperElement.map(e)][tangDim][normDim];
#endif // ENABLE_SYMMETRIZED_VELOCITY_GRADIENT
  }

  MapperElement mapperElement;
  mutable std::vector<bool> useWallFunction_;
//   mutable std::vector<double> distanceToWall_;
  mutable std::vector<unsigned int> wallNormalAxis_;
  mutable std::vector<Dune::FieldMatrix<double, GV::dimension, GV::dimension> > storedVelocityGradientTensor_;
};

  } // end namespace PDELab
} // end namespace Dune

#endif // DUMUX_VELOCITY_WALLFUNCTIONS_HH
